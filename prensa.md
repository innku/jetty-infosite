---
layout: default-cobertura
title: Jetty | El transporte que mereces
description: Aplicación de Transporte, Disfruta de un Traslado Cómodo, Rápido y Seguro de Manera Diaria a Bordo de Nuestras Camionetas Ejecutivas con Conductores Verificados.
id: prensa
---

<div class="container marginTop">
  <div class="row prensa">
    <div class="col-md-4">
      <h1>Prensa</h1>
    </div>
    <div class="col-md-8 text-right contacta">
      <p><strong>Contacta con nosotros</strong>
      <br>
      <a href="mailto:prensa@jetty.mx">prensa@jetty.mx</a></p>
    </div>

    <div class="col-md-10 col-md-offset-1" style="margin-top: 20px; margin-bottom: 20px;">
      <p>Jetty es una aplicación de transporte colectivo privado con rutas optimizadas para llevar las personas de su casa a su trabajo y de regreso. Ofrecemos un servicio de transporte seguro, cómodo y accesible a bordo de camionetas ejecutivas con conductores altamente calificados.</p>
    </div>

    <div class="col-md-10 col-md-offset-1">

      <div class="row nota">
        <div class="col-md-3 logo-medios">
          <img src="imgs-prensa/expansion.png">
        </div>
        <div class="col-md-9">
          <h3>App de transporte colectivo movilizará trayectos entre CDMX y EDOMEX</h3>
          <p><small><i>10 agosto 2017</i></small></p>
          <p>La app Jetty ofrecerá servicio de transporte, tipo Uber, en camionetas entre ambos estados; la ruta inicial conectará a la zona de Lomas Verdes con Polanco.</p>
          <a href="http://expansion.mx/tecnologia/2017/08/09/app-de-transporte-colectivo-movilizara-trayectos-entre-cdmx-y-edomex" target="_blank">Ver artículo</a>
        </div>
      </div>

      <div class="row nota">
        <div class="col-md-3 logo-medios">
          <img src="imgs-prensa/excelsior.png">
        </div>
        <div class="col-md-9">
          <h3>¿Utilizas transporte en Polanco? Lanzan app de servicio colectivo.</h3>
          <p><small><i>9 agosto 2017</i></small></p>
          <p>Jetty, iniciará servicio la próxima semana en vagonetas que cubrirán la ruta Polanco-Lomas Verdes por un costo de 49 pesos por persona en una ruta metropolitana de 12 kilómetros.</p>
          <a href="http://www.excelsior.com.mx/comunidad/2017/08/09/1180659" target="_blank">Ver artículo</a>
        </div>
      </div>

      <div class="row nota">
        <div class="col-md-3 logo-medios">
          <img src="imgs-prensa/milenio.png">
        </div>
        <div class="col-md-9">
          <h3>Jetty deja fuera de circulación a 24 mil 500 autos en un año</h3>
          <p><small><i>14 septiembre 2018</i></small></p>
          <p>La plataforma contribuye a bajar emisión de contaminantes y evita congestionamientos.</p>
          <a href="http://www.milenio.com/politica/comunidad/jetty-deja-circulacion-24-mil-500-autos-ano" target="_blank">Ver artículo</a>
        </div>

      </div>

      <div class="row nota">
        <div class="col-md-3 logo-medios">
        </div>
        <div class="col-md-9">
          <h3>Camionetas Jetty conectarán Lomas Verdes y Polanco.</h3>
          <p><small><i>9 agosto 2017</i></small></p>
          <p>Las camionetas tienen características similares en seguridad y estándar que Uber y Cabify pero el precio del viaje será menor, destacó Onésimo Flores, socio fundador de Jetty.</p>
          <a href="http://www.milenio.com/df/jetty-uber-cabify-camionetas-vans-lomas_verdes-polanco-noticias_milenio_0_1008499575.html" target="_blank">Ver artículo</a>
        </div>
      </div>

      <div class="row nota">
        <div class="col-md-3 logo-medios">
          <img src="imgs-prensa/forbes.png">
        </div>
        <div class="col-md-9">
          <h3>Una app para cambiar el rumbo del transporte público</h3>
          <p><small><i>12 febrero 2018</i></small></p>
          <p>Jetty no cuenta con ningún vehículo propio, pero esta app de transporte quiere cambiar la calidad de los viajes de las personas que todos los días se trasladan del Estado de México a la Ciudad para trabajar.</p>
          <a href="https://www.forbes.com.mx/app-cambia-rumbo-del-transporte-publico/" target="_blank">Ver artículo</a>
        </div>
      </div>

      <div class="row nota">
        <div class="col-md-3 logo-medios">
          <img src="imgs-prensa/transportes.png">
        </div>
        <div class="col-md-9">
          <h3>Intengran App a servicios de SVBus</h3>
          <p><small><i>5 mayo 2018</i></small></p>
          <p>La plataforma de origen mexicano Jetty formó una alianza con SVBUS, empresa dedicada a operar los autobuses que circulan por el segundo piso y por la Supervía Poniente, en la Ciudad de México, en la que los usuarios de este servicio pueden reservar asientos en dichos buses.</p>
          <a href="https://tyt.com.mx/noticias/integran-app-a-servicios-de-svbus/" target="_blank">Ver artículo</a>
        </div>
      </div>

      <div class="row nota">
        <div class="col-md-3 logo-medios">
          <img src="imgs-prensa/vanguardia.png">
        </div>
        <div class="col-md-9">
          <h3>Jetty: Lanzan app de servicio colectivo en Polanco</h3>
          <p><small><i>9 agosto 2017</i></small></p>
          <p>Llega una buena noticia para los empleados de la zona de Polanco, una nueva aplicación para teléfonos inteligentes busca ofrecer una mejor opción de transporte colectivo.</p>
          <a href="https://www.vanguardia.com.mx/articulo/jetty-lanzan-app-de-servicio-colectivo-en-polanco" target="_blank">Ver artículo</a>
        </div>
      </div>

      <div class="row nota">
        <div class="col-md-3 logo-medios">
          <img src="imgs-prensa/animal-politico.png">
        </div>
        <div class="col-md-9">
          <h3>Jetty: Lanzan app de servicio colectivo en Polanco</h3>
          <p><small><i>10 agosto 2017</i></small></p>
          <p>Todos los días 1.7 millones de personas viajan del Estado de México, donde están las viviendas asequibles, a la CDMX, donde están las oportunidades de empleo. </p>
          <a href="https://www.animalpolitico.com/blogueros-ciudad-posible/2017/08/10/ciudad-mexico-te-presentamos-jetty/" target="_blank">Ver artículo</a>
        </div>
      </div>

      <div class="row nota">
        <div class="col-md-3 logo-medios">
          <img src="imgs-prensa/reforma.png">
        </div>
        <div class="col-md-9">
          <h3>Cumple Jetty primer año de traslados</h3>
          <p><small><i>12 septiembre 2018</i></small></p>
          <p>La mujer tiene 89 años y desde los 45 había dejado de usar el transporte público por precaución.</p>
          <a href="https://www.reforma.com/aplicacioneslibre/articulo/default.aspx?id=1489140&md5=75d84957163b1365bb2bdae2b05a9a72&ta=0dfdbac11765226904c16cb9ad1b2efe&lcmd5=c178f76cd86e823790b69674b20e622c" target="_blank">Ver artículo</a>
        </div>
      </div>

      <div class="row nota">
        <div class="col-md-3 logo-medios">
        </div>
        <div class="col-md-9">
          <h3>Refleja en documental traslados extremos</h3>
          <p><small><i>8 junio 2018</i></small></p>
          <p>No sólo en la Ciudad de México, también ciudadanos de otras urbes del mundo padecen el traslado sobre distancias extremas desde sus hogares hasta sus lugares de trabajo, experimentando pérdida de calidad de vida, violencia y menos tiempo con sus familias.</p>
          <a href="docs/reforma-documental.pdf" target="_blank">Ver artículo</a>
        </div>
      </div>

      <div class="row nota">
        <div class="col-md-3 logo-medios">
          <img src="imgs-prensa/radiocentro.png">
        </div>
        <div class="col-md-9">
          <h3>Buscan regular transporte que va a Santa Fe</h3>
          <p><small><i>22 Marzo 2019</i></small></p>
          <p>Tras el conflicto que existe entre operadores de la Ruta 5 y conductores del servicio SVBus, operado por la empresa Jetty, las autoridades capitalinas pretenden organizar los servicios de transporte colectivo privado.</p>
          <a href="http://radiocentro977.com/noticias/buscan-regular-transporte-que-va-a-santa-fe.html" target="_blank">Ver artículo</a>
        </div>
      </div>

      <div class="row nota">
        <div class="col-md-3 logo-medios">
          <img src="imgs-prensa/televisa-news.png">
        </div>
        <div class="col-md-9">
          <h3>Choferes de transporte concesionado agreden a conductores de transporte privado</h3>
          <p><small><i>23 Marzo 2019</i></small></p>
          <p>En la zona de Santa Fe, al poniente de Ciudad de México, choferes de unidades de transporte colectivo privado de una empresa que se contrata por medio de una aplicación han sido agredidos por choferes de una ruta de transporte concesionado, la Ruta 5, que corre en la misma zona y que así busca evitar la competencia.</p>
          <a href="https://noticieros.televisa.com/ultimas-noticias/choferes-de-transporte-concesionado-agreden-a-conductores-de-transporte-privado/" target="_blank">Ver artículo</a>
        </div>
      </div>

      <div class="row nota">
        <div class="col-md-3 logo-medios">
          <img src="imgs-prensa/elfinanciero.png">
        </div>
        <div class="col-md-9">
          <h3>Jetty reanudará servicio a Santa Fe el próximo lunes tras suspensión por inseguridad</h3>
          <p><small><i>24 Marzo 2019</i></small></p>
          <p>Durante el jueves y viernes pasado, la startup interrumpió su servicio en seis rutas, que corren desde el sur de la Ciudad y de Cuautitlán a Santa Fe por amenazas de conductores de camiones.</p>
          <a href="https://www.elfinanciero.com.mx/tech/jetty-reanudara-su-servicio-a-santa-fe-el-proximo-lunes-tras-suspension-por-inseguridad" target="_blank">Ver artículo</a>
        </div>
      </div>

      <div class="row nota">
        <div class="col-md-3 logo-medios">
          <img src="imgs-prensa/informador.png">
        </div>
        <div class="col-md-9">
          <h3>Jetty, la batalla por el presente</h3>
          <p><small><i>25 Marzo 2019</i></small></p>
          <p>Los modelos de transporte público de nuestras ciudades llevan décadas de rezago. Además, constituyen feudos de poder que atemorizan a cualquier gobernador.</p>
          <a href="https://www.informador.mx/ideas/Jetty-la-batalla-por-el-presente-20190325-0039.html" target="_blank">Ver artículo</a>
        </div>
      </div>

      <div class="row nota">
        <div class="col-md-3 logo-medios">
          <img src="imgs-prensa/telediario.png">
        </div>
        <div class="col-md-9">
          <h3>Jetty vuelve a operaciones tras presentar 12 denuncias de agresiones por transportistas de la Ruta 5</h3>
          <p><small><i>26 Marzo 2019</i></small></p>
          <p>Operadores del transporte masivo mediante app reanudan operaciones tras bloqueo de choferes de la Ruta 5</p>
          <a href="https://www.telediario.mx/metrópoli/jetty-vuelve-operaciones-tras-presentar-12-denuncias-de-agresiones-por-transportistas-de-la-ruta-5" target="_blank">Ver artículo</a>
        </div>
      </div>

      <div class="row nota">
        <div class="col-md-3 logo-medios">
          <img src="imgs-prensa/elheraldo.png">
        </div>
        <div class="col-md-9">
          <h3>Primero los usuarios</h3>
          <p><small><i>27 Marzo 2019</i></small></p>
          <p>Las nuevas empresas han asumido riesgos en búsqueda de mejorar los servicios, por lo que las reglas deberán balancear los incentivos</p>
          <a href="https://heraldodemexico.com.mx/opinion/primero-los-usuarios/amp/" target="_blank">Ver artículo</a>
        </div>
      </div>

    </div>
  </div>
</div>
