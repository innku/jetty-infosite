---
layout: default
title: Jetty | Soluciona tu Transporte Diario en México.
description: Aplicación de Transporte, Disfruta de un Traslado Cómodo, Rápido y Seguro de Manera Diaria a Bordo de  Camionetas Ejecutivas con Conductores Verificados.
image: https://www.jetty.mx/img/Jetty_MX.png
id: Home
priority: 1.0
---

<figure class="header">
  <img src="img/back-header2.png" alt="Jetty MX" class="back">
  <!-- <img src="img/back-header.jpg" alt="Jetty MX" class="back"> -->
  <figcaption>
    <div class="container">
      <div class="row">
        <div id="titleHead" class="col-md-5 col-sm-7" data-aos="fade" data-aos-duration="2500" data-aos-delay="300">
          <div class="text-fadein">
             <h2>¿Estás cansado de perder horas atrás del volante en el tráfico?</h2>
             <h2>¿Te incomoda ir parado en el camión?</h2>
             <h2>¿Te da miedo viajar en transporte público?</h2>
             <h2>¿Te estresa ver cómo avanza el taxímetro y las tarifas dinámicas?</h2>
          </div>
          <p class="txt-header-index">Olvida esas preocupaciones, viaja cómodo y seguro. En Jetty convertiremos cada traslado en un buen viaje.</p>

          <p class="text-downapp-index">Descarga la app.</p>

          <a href="https://itunes.apple.com/us/app/jetty-soluciona-tu-transporte/id1276413293?l=es&ls=1&mt=8" target="_blank" class="download-app hvr-shadow">
            <img src="img/jetty-iOS.png" alt="Jetty Descargar iOS">
          </a>

          <a href="https://play.google.com/store/apps/details?id=mx.jetty.jetty" target="_blank" class="download-app hvr-shadow">
            <img src="img/jetty-android.png" alt="Jetty Descargar Android">
          </a>
        </div>
        <div class="col-md-7 col-sm-5 van" data-aos="fade-right" data-aos-duration="1000" data-aos-easing="ease-out
        " >
          <img src="img/jetty-mx-camioneta.png" alt="Jetty Van">
        </div>
      </div>
    </div>
  </figcaption>
</figure>

<div class="clearfix"></div>

<div class="container index-valor">
  <div class="row">
    <div class="col-md-4">
      <div class="text-center">
        <img src="img/icon-shield.svg" alt="Jetty, Seguridad">
      </div>
      <h3>Seguridad</h3>
      <p>Todas nuestras unidades cuentan con cámaras de vigilancia y son monitoreadas vía GPS.</p>
    </div>
    <!-- <div class="col-md-3">
      <div class="text-center">
        <img src="img/icon-wifi.svg" alt="Jetty, Conectividad">
      </div>
      <h3>Conectividad</h3>
      <p>Durante el viaje, carga tu celular y conéctate a nuestro Wi-Fi.</p>
    </div> -->
    <div class="col-md-4">
      <div class="text-center">
        <img src="img/icon-support.svg" alt="Jetty, Tranquilidads">
      </div>
      <h3>Tranquilidad</h3>
      <p>Nuestros conductores y el equipo de soporte, están capacitados para darte el mejor servicio.</p>
    </div>
    <div class="col-md-4">
      <div class="text-center">
        <img src="img/icon-gps.svg" alt="Jetty, Eficiencia">
      </div>
      <h3>Eficiencia</h3>
      <p>Nuestro viajes están pensados para que puedas llegar a tu destino de la manera más directa.</p>
    </div>
  </div>
</div>

<div class="clearfix"></div>

<div>
  <img src="img/back-gray-down.png" class="img-como-funciona">
</div>

<div class="container-fluid index-como-funciona">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <h1>Cómo <span>Funciona</span></h1>
      </div>

      <div class="col-md-3" data-aos="fade-up" data-aos-easing="ease-in-sine" data-aos-duration="300">
        <div class="row">
          <div class="col-xs-2">
            <img src="img/pin-como.svg">
          </div>
          <div class="col-xs-10">
            <p>Dinos de dónde sales y a dónde viajas.</p>
            <br>
          </div>
        </div>
        <div class="text-center app-jetty">
          <img src="img/app-jetty-1.png" alt="Jetty, Cómo funciona">
        </div>
      </div>
      <div class="col-md-3" data-aos="fade-up" data-aos-easing="ease-in-sine" data-aos-duration="500">
        <div class="row">
          <div class="col-xs-2">
            <img src="img/reloj-como.svg">
          </div>
          <div class="col-xs-10">
            <p>Revisa las opciones y elige la que más te convenga.</p>
          </div>
        </div>
        <div class="text-center app-jetty">
          <img src="img/app-jetty-2.png" alt="Jetty, Cómo funciona">
        </div>
      </div>
      <div class="col-md-3" data-aos="fade-up" data-aos-easing="ease-in-sine" data-aos-duration="800">
        <div class="row">
          <div class="col-xs-2">
            <img src="img/asiento-como.svg">
          </div>
          <div class="col-xs-10">
            <p>Reserva pagando con tu tarjeta y monitorea tu Jetty.</p>
          </div>
        </div>
        <div class="text-center app-jetty">
          <img src="img/app-jetty-3.png" alt="Jetty, Cómo funciona">
        </div>
      </div>
      <div class="col-md-3" data-aos="fade-up" data-aos-easing="ease-in-sine" data-aos-duration="1000">
        <div class="row">
          <div class="col-xs-2">
            <img src="img/people-como.svg">
          </div>
          <div class="col-xs-10">
            <p>Camina al punto de abordaje y muéstrale tu pase al conductor.</p>
          </div>
        </div>
        <div class="text-center app-jetty">
          <img src="img/app-jetty-4.png" alt="Jetty, Cómo funciona">
        </div>
      </div>

      <div class="col-md-12 text-center">
        <p class="lead text-downapp">Descarga la app y disfruta de un buen viaje.</p>

        <a href="https://itunes.apple.com/us/app/jetty-soluciona-tu-transporte/id1276413293?l=es&ls=1&mt=8" target="_blank" class="download-app hvr-shadow">
          <img src="img/jetty-iOS.png" alt="Jetty Descarga App iOS">
        </a>

        <a href="https://play.google.com/store/apps/details?id=mx.jetty.jetty" target="_blank" class="download-app hvr-shadow">
          <img src="img/jetty-android.png" alt="Jetty Descarga App Android">
        </a>
      </div>
    </div>
  </div>
</div>

<div>
  <img src="img/back-grayblue-up.png" class="img-como-funciona">
</div>

<div class="clearfix"></div>

<div class="container-fluid content-buen-viaje">
  <div class="container buen-viaje">
    <div class="row">
      <div class="col-md-12" data-aos="fade-left">
        <h1>Buen viaje<br>
          <span>es...</span>
        </h1>
      </div>
    </div>

    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
          <!-- Indicators -->
          <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
          </ol>

          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">
            <div class="item active text-center">
              <img src="img/buen-viaje-slide-1.png" alt="Jetty, buen viaje es..">
            </div>
            <div class="item text-center">
              <img src="img/buen-viaje-slide-2.png" alt="Jetty, buen viaje es..">
            </div>
          </div>

          <!-- Controls -->
          <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>
    </div>

    <div class="row buen-viaje-info">
      <div class="col-md-6 col-md-offset-3 text-center" data-aos="fade-up" >
      <a href="beneficios" class="btn btn-default btn-lg btn-green btn-beneficios">Conoce más de nuestros beneficios</a>
      </div>
    </div>

  </div>
</div>

<div class="clearfix"></div>

<div class="space-greenUp">
  <img src="img/back-grayblue.png">
</div>

<div class="container testimonial">
   <div class="row">

    <div class="col-md-12 text-center">
      <h2>Lo que dicen de nosotros</h2>
    </div>

      <div class="col-md-10 col-md-offset-1">
        <div id="carousel-testimonios" class="carousel slide" data-ride="carousel">
          <!-- Indicators -->
          <!-- <ol class="carousel-indicators">
            <li data-target="#carousel-testimonios" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-testimonios" data-slide-to="1"></li>
          </ol> -->

          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">
            <div class="item active text-center">
              <h3><img src="img/comillas1.png" class="comillas"> Genial servicio! Conductor súper amable. Cada vez van abriendo más servicios y si no te queda alguna puedes preguntar para que te digan por donde pasa y te recojan. Seguro, rápido y cómodo. No hay mejor manera para llegar a Santa Fé <img src="img/comillas2.png" class="comillas"><br><span class="name">Nancy G.</span></h3>
            </div>
            <div class="item text-center">
              <h3><img src="img/comillas1.png" class="comillas"> Excelente servicio, de hecho extraordinario de verdad, lo recomendaré mucho. <img src="img/comillas2.png" class="comillas"><br><span class="name">Juan Carlos C.</span></h3>
            </div>
            <div class="item text-center">
              <h3><img src="img/comillas1.png" class="comillas"> Muy buen servicio, las camionetas son super puntuales, cuentan con cámaras de seguridad y el servicio al cliente siempre está atento a lo que necesites, lo utilizaré casi diario. <img src="img/comillas2.png" class="comillas"><br><span class="name">Rafael C.</span></h3>
            </div>
            <div class="item text-center">
              <h3><img src="img/comillas1.png" class="comillas"> Excelente, llevo varios meses viajando en Jetty y el servicio es perfecto, Enrique es super amable muy educado y siempre preocupado porque lleguemos a tiempo y con bien a nuestro destino, al servicio y a Enrique les doy 5 estrellas, gracias. <img src="img/comillas2.png" class="comillas"><br><span class="name">Hugo C.</span></h3>
            </div>
            <div class="item text-center">
              <h3><img src="img/comillas1.png" class="comillas"> Excelente servicio, ya no uso mi carro para ir a la oficina. <img src="img/comillas2.png" class="comillas"><br><span class="name">Luis Antonio F.</span></h3>
            </div>
            <div class="item text-center">
              <h3><img src="img/comillas1.png" class="comillas"> Como ya se está volviendo una maravillosa costumbre, JETTY me acompaña casi diario para comenzar bien mi día... ¡Son únicos! ¡Los amo! <img src="img/heart.png" width="25"> <img src="img/comillas2.png" class="comillas"><br><span class="name">Klaus</span></h3>
            </div>
          </div>

          <!-- Controls -->
          <!-- <a class="left carousel-control" href="#carousel-testimonios" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#carousel-testimonios" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a> -->
        </div>
      </div>
    </div>
</div>
