<nav class="navbar navbar-fixed-top navbar-default nav-backg">
  <div class="container-fluid">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="{{site.baseurl}}/en/">
          <img src="{{ site.baseurl }}/img/logo-jetty.svg">
        </a>
      </div>
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-right">
          <li><a href="{{site.baseurl}}/en/">Home</a></li>
          <li><a href="{{site.baseurl}}/en/beneficios">Benefits</a></li>
          <li><a href="{{site.baseurl}}/en/transporte-personal">Enterprise</a></li>
          <li><a href="{{site.baseurl}}/en/cobertura">Service areas</a></li>
          <li><a href="{{site.baseurl}}/en/transporte-eventos">Events</a></li>
          <li><a href="{{site.baseurl}}/blog">Blog</a></li>
          <li><a href="{{site.baseurl}}/">Español</a></li>
        </ul>
      </div>
    </div>
  </div>
</nav>