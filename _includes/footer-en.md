
<div class="container footer">

  <div class="row">
    <div class="col-md-4 footer-downapp">
      <p class="text-downapp">Download the app.</p>
        <!-- <a href="solicitud" class="btn btn-default btn-lg btn-green btn-header">¿A dónde te llevamos?</a> -->

        <a href="https://itunes.apple.com/us/app/jetty-soluciona-tu-transporte/id1276413293?l=es&ls=1&mt=8" target="_blank" class="download-app hvr-shadow">
          <img src="{{ site.baseurl }}/img/jetty-iOS-en.png" alt="Jetty Descargar iOS">
        </a>

        <a href="https://play.google.com/store/apps/details?id=mx.jetty.jetty" target="_blank" class="download-app hvr-shadow">
          <img src="{{ site.baseurl }}/img/jetty-android-en.png" alt="Jetty Descargar Android">
        </a>
    </div>
    <!-- <div class="col-md-1"></div> -->
    <div class="col-md-3">
      <ul class="submenu">
        <li>
          <a href="{{site.baseurl}}/en/quienes">About us</a>
        </li>
        <li>
          <a href="{{site.baseurl}}/en/vacantes" target="_self">Work with us</a>
        </li>
        <li>
          <a href="{{site.baseurl}}/en/transporte-eventos" target="_self">Events</a>
        </li>
        <li>
          <a href="{{site.baseurl}}/en/prensa" target="_self">Press</a>
        </li>
        <li>
          <a href="{{site.baseurl}}/en/trabajo-conductor" target="_self">Driver or transportation operators </a>
        </li>
      </ul>
    </div>

    <div class="col-md-3">
      <ul class="submenu">
        <li>
          <a href="http://ayuda.jetty.mx" target="_self">FAQs</a>
        </li>
        <li>
          <a href="{{site.baseurl}}/en/aviso-de-privacidad" target="_self">Privacy</a>
        </li>
        <li>
          <a href="{{site.baseurl}}/en/terminos" target="_self">Terms & conditions</a>
        </li>
        <li>
          <a href="{{site.baseurl}}/en/prestacion" target="_self">Service</a>
        </li>
      </ul>
    </div>

    <div class="col-md-2 logo-footer">
      <img src="{{ site.baseurl }}/img/logo-jetty-green.svg">
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <ul class="social text-center">
        <li class="social-mobile">
          <a href="https://www.facebook.com/JettyMX/" target="_blank" class="social-fb"></a>
        </li>
        <li class="social-mobile">
          <a href="https://twitter.com/jettymx" target="_blank" class="social-tw"></a>
        </li>
        <li class="social-mobile">
          <a href="https://www.instagram.com/jetty.mx" target="_blank" class="social-ins"></a>
        </li>
        <li class="social-mobile">
          <a href="https://medium.com/@jettymx" target="_blank" class="social-medium"></a>
        </li>
      </ul>
    </div>
  </div>

</div>