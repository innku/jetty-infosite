<div class="container-fluid footer">
  <div class="container">

    <div class="row">

      <div class="col-md-2 logo-footer">
        <img src="{{ site.baseurl }}/img/logo-jetty-green.svg">
      </div>

      <div class="col-md-3">
        <ul class="submenu">
          <li>
            <a href="{{site.baseurl}}/quienes">Quiénes somos</a>
          </li>
          <li>
            <a href="{{site.baseurl}}/vacantes" target="_self">Trabaja con nosotros</a>
          </li>
          <li>
            <a href="{{site.baseurl}}/transporte-eventos" target="_self">Eventos</a>
          </li>
          <li>
            <a href="{{site.baseurl}}/prensa" target="_self">Prensa</a>
          </li>
          <li>
            <a href="{{site.baseurl}}/trabajo-conductor" target="_self">Conductor o transportista</a>
          </li>
        </ul>
      </div>

      <div class="col-md-3">
        <ul class="submenu">
          <li>
            <a href="http://ayuda.jetty.mx" target="_self">Ayuda y Soporte</a>
          </li>
          <li>
            <a href="{{site.baseurl}}/aviso-de-privacidad" target="_self">Aviso de privacidad</a>
          </li>
          <li>
            <a href="{{site.baseurl}}/terminos" target="_self">Términos y condiciones</a>
          </li>
          <li>
            <a href="{{site.baseurl}}/prestacion" target="_self">Prestación de servicio</a>
          </li>
        </ul>
      </div>

      <div class="col-md-4 footer-downapp">
        <p class="text-downapp">Descarga la app.</p>

          <a href="https://itunes.apple.com/us/app/jetty-soluciona-tu-transporte/id1276413293?l=es&ls=1&mt=8" target="_blank" class="download-app hvr-shadow">
            <img src="{{ site.baseurl }}/img/jetty-iOS.png" alt="Jetty Descargar iOS">
          </a>

          <a href="https://play.google.com/store/apps/details?id=mx.jetty.jetty" target="_blank" class="download-app hvr-shadow">
            <img src="{{ site.baseurl }}/img/jetty-android.png" alt="Jetty Descargar Android">
          </a>

          <div class="">
            <ul class="social">
              <li class="social-mobile">
                <a href="https://www.facebook.com/JettyMX/" target="_blank" class="social-fb"></a>
              </li>
              <li class="social-mobile">
                <a href="https://twitter.com/jettymx" target="_blank" class="social-tw"></a>
              </li>
              <li class="social-mobile">
                <a href="https://www.instagram.com/jetty.mx" target="_blank" class="social-ins"></a>
              </li>
              <li class="social-mobile">
                <a href="https://medium.com/@jettymx" target="_blank" class="social-medium"></a>
              </li>
              <li class="social-mobile">
                <a href="https://mx.linkedin.com/company/jetty.mx" target="_blank" class="social-linkedin"></a>
              </li>
            </ul>
          </div>

      </div>

    </div>

  </div>
</div>
