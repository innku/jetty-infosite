---
layout: default
title: Jetty | Preguntas Frecuentes
description: En el centro de ayuda, encontrarás las respuestas a las preguntas más frecuentes. ¿Qué es Jetty? ¿Cómo funciona?
id: aviso-de-privacidad
---

<body>
  <script type="text/javascript">
  window.location="http://ayuda.jetty.mx";
  </script>
</body>

<!-- <div class="container">
  <div class="row privacidad">
    <div class="col-md-12">
      <h1>FAQs</h1>
    </div>

    <div>
      <ul class="indice">
          <div class="col-md-6">
            <li><a href="javascript:scroll('#uno', 800)" >¿Qué es Jetty?</a></li>
            <li><a href="javascript:scroll('#dos',800);">¿Cómo funciona?</a></li>
            <li><a href="javascript:scroll('#tres',800);">¿Puedo viajar sin reservación?</a></li>
            <li><a href="javascript:scroll('#cuatro',800);">¿Mi asiento está garantizado?</a></li>
            <li><a href="javascript:scroll('#cinco',800);">¿Cómo monitoreo mi viaje?</a></li>
            <li><a href="javascript:scroll('#seis',800);">¿Qué pasa si llego tarde al punto de abordaje del Jetty?</a></li>
            <li><a href="javascript:scroll('#siete',800);">¿Tienen tolerancia en los tiempos de salida?</a></li>
            <li><a href="javascript:scroll('#ocho',800);">¿Qué pasa si mi Jetty llega tarde?</a></li>
            <li><a href="javascript:scroll('#nueve',800);">¿Qué hago si no encuentro la parada?</a></li>
            <li><a href="javascript:scroll('#diez',800);">¿Puedo cambiar / cancelar mi viaje?</a></li>
            <li><a href="javascript:scroll('#once',800);">¿Estoy asegurado cuando viajo en Jetty?</a></li>
            <li><a href="javascript:scroll('#doce',800);">¿Cuál es su política acerca de las sillas de rueda u otros dispositivos de asistencia?</a></li>
            <li><a href="javascript:scroll('#trece',800);">¿Cuál es su política contra la discriminación?</a></li>
            <li><a href="javascript:scroll('#catorce',800);">¿Tienen servicio en días festivos?</a></li>
            <li><a href="javascript:scroll('#quince',800);">¿Pueden viajar con un menor de edad?</a></li>
            <li><a href="javascript:scroll('#dieciseis',800);">¿Puedo darle propina a mi conductor?</a></li>
            <li><a href="javascript:scroll('#diecisiete',800);">¿Qué hago si el viaje que deseo no está disponible?</a></li>
            <li><a href="javascript:scroll('#dieciocho',800);">¿Dónde puedo encontrar las áreas de servicio y horarios de Jetty?</a></li>
          </div>

          <div class="col-md-6">
            <li><a href="javascript:$.scrollTo('#diecinueve',800);">¿Cuánto cuesta viajar en Jetty?</a></li>
            <li><a href="javascript:$.scrollTo('#veinte',800);">¿De qué forma puedo comprar mi boleto?</a></li>
            <li><a href="javascript:$.scrollTo('#veintiuno',800);">¿Con cuánto tiempo de anticipación puedo reservar mi viaje?</a></li>
            <li><a href="javascript:$.scrollTo('#veintidos',800);">¿Puedo viajar en Jetty sin tener la app?</a></li>
            <li><a href="javascript:$.scrollTo('#veintitres',800);">¿Puedo llevar maletas grandes?</a></li>
            <li><a href="javascript:$.scrollTo('#veinticuatro',800);">¿Puedo subir mi bicicleta al Jetty?</a></li>
            <li><a href="javascript:$.scrollTo('#veinticinco',800);">¿Olvidé algo en un Jetty, ¿qué tengo que hacer?</a></li>
            <li><a href="javascript:$.scrollTo('#veintiseis',800);">¿Puedo comer en mi Jetty?</a></li>
            <li><a href="javascript:$.scrollTo('#veintisiete',800);">¿Qué pasa si el sistema de cobro rechaza mi pago?</a></li>
            <li><a href="javascript:$.scrollTo('#veintiocho',800);">¿Qué hago si la app no funciona?</a></li>
            <li><a href="javascript:$.scrollTo('#veintinueve',800);">¿Puedo solicitar Jetty para un evento o viaje privado?</a></li>
            <li><a href="javascript:$.scrollTo('#treinta',800);">¿Puedo cancelar mi reservación?</a></li>
            <li><a href="javascript:$.scrollTo('#treintaiuno',800);">¿Mi reservación es transferible?</a></li>
            <li><a href="javascript:$.scrollTo('#trentaidos',800);">¿Qué hago si me equivoqué al reservar mi Jetty?</a></li>
            <li><a href="javascript:$.scrollTo('#trentaitres',800);">¿Cómo puedo pedir una factura?</a></li>
            <li><a href="javascript:$.scrollTo('#trentaicuatro',800);">¿Qué hago para trabajar en Jetty?</a></li>
            <li><a href="javascript:$.scrollTo('#trentaicinco',800);">¿Puedo comprar más de un boleto?</a></li>
          </div>
      </ul>
    </div>

    <div class="col-md-12"  style="margin-top: 40px">
      <h3 id="uno">¿Qué es Jetty?</h3>
      <p>Jetty es una plataforma tecnológica 100% mexicana que te permite pedir un transporte seguro y cómodo para ir y regresar de tu casa a tu trabajo o universidad. Contamos con vehículos asegurados en perfectas condiciones y conductores altamente calificados.</p>

      <h3 id="dos">¿Cómo funciona?</h3>
      <p><strong>Es sencillo:</strong></p>
      <ol>
        <li>Descarga la aplicación: <a href="https://itunes.apple.com/us/app/jetty-soluciona-tu-transporte/id1276413293?l=es&ls=1&mt=8" target="_blank" target="_blank">iOS</a> / <a href="https://play.google.com/store/apps/details?id=mx.jetty.jetty" target="_blank">Android</a></li>
        <li>Abre la app y dinos de dónde a dónde viajas.</li>
        <li>Se mostrarán los viajes disponibles, o la zona de cobertura si tu viaje no coincide con nuestra área de servicio.</li>
        <li>Reserva tu asiento pagando con tarjeta de crédito o débito.</li>
        <li>Recibe confirmación a través de tu pase de abordar electrónico.</li>
        <li>El dia y hora de tu viaje, camina hacia el punto de abordaje del Jetty. Te recomendamos estar 5 minutos antes de la hora de salida de tu Jetty.</li>
        <li>Revisa la foto del conductor, los datos del vehículo, y rastrea el trayecto del Jetty en el mapa mientras llega.</li>
        <li>Enseñale tu pase de abordar al conductor.</li>
        <li>Disfruta del viaje sin tantas paradas innecesarias. Conéctate a internet, relájate o duerme un poco. Tu conductor te notificará una vez que llegues a tu destino.</li>
        <li>Después de tu viaje, deja retroalimentación sobre tu experiencia. Con tu ayuda, seguimos mejorando.</li>
        <li>¡Vuelve a pedir un asiento en Jetty!</li>
      </ol>

      <h3 id="tres">¿Puedo viajar sin reservación?</h3>
      <p>No. Por tu seguridad y de los demás usuarios, es necesario que cada persona que aborde Jetty cuente con un pase válido.</p>

      <h3 id="cuatro">¿Mi asiento está garantizado?</h3>
      <p>Sí. Nunca sobrevendemos el número de asientos disponibles. Y pedimos a cada usuario guardar sus pertenencias sobre sus piernas o debajo del asiento para que las otras personas que abordan puedan tener espacio y viajar cómodas.</p>

      <h3 id="cinco">¿Cómo monitoreo mi viaje?</h3>
      <p>En la esquina superior derecha de la app podrás ver un ícono de pases. Al seleccionarlo verás los viajes que has reservado. Tan sólo necesitas seleccionar el viaje asociado a tu viaje en curso y te llevará al mapa para rastrear el movimiento de tu Jetty hasta el punto de abordaje en donde estarás iniciando tu recorrido.</p>

      <h3 id="seis">¿Qué pasa si llego tarde al punto de abordaje del Jetty?</h3>
      <p>Por respeto al tiempo de los demás usuarios, cuando han transcurrido los 2 minutos de tolerancia el Jetty seguirá su camino y al no haberte presentado se contará como un viaje efectuado, por lo que no se realizará reposición del boleto. Si quieres revisar tu situación contáctanos en <a href="mailto:soporte@jetty.mx">soporte@jetty.mx</a>, cuéntanos lo que pasó y el equipo de Jetty analizará tu caso.</p>

      <h3 id="siete">¿Tienen tolerancia en los tiempos de salida?</h3>
      <p>Tenemos una tolerancia de 2 minutos en cada punto de ascenso durante el trayecto. Sin embargo, nos esforzamos por mantener una puntualidad rigurosa, pues nuestra máxima preocupación es que cada usuario llegue a tiempo a su destino, y un tiempo de tolerancia mayor, significa un retraso para todos los usuarios, incluyéndote a ti. </p>

      <h3 id="ocho">¿Qué pasa si mi Jetty llega tarde?</h3>
      <p>El equipo de operaciones monitorea permanentemente los viajes en curso, por lo que si existe una demora (usualmente debida al tránsito), te lo haremos saber mediante un mensaje en la app. Sabemos que esto puede causar alguna molestia, pero te pedimos ser paciente y esperar en el punto designado, pues la movilidad en la Ciudad no siempre permite exactitud en los tiempos de traslado. Ten por seguro que estaremos contigo para realizar tu trayecto.</p>

      <h3 id="nueve">¿Qué hago si no encuentro la parada?</h3>
      <p>Los puntos de ascenso están en la app, y es este el punto en donde debes abordar. Para tu tranquilidad, el día de tu primer viaje te haremos una llamada personalizada, 5 minutos antes de abordar, para confirmar que el punto en el que te encuentras sea el correcto, y ayudarte a identificar el Jetty que te llevará a tu destino.</p>

      <h3 id="diez">¿Puedo cambiar / cancelar mi viaje?</h3>
      <p>Sí, aunque te pedimos considerar el tiempo de respuesta para nuestro equipo operativo. En caso de cancelación o cambio de viaje, debes realizarlo con un mínimo de 30 minutos de anticipación para que dichas modificaciones surtan efecto, ya que debe realizarse esta operación manualmente.</p>

      <h3 id="once">¿Estoy asegurado cuando viajo en Jetty?</h3>
      <p>Sí. Como parte de nuestros estándares en Jetty, todas las unidades y usuarios a bordo se encuentran respaldados por la póliza de seguros de cobertura amplia, así que puedes viajar con la tranquilidad de que, ante cualquier eventualidad, Jetty te cuida.</p>

      <h3 id="doce">¿Cuál es su política acerca de las sillas de rueda u otros dispositivos de asistencia?</h3>
      <p>Dentro de las posibilidades del vehículo, aceptamos a los usuarios que utilizan sillas de rueda plegables, caminadores, bastones u otros dispositivos de asistencia. El conductor tiene instrucciones de poder ofrecer asistencia para el abordaje y descenso.</p>
      <p>Somos muy rigurosos al momento de ofrecer opciones de accesibilidad e inclusión para cada pasajero, por lo que cualquier informe de discriminación resultará en la desactivación temporal del conductor, mientras investiguemos el incidente. Si proceden las infracciones a las leyes de admisión y protección de usuarios con discapacidades, puede resultar en la pérdida permanente del acceso a la plataforma para el conductor.</p>

      <h3 id="trece">¿Cuál es su política contra la discriminación?</h3>
      <p>Estamos orgullosos de ser una plataforma que no discrimina, ya sea por motivos de raza, religión, orientación sexual, condición física o socioeconómica ni por ningún otro motivo. Creemos que cualquier persona merece tener un buen viaje. Discriminación de cualquier tipo resultará en la desactivación temporal del conductor, mientras investiguemos el incidente.</p>

      <h3 id="catorce">¿Tienen servicio en días festivos?</h3>
      <p>Damos servicio de lunes a viernes. Los días que no damos servicio son los días de asueto establecidos por ley.</p>

      <h3 id="quince">¿Pueden viajar con un menor de edad?</h3>
      <p>Sí. Es necesario que siempre vayan acompañados por un adulto.</p>

      <h3 id="dieciseis">¿Puedo darle propina a mi conductor?</h3>
      <p>Nuestros conductores no aceptan propina debido a que todos cuentan con un salario competitivo. Si recibes un gran servicio, una sonrisa y las gracias siempre son bienvenidos. Y por supuesto, agradecemos nos compartas tu experiencia y tu calificación en la Play Store o Apple Store y en nuestras redes sociales.</p>

      <h3 id="diecisiete">¿Qué hago si el viaje que deseo no está disponible?</h3>
      <p>Puedes entrar a <a href="http://jetty.mx/solicitud" target="_blank">http://jetty.mx/solicitud</a> para sugerirnos una nueva área de servicio. Constantemente analizamos nuevas zonas de cobertura, así que mientras más usuarios la soliciten, aumenta la posibilidad de abrir las áreas de servicio que sugieres. Te haremos saber cuando abramos esta nueva zona de cobertura.</p>

      <h3 id="dieciocho">¿Dónde puedo encontrar las áreas de servicio y horarios de Jetty?</h3>
      <p>Al ser un servicio de transporte dinámico, no utilizamos un sistema programado con rutas establecidas y horarios fijos. Nuestra tecnología está diseñada para que puedas seleccionar los puntos de origen y destino, y a partir de las opciones elegir un viaje que se adapte a tus necesidades. Los horarios pueden variar de vez en cuando en función del tráfico. Puedes ver nuestras áreas de servicio en <a href="http://www.jetty.mx/cobertura" target="_blank">http://www.jetty.mx/cobertura</a></p>

      <h3 id="diecinueve">¿Cuánto cuesta viajar en Jetty?</h3>
      <p>Tu tarifa de viaje se calcula al sumar una tarifa base, la distancia que vas a recorrer y la aportación al fondo para la movilidad. En Jetty no hay ninguna sorpresa del costo de tu viaje. Te mandamos el precio del viaje, y una vez confirmada tu reservación, ya no habrá ningún cargo adicional.</p>

      <h3 id="veinte">¿De qué forma puedo comprar mi boleto?</h3>
      <p>Para garantizar la seguridad de nuestros usuarios y conductores, las reservaciones se realizan a través de cualquier tarjeta de crédito o débito. Si tu cuenta presenta algún problema, puedes realizar un depósito o transferencia bancaria a la cuenta de Jetty, una vez que te pongas en contacto con nosotros. No aceptamos pago en efectivo.</p>

      <h3 id="veintiuno">¿Con cuánto tiempo de anticipación puedo reservar mi viaje?</h3>
      <p>Puedes comprar tu pase de abordar hasta 10 minutos antes de la hora de abordaje, aunque es recomendable realizarlo con un poco más de anticipación para garantizar que tu asiento esté disponible. Una vez transcurridos los 10 minutos, ya no podrás realizar tu reservación.</p>

      <h3 id="veintidos">¿Puedo viajar en Jetty sin tener la app?</h3>
      <p>No. La única forma de reservar un viaje es a través de la app ya que al ingresar al vehículo, debes enseñarle tu pase al conductor. De esta forma garantizamos tu seguridad y la de todos nuestros usuarios, sabiendo quién viaja en cada recorrido y evitando que aborden usuarios sin boleto.</p>

      <h3 id="veintitres">¿Puedo llevar maletas grandes?</h3>
      <p>Desafortunadamente, todos tus artículos personales deben caber sobre tus piernas, o debajo de tu asiento para nuestro servicio de lunes a viernes. Debemos mantener los pasillos libres para que los pasajeros puedan subir y bajar del vehículo. Si tienes la necesidad de llevar equipaje grande y/o equipo deportivo para un evento / fin de semana, te pedimos que nos notifiques previamente antes de abordar tu Jetty.</p>

      <h3 id="veinticuatro">¿Puedo subir mi bicicleta al Jetty?</h3>
      <p>Siempre y cuando tu bicicleta no obstruya los pasillos y no incomode a los demás pasajeros, sí está permitido subirla a la camioneta. Procura colocarla de manera que esté garantizado el ascenso y descenso de todos los usuarios con facilidad. Asimismo, al subir y bajar tu bicicleta, hazlo con la mayor agilidad posible para no retrasar el viaje en curso.</p>

      <h3 id="veinticinco">Olvidé algo en un Jetty, ¿qué tengo que hacer?</h3>
      <p>Contáctanos a través de la pestaña de soporte en la aplicación o escribiendo a <a href="mailto:soporte@jetty.mx" target="_blank">soporte@jetty.mx</a> y danos el detalle de lo que se te olvidó y en qué viaje. Nos pondremos en contacto con el conductor para que puedas recuperar lo que extraviaste en nuestra oficina.</p>

      <h3 id="veintiseis">¿Puedo comer en mi Jetty?</h3>
      <p>Está permitido comer o beber en los vehículos, pero por favor considera a los demás. Es válido un bocado ligero y frío pero guarda tus alimentos más exóticos para cuando llegues a tu destino. Y por atención te pedimos no dejar ningún residuo dentro del vehículo. Recuerda que el servicio es para todos, y nos gustaría que tú lo encuentres en las mejores condiciones. Siempre.</p>

      <h3 id="veintisiete">¿Qué pasa si el sistema de cobro rechaza mi pago?</h3>
      <p>Te pedimos revisar que los datos ingresados sean correctos, y que cuentas con saldo suficiente para reservar tu viaje. Si se sigue presentando el error, te pedimos comunicarte con nosotros a <a href="mailto:soporte@jetty.mx" target="_blank">soporte@jetty.mx</a> para encontrar una solución. Adicionalmente te pedimos no realizar más intentos de cobro, pues existe el riesgo de ser bloqueado por las políticas antifraudes de nuestro sistema de cobro. Si el problema se debe a nuestro sistema, usualmente queda resuelto en menos de 24 horas. Estaremos en contacto contigo durante este tiempo para ayudarte a resolver satisfactoriamente esta situación.</p>

      <h3 id="veintiocho">¿Qué hago si la app no funciona?</h3>
      <p>Primero revisa que estés conectado a la red celular o a un wi-fi. Posteriormente, revisa que tengas la versión actualizada de la aplicación. Esto lo puedes realizar en la Play Store o Apple Store en donde descargaste la app. Si, a pesar de descargar la última versión, el problema persiste, comunícate con nosotros a <a href="mailto:soporte@jetty.mx" target="_blank">soporte@jetty.mx</a> y con gusto te ayudaremos.</p>

      <h3 id="veintinueve">¿Puedo solicitar Jetty para un evento o viaje privado?</h3>
      <p>Claro. Puedes reservar vehículo particular con conductor incluido, para salidas de fin de semana, bodas, conciertos, etc. Si deseas reservar este tipo de servicio, puedes entrar a <a href="http://www.jetty.mx/eventos" target="_blank">http://www.jetty.mx/eventos</a> para conocer los detalles.</p>

      <h3 id="treinta">¿Puedo cancelar mi reservación?</h3>
      <p>Sí, aunque te pedimos considerar el tiempo de respuesta para nuestro equipo operativo. En caso de cancelación o cambio, debes realizarlo con un mínimo de 30 minutos de anticipación para que dichas modificaciones surtan efecto, ya que debe realizarse esta operación manualmente. No podemos hacerte la devolución del pago por lo que únicamente únicamente podrás cambiarlo por otro viaje que tenga disponibilidad. </p>

      <h3 id="treintaiuno">¿Mi reservación es transferible?</h3>
      <p>No. Esto lo hacemos para garantizar la seguridad de nuestros usuarios y conductores. Si tu cuenta presenta algún problema, puedes realizar un depósito o transferencia bancaria a la cuenta de Jetty, una vez que te pongas en contacto con nosotros. </p>

      <h3 id="trentaidos">¿Qué hago si me equivoqué al reservar mi Jetty?</h3>
      <p>Puedes cambiarlo aunque te pedimos considerar el tiempo de respuesta para nuestro equipo operativo. En caso de cancelación o cambio de viaje, debes realizarlo con un mínimo de 30 minutos de anticipación para que dichas modificaciones surtan efecto, ya que debe realizarse esta operación manualmente. Tu cambio estará sujeto a que el Jetty tenga disponibilidad. </p>

      <h3 id="trentaitres">¿Cómo puedo pedir una factura?</h3>
      <p>Debido al proceso contable de Jetty, solamente es posible emitir una factura mensual para el total de tus viajes en el mes, pues no podemos emitir una factura individual para cada viaje. La fecha de nuestro cierre es el 25 de cada mes, así que podemos expedir la factura por el total de tus viajes desde el 25 del mes anterior hasta el 24 del mes en curso. Para solicitarla, envía un correo a <a href="mailto:cristina@jetty.mx" target="_blank">cristina@jetty.mx</a>  con los datos fiscales y el correo al que quieres que lleguen.</p>

      <h3 id="trentaicuatro">¿Qué hago para trabajar en Jetty? </h3>
      <p>Siempre estamos buscando a gente que se sume al equipo. Si te interesa escríbenos a <a href="mailto:contacto@jetty.mx" target="_blank">contacto@jetty.mx</a> con tu CV.</p>

      <h3 id="trentaicinco">¿Puedo comprar más de un boleto?</h3>
      <p>Sí, a la hora de reservar tu viaje puedes señalar cuántos pases quieres haciendo clic en la parte de más o menos pases. <img src="img/mas-menos.png" width="60"></p>

    </div>

    <div class="col-md-12 text-center typeform">
      <p class="lead">Si no encontraste lo que buscabas, haz clic <a class="typeform-share" href="https://cledestino.typeform.com/to/Hqhtgq" data-mode="popup" target="_blank">aquí </a> <script> (function() { var qs,js,q,s,d=document, gi=d.getElementById, ce=d.createElement, gt=d.getElementsByTagName, id="typef_orm_share", b="https://embed.typeform.com/"; if(!gi.call(d,id)){ js=ce.call(d,"script"); js.id=id; js.src=b+"embed.js"; q=gt.call(d,"script")[0]; q.parentNode.insertBefore(js,q) } })() </script> para recibir atención personalizada</p>
    </div>

  </div>

</div> -->