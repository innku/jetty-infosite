---
layout: default-cobertura-en
title: Jetty | El transporte que mereces
description: Aplicación de Transporte, Disfruta de un Traslado Cómodo, Rápido y Seguro de Manera Diaria a Bordo de Nuestras Camionetas Ejecutivas con Conductores Verificados.
id: prensa
---

<div class="container marginTop">
  <div class="row prensa">
    <div class="col-md-4">
      <h1>Prensa</h1>
    </div>
    <div class="col-md-8 text-right contacta">
      <p><strong>Contacta con nosotros</strong>
      <br>
      Cristina Palacios, <a href="mailto:cristina@jetty.mx">cristina@jetty.mx</a></p>
    </div>

    <div class="col-md-10 col-md-offset-1">

      <div class="row nota">
        <div class="col-md-3 logo-medios">
          <a href="http://www.reforma.com/aplicacioneslibre/articulo/default.aspx?id=1184435&md5=1f679f6fd54761085bdd31033166b808&ta=0dfdbac11765226904c16cb9ad1b2efe&lcmd5=e79502511d06f4b2cc756574b8436514" target="_blank"><img src="imgs-prensa/reforma.png"></a>
        </div>
        <div class="col-md-9">
          <h3>Ofrecen transporte de lujo en Metrópoli.</h3>
          <p>Ahora, viajar entre Lomas Verdes y Polanco con asiento reservado, wifi y hora de abordaje, será posible a través de una app para iOS y Android.</p>
          <a href="http://www.reforma.com/aplicacioneslibre/articulo/default.aspx?id=1184435&md5=1f679f6fd54761085bdd31033166b808&ta=0dfdbac11765226904c16cb9ad1b2efe&lcmd5=e79502511d06f4b2cc756574b8436514" target="_blank">Ver artículo</a>
        </div>
      </div>

      <div class="row nota">
        <div class="col-md-3 logo-medios">
          <!-- <a href="docs/reforma-documental.pdf" target="_blank"><img src="imgs-prensa/reforma.png"></a> -->
        </div>
        <div class="col-md-9">
          <h3>Refleja en documental traslados extremos</h3>
          <p>No sólo en la Ciudad de México, también ciudadanos de otras urbes del mundo padecen el traslado sobre distancias extremas desde sus hogares hasta sus lugares de trabajo, experimentando pérdida de calidad de vida, violencia y menos tiempo con sus familias.</p>
          <a href="docs/reforma-documental.pdf" target="_blank">Ver artículo</a>
        </div>
      </div>

      <div class="row nota">
        <div class="col-md-3 logo-medios">
          <a href="http://www.excelsior.com.mx/comunidad/2017/08/09/1180659" target="_blank"><img src="imgs-prensa/excelsior.png"></a>
        </div>
        <div class="col-md-9">
          <h3>¿Utilizas transporte en Polanco? Lanzan app de servicio colectivo.</h3>
          <p>Jetty, iniciará servicio la próxima semana en vagonetas que cubrirán la ruta Polanco-Lomas Verdes por un costo de 49 pesos por persona en una ruta metropolitana de 12 kilómetros.</p>
          <a href="http://www.excelsior.com.mx/comunidad/2017/08/09/1180659" target="_blank">Ver artículo</a>
        </div>
      </div>

      <div class="row nota">
        <div class="col-md-3 logo-medios">
          <a href="http://www.milenio.com/df/jetty-uber-cabify-camionetas-vans-lomas_verdes-polanco-noticias_milenio_0_1008499575.html" target="_blank"><img src="imgs-prensa/milenio.png"></a>
        </div>
        <div class="col-md-9">
          <h3>Camionetas Jetty conectarán Lomas Verdes y Polanco.</h3>
          <p>Las camionetas tienen características similares en seguridad y estándar que Uber y Cabify pero el precio del viaje será menor, destacó Onésimo Flores, socio fundador de Jetty.</p>
          <a href="http://www.milenio.com/df/jetty-uber-cabify-camionetas-vans-lomas_verdes-polanco-noticias_milenio_0_1008499575.html" target="_blank">Ver artículo</a>
        </div>
      </div>

      <div class="row nota">
        <div class="col-md-3 logo-medios">
          <a href="http://expansion.mx/tecnologia/2017/08/09/app-de-transporte-colectivo-movilizara-trayectos-entre-cdmx-y-edomex" target="_blank"><img src="imgs-prensa/expansion.png"></a>
        </div>
        <div class="col-md-9">
          <h3>APP DE TRANSPORTE COLECTIVO MOVILIZARÁ TRAYECTOS ENTRE CDMX Y EDOMEX</h3>
          <p>La app Jetty ofrecerá servicio de transporte, tipo Uber, en camionetas entre ambos estados; la ruta inicial conectará a la zona de Lomas Verdes con Polanco.</p>
          <a href="http://expansion.mx/tecnologia/2017/08/09/app-de-transporte-colectivo-movilizara-trayectos-entre-cdmx-y-edomex" target="_blank">Ver artículo</a>
        </div>
      </div>

      <div class="row nota">
        <div class="col-md-3 logo-medios">
          <a href="https://www.forbes.com.mx/app-cambia-rumbo-del-transporte-publico/" target="_blank"><img src="imgs-prensa/forbes.png"></a>
        </div>
        <div class="col-md-9">
          <h3>Una app para cambiar el rumbo del transporte público</h3>
          <p>Jetty no cuenta con ningún vehículo propio, pero esta app de transporte quiere cambiar la calidad de los viajes de las personas que todos los días se trasladan del Estado de México a la Ciudad para trabajar.</p>
          <a href="https://www.forbes.com.mx/app-cambia-rumbo-del-transporte-publico/" target="_blank">Ver artículo</a>
        </div>
      </div>

      <div class="row nota">
        <div class="col-md-3 logo-medios">
          <a href="https://tyt.com.mx/noticias/integran-app-a-servicios-de-svbus/" target="_blank"><img src="imgs-prensa/transportes.png"></a>
        </div>
        <div class="col-md-9">
          <h3>Intengran App a servicios de SVBus</h3>
          <p>La plataforma de origen mexicano Jetty formó una alianza con SVBUS, empresa dedicada a operar los autobuses que circulan por el segundo piso y por la Supervía Poniente, en la Ciudad de México, en la que los usuarios de este servicio pueden reservar asientos en dichos buses.</p>
          <a href="https://tyt.com.mx/noticias/integran-app-a-servicios-de-svbus/" target="_blank">Ver artículo</a>
        </div>
      </div>

      <div class="row nota">
        <div class="col-md-3 logo-medios">
          <a href="https://www.vanguardia.com.mx/articulo/jetty-lanzan-app-de-servicio-colectivo-en-polanco" target="_blank"><img src="imgs-prensa/vanguardia.png"></a>
        </div>
        <div class="col-md-9">
          <h3>Jetty: Lanzan app de servicio colectivo en Polanco</h3>
          <p>Llega una buena noticia para los empleados de la zona de Polanco, una nueva aplicación para teléfonos inteligentes busca ofrecer una mejor opción de transporte colectivo.</p>
          <a href="https://www.vanguardia.com.mx/articulo/jetty-lanzan-app-de-servicio-colectivo-en-polanco" target="_blank">Ver artículo</a>
        </div>
      </div>

      <div class="row nota">
        <div class="col-md-3 logo-medios">
          <a href="https://www.animalpolitico.com/blogueros-ciudad-posible/2017/08/10/ciudad-mexico-te-presentamos-jetty/" target="_blank"><img src="imgs-prensa/animal-politico.png"></a>
        </div>
        <div class="col-md-9">
          <h3>Jetty: Lanzan app de servicio colectivo en Polanco</h3>
          <p>Todos los días 1.7 millones de personas viajan del Estado de México, donde están las viviendas asequibles, a la CDMX, donde están las oportunidades de empleo. </p>
          <a href="https://www.animalpolitico.com/blogueros-ciudad-posible/2017/08/10/ciudad-mexico-te-presentamos-jetty/" target="_blank">Ver artículo</a>
        </div>
      </div>


    </div>

  </div>

</div>
