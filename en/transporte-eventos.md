---
layout: default-en
title: Jetty | La Mejor Solución de Transporte para tu Evento
description: Organizas o vas a un evento y estás buscando transporte. ¿Consideras rentar una camioneta con chofer? Nos adaptamos a tu necesidad al mejor precio.
id: transporte-eventos
---

<div class="header-eventos menos-head-en">
  <div class="container header-content-organizaciones">
    <div class="row">
      <div class="col-md-12 text-center">
        <h1>Are you looking for a charter?</h1>
        <!-- <h1>¿Consideras rentar una camioneta con chofer?</h1> -->
      </div>
      <div class="col-md-8 col-md-offset-2 text-center">
        <h3>We can adapt to your needs at the best price to solve your transportation needs.</h3>
        <button type="button" class="btn btn-default btn-gray" data-toggle="modal" data-target="#ModalEventos">
          Quote
        </button>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid eventos-content backgrayblue-down">
  <div class="container eventos">
    <div class="row">
      <div class="col-md-8 col-md-offset-2 text-center">
        <!-- <h2>Ya sea que organices o vayas a un evento masivo, tenemos la solución para ti.</h2> -->
        <br>
        <br>
        <br>
        <br>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 eventos">
        <ul>
          <li>
            <figure>
              <img src="img/evento-deportivo.jpg" alt="Jetty, Evento deportivo">
              <figcaption>
                <h3>Sport Events</h3>
              </figcaption>
            </figure>
          </li>
          <li>
            <figure>
              <img src="img/evento-boda.jpg" alt="jetty, Evento boda">
              <figcaption>
                <h3>Wedding</h3>
              </figcaption>
            </figure>
          </li>
          <li>
            <figure>
              <img src="img/evento-concierto.jpg" alt="jetty, Evento Concierto">
              <figcaption>
                <h3>Concert</h3>
              </figcaption>
            </figure>
          </li>
          <li>
            <figure>
              <img src="img/evento-empresarial.jpg" alt="jetty, Evento Empresarial">
              <figcaption>
                <h3>Business Event</h3>
              </figcaption>
            </figure>
          </li>
          <li>
            <figure>
              <img src="img/evento-ecoturismo.jpg" alt="jetty, Evento ecoturismo">
              <figcaption>
                <h3>Tourism</h3>
              </figcaption>
            </figure>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>


<div class="container-fluid content-buen-viaje">
  <div class="container buen-viaje">
    <div class="row">
      <div class="col-md-12 text-center" data-aos="fade-up">
        <h1>BENEFITS</h1>
      </div>
    </div>

    <div class="row buen-viaje-info">
      <div class="col-md-4 text-center">
        <img src="img/mas-autos.svg">
        <p class="lead">Travel in fewer cars and have fun with your friends</p>
      </div>
      <div class="col-md-4 text-center">
      <img src="img/drink.svg">
        <p class="lead">Do not worry about the designated driver.</p>
      </div>
      <div class="col-md-4 text-center">
        <img src="img/mas-flexible.svg">
        <p class="lead">We adapt to what you need.</p>
      </div>
    </div>

    <div class="row buen-viaje-info">
      <div class="col-md-6 col-md-offset-3 text-center" data-aos="fade-up" >
        <button type="button" class="btn btn-default btn-lg btn-green" data-toggle="modal" data-target="#ModalEventos">
          Quote
        </button>
      </div>
    </div>

  </div>
</div>

<div class="clearfix"></div>

<div class="space-greenUp">
  <img src="img/back-grayblue.png">
</div>

<div class="container eventos-servicios">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <h2 class="text-center">Our service includes:</h2>
    </div>

    <div class="col-md-4 text-center">
      <img src="img/chofer.png" class="img-servicios">
      <h3>Professional Drivers</h3>
      <p>The driver underwent a strict selection process that included interviews, toxicological tests and psychometric evaluations, among others.</p>
    </div>
    <div class="col-md-4 text-center">
      <img src="img/interiores.png" class="img-servicios">
      <h3>Equipment</h3>
      <p>Air conditioning in cabin and for passengers, 4 USB chargers per row, GPS, Security cameras indoors and outdoors.</p>
    </div>
    <div class="col-md-4 text-center">
      <img src="img/camionetas.png" class="img-servicios">
      <h3>New Vehicles</h3>
      <p>Crafter VW - 19pax <br> Transit Ford - 14 y 17pax</p>
    </div>

    <div class="col-md-8 col-md-offset-2 text-center">
      <button type="button" class="btn btn-default btn-green" data-toggle="modal" data-target="#ModalEventos">
        Quote
      </button>
    </div>
  </div>
</div>


<!-- Modal Eventos -->
<div class="modal fade" id="ModalEventos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Déjanos tus datos y nos pondremos en contacto</h4>
      </div>

      <form class="js--form-events">
        <div class="modal-body">

            <div class="col-md-6 form-group">
              <label  for="name">Nombre</label>
              <input  type="text"
                      class="form-control"
                      required
                      maxlength="30"
                      name="name"
                      placeholder="Nombre" />
            </div>

            <div class="col-md-6 form-group">
              <label  for="lastname">Apellido</label>
              <input  type="text"
                      class="form-control"
                      required
                      maxlength="60"
                      name="lastname"
                      placeholder="Apellido" />
            </div>

            <div class="col-md-6 form-group">
              <label for="mail">Correo electrónico</label>
              <input  type="email"
                      class="form-control"
                      required
                      maxlength="60"
                      name="email"
                      placeholder="Correo electrónico" />
            </div>

            <div class="col-md-6 form-group">
              <label for="cellphone">Número telefónico</label>
              <input  type="text"
                      class="form-control"
                      required
                      maxlength="20"
                      name="phone"
                      placeholder="Número telefónico">
            </div>

            <div class="col-md-12 form-group">
              <label for="eventname">¿Cómo se llama el evento?</label>
              <input  type="text"
                      class="form-control"
                      required
                      maxlength="150"
                      name="eventname"
                      placeholder="">
            </div>

            <div class="col-md-6 form-group">
              <label for="company">¿Eres uno de los organizadores?</label>
              <div class="radio">
                <label>
                  <input  type="radio"
                          name="organizer"
                          value="true"
                          checked>
                  Sí
                </label>
              </div>
              <div class="radio">
                <label>
                  <input  type="radio"
                          name="organizer"
                          value="false">
                  No
                </label>
              </div>
            </div>

            <div class="col-md-6 form-group">
              <label for="company">¿Cuál es tu necesidad?</label>
              <div class="radio">
                <label>
                  <input  type="radio"
                          name="trip"
                          value="Viaje sencillo" checked>
                  Viaje sencillo
                </label>
              </div>
              <div class="radio">
                <label>
                  <input  type="radio"
                          name="trip"
                          value="Viaje redondo">
                  Viaje redondo
                </label>
              </div>
              <div class="radio">
                <label>
                  <input  type="radio"
                          name="trip"
                          value="Vueltas constantes">
                  Vueltas constantes
                </label>
              </div>
            </div>

            <div class="col-md-12 form-group">
              <label for="company">¿Número de asistentes?</label>
            </div>
            <div class="col-md-6 form-group">
              <div class="radio">
                <label>
                  <input  type="radio"
                          name="people"
                          value="1-14" checked>
                  1 a 14
                </label>
              </div>
              <div class="radio">
                <label>
                  <input  type="radio"
                          name="people"
                          value="15-19">
                  15 a 19
                </label>
              </div>
              <div class="radio">
                <label>
                  <input  type="radio"
                          name="people"
                          value="20-28">
                  20 a 28
                </label>
              </div>
            </div>

            <div class="col-md-6 form-group">
              <div class="radio">
                <label>
                  <input  type="radio"
                          name="people"
                          value="29-38">
                  29 a 38
                </label>
              </div>
              <div class="radio">
                <label>
                  <input  type="radio"
                          name="people"
                          value="39-58">
                  39 a 58
                </label>
              </div>
              <div class="radio">
                <label>
                  <input  type="radio"
                          name="people"
                          value="+59">
                  Más de 59
                </label>
              </div>
            </div>

            <div class="col-md-12 form-group">
              <label for="adress">Dirección de salida</label>
              <input  type="text"
                      class="form-control"
                      name="origin_address"
                      maxlength="200"
                      placeholder="Calle, Nº, Colonia, Delegación, Ciudad, Estado" />
            </div>

            <div class="col-md-6 form-group">
              <label for="datetimepicker-event-1">Fecha de salida</label>
              <div class='input-group date'>
                <input  type='text'
                        class="form-control event-date-picker"
                        id='datetimepicker-event-1'
                        maxlength="20"
                        name="origin_date"
                        placeholder="Día / Mes / Año"/>
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
              </div>
            </div>

            <div class="col-md-6 form-group">
              <label for="timepicker-event-1">Hora de salida</label>
              <input  type="text"
                      id="timepicker-event-1"
                      maxlength="20"
                      class="form-control event-time-picker"
                      name="origin_time"
                      placeholder="AM / PM" />
            </div>

            <div class="col-md-12 form-group">
              <label for="adress">Dirección de Llegada</label>
              <input  type="text"
                      class="form-control"
                      maxlength="200"
                      name="destination_address"
                      placeholder="Calle, Nº, Colonia, Delegación, Ciudad, Estado" />
            </div>

            <div class="col-md-6 form-group">
              <label for="datetimepicker-event-2">Fecha de regreso  </label>
              <div class='input-group' data-date-format="dd/mm/yyyy">
                <input  type='text'
                        class="form-control event-date-picker"
                        id='datetimepicker-event-2'
                        maxlength="20"
                        name="destination_date"
                        placeholder="Día / Mes / Año" />
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
              </div>
            </div>

            <div class="col-md-6 form-group">
              <label for="timepicker-event-2">Hora de regreso </label>
              <input  type="text"
                      class="form-control event-time-picker"
                      id="timepicker-event-2"
                      maxlength="20"
                      name="destination_time"
                      placeholder="AM / PM">
            </div>

        </div>
        <div class="clearfix"></div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
          <button type="submit" class="btn btn-green-small">Enviar</button>
        </div>
      </form>

    </div>
  </div>
</div>

 <!-- Modal Conductor -->
 <div id="ModalSuccess" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Recibimos tu solicitud</h4>
      </div>

      <div class="modal-body text-center">
        <div class="row">
          <div class="col-md-12  .center">
              <h5>Nos pondremos en contacto contigo dentro de poco.</h5>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
