---
layout: default-en
title: Jetty | Soluciona El Transporte de Personal de tu Empresa
description: Rentar una camioneta con chofer puede salir caro. Resolvemos tu transporte empresarial. Sin plazo forzoso ni presupuesto mínimo.
id: transporte-personal
---

<div class="header-organizaciones">
  <div class="container header-content-organizaciones">
    <div class="row">
      <div class="col-md-9" data-aos="fade-left" data-aos-easing="ease-out-sine" data-aos-duration="1000">
        <div class="text-fadein text-organizaciones">
          <h2>¿Está considerando contratar los servicios de una empresa de transporte de personal?</h2>
          <h2>¿Le preocupa que no sea eficiente?</h2>
          <h2>¿No quiere firmar un contrato a plazo forzoso con una empresa de transporte empresarial?</h2>
          <h2>¿No tiene suficiente presupuesto dedicado al transporte?</h2>
        </div>
        <p class="lead">Jetty se adapta a su necesidad bajo diferentes esquemas para alcanzar los mejores resultados.</p>
        <button type="button" class="btn btn-default btn-gray" data-toggle="modal" data-target="#ModalOrganizaciones">
          Solicita información
        </button>
      </div>
      <div class="col-md-3"></div>
    </div>
  </div>
</div>

<div class="container valor">
  <div class="row">
    <div class="col-md-4">
      <div class="text-center">
        <img src="img/icon-tecnologia.svg" alt="Jetty, Tecnología">
        <h2>Tecnología</h2>
      </div>
      <p>Con la <b>app de Jetty,</b> es más fácil revisar y reservar tus trayectos. Además puedes <b>monitorear la ubicación</b> de tu Jetty.</p>
    </div>
    <div class="col-md-4">
      <div class="text-center">
        <img src="img/icon-seguridad.svg">
        <h2>Vehículos de primera</h2>
      </div>
      <p>Todos cuentan con <b>cinturones de seguridad</b> en todos los asientos, AC, USBs, GPS, y<b> cámaras de seguridad.</b></p>
    </div>
    <div class="col-md-4">
      <div class="text-center">
        <img src="img/icon-choferes.svg">
        <h2>Conductores expertos</h2>
      </div>
      <p>Todos pasan por un <b>estricto proceso de selección</b> que incluye entrevistas, exámenes toxicológicos y evaluaciones psicométricas.</p>
    </div>
    <div class="col-md-12 text-center">
      <button type="button" class="btn btn-default btn-gray" data-toggle="modal" data-target="#ModalOrganizaciones">
        Solicita información
      </button>
    </div>
  </div>
</div>

<div class="container porque-content">
  <div class="row">
    <div class="col-md-12 text-center porque-title" data-aos="fade-up">
      <h2>Beneficios para tu organización</h2>
    </div>
  </div>

  <div class="row cuatro-puntos">
    <div class="col-md-3 porque text-center" data-aos="fade-up" data-aos-duration="200">
      <img src="img/engrane.svg">
      <p><strong>Aumenta la productividad</strong></p>
      <p>Viajando en Jetty puedes descansar durante el viaje o ponerte al corriente de tus correos.</p>
    </div>
    <div class="col-md-3 porque text-center" data-aos="fade-up" data-aos-duration="300">
      <img src="img/mundo.svg">
      <p><strong>Responsabilidad social</strong></p>
      <p>Impulsa el uso de un transporte más amigable con el medio ambiente.</p>
    </div>
    <div class="col-md-3 porque text-center" data-aos="fade-up" data-aos-duration="400">
      <img src="img/alcancia.svg">
      <p><strong>Grandes ahorros</strong></p>
      <p>Con Jetty, puedes ahorrar en el costo de los traslados y estacionamientos.</p>
    </div>
    <div class="col-md-3 porque text-center" data-aos="fade-up" data-aos-duration="500">
      <img src="img/manos.svg">
      <p><strong>Usa a Jetty como un arma de reclutamiento</strong></p>
      <p>El transporte es uno de los factores más importante para aceptar un trabajo.</p>
    </div>
  </div>

  <div class="row tres-puntos">
    <div class="col-md-12 text-center tres-puntos-title" data-aos="fade-up" data-aos-duration="1000">
      <h2>Jetty no es un servicio de transporte tradicional</h2>
      <p class="lead">
        <b>Te ofrecemos diferentes esquemas para adaptarnos a las necesidades de tu organización:</b>
      </p>
    </div>

    <div class="col-md-4 text-center" data-aos="fade-right" data-aos-duration="1000">
      <p>Promueve los servicios de Jetty. Podemos crearte un código de descuento</p>
    </div>
    <div class="col-md-4 text-center middle" data-aos="fade-up" data-aos-duration="1000">
      <p>Cotiza servicios exclusivos para la gente de tu organización.</p>
    </div>
    <div class="col-md-4 text-center" data-aos="fade-left" data-aos-duration="1000">
      <p>Subsidia parte o el 100% de viajes de tus colaboradores.</p>
    </div>
  </div>

  <div class="row tres-puntos">
    <div class="col-md-12 text-center" data-aos="fade-up">
      <!-- Button trigger modal -->
      <button type="button" class="btn btn-default btn-gray" data-toggle="modal" data-target="#ModalOrganizaciones">
        Solicita información
      </button>
    </div>
  </div>
</div>

<!-- Modal Organizaciones -->
<div class="modal fade" id="ModalOrganizaciones" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Déjanos tus datos y nos pondremos en contacto</h4>
      </div>

      <form id="form-company">
        <div class="modal-body">

            <div class="col-md-6 form-group">
              <label  for="name">Nombre</label>
              <input  type="text"
                      class="form-control"
                      maxlength="30"
                      required
                      name="name"
                      placeholder="Nombre">
            </div>

            <div class="col-md-6 form-group">
              <label  for="lastname">Apellido</label>
              <input  type="text"
                      class="form-control"
                      maxlength="60"
                      required
                      name="lastname"
                      placeholder="Apellidos">
            </div>

            <div class="col-md-6 form-group">
              <label for="mail">Correo electrónico</label>
              <input  type="email"
                      pattern=".+@*.*"
                      class="form-control"
                      required
                      maxlength="60"
                      name="email"
                      placeholder="Correo electrónico">
            </div>

            <div class="col-md-6 form-group">
              <label for="phone">Número telefónico</label>
              <input  type="text"
                      class="form-control"
                      required
                      maxlength="20"
                      name="phone"
                      placeholder="Número telefónico">
            </div>

            <div class="col-md-12 form-group">
              <label  for="company">Nombre de la Institución</label>
              <input  type="text"
                      class="form-control"
                      name="company"
                      required
                      maxlength="120"
                      placeholder="Compañía">
            </div>

            <div class="col-md-12 form-group">
              <label for="company">Dirección de la Institución</label>
              <input  type="text"
                      class="form-control"
                      name="company_address"
                      maxlength="200"
                      placeholder="Calle, Nº, Colonia, Delegación, Ciudad, Estado">
            </div>

            <div class="col-md-6 form-group">
              <label  for="schedule_in">Horario de entrada</label>
              <input  type="text"
                      id="timepicker-org-1"
                      maxlength="20"
                      class="form-control"
                      name="departure_hour"
                      placeholder="AM / PM" />
            </div>

            <div class="col-md-6 form-group">
              <label  for="schedule_out">Horario de salida</label>
              <input  type="text"
                      id="timepicker-org-2"
                      maxlength="20"
                      class="form-control"
                      name="return_hour"
                      placeholder="AM / PM" />
            </div>

            <div class="col-md-12 form-group">
              <label for="company">Número estimado de personas interesadas en el servicio:</label>
              <input  type="text"
                      class="form-control"
                      name="people"
                      required
                      maxlength="120"
                      placeholder="">
            </div>

            <div class="col-md-12 form-group">
              <label for="company">¿Uno o varios de <a href="/cobertura" target="_blank">nuestros servicios actuales </a>cumplen con la necesidad de los interesados?</label>
              <div class="radio">
                <label>
                  <input  type="radio"
                          name="match_service"
                          value="true"
                          checked>
                  Sí
                </label>
              </div>
              <div class="radio">
                <label>
                  <input  type="radio"
                          name="match_service"
                          value="false">
                  No
                </label>
              </div>
            </div>

        </div>
        <div class="clearfix"></div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
          <button type="submit" class="btn btn-green-small">Enviar</button>
        </div>
      </form>

    </div>
  </div>
</div>

<!-- Modal Conductor -->
<div id="ModalSuccess" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Recibimos tu solicitud</h4>
      </div>

      <div class="modal-body text-center">
        <div class="row">
          <div class="col-md-12  .center">
              <h5>Nos pondremos en contacto contigo dentro de poco.</h5>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>