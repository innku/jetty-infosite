---
layout: default-cobertura-en
title: Jetty | El transporte que mereces
description: Nuestra misión es ofrecer una alternativa de transporte privado seguro, confiable y accesible. Actualmente ofrecemos soluciones de traslado para personas en viajan entre las zonas de Aragón, Cuautitlán, Condesa, Narvarte, Santa Fe, Polanco.
id: vacantes
---

<div class="container marginTop">
  <div class="row prensa">
    <div class="col-md-12">
      <h1>SÚMATE AL EQUIPO</h1>
      <br>
      <p>En <a href="{{site.baseurl}}/">Jetty</a> estamos construyendo soluciones tecnológicas para mejorar la experiencia de traslado en la ciudad. Nuestra misión es ofrecer una alternativa de transporte privado seguro, confiable y accesible. Actualmente ofrecemos soluciones de traslado para personas en viajan entre las zonas de Aragón, Cuautitlán, Condesa, Narvarte, Santa Fe y Polanco, y próximamente tendremos una cobertura de servicio mayor en el área metropolitana del Valle de México.</p>
    </div>

    <div class="col-md-10 col-md-offset-1 content-vacantes">

      <!-- VACANTE 1 -->
      <!-- <div class="panel panel-default vacante">
        <div class="panel-body">

          <div class="col-md-1">
            <img src="img/vacante-soporte.png" width="60">
          </div>

          <div class="col-md-11">
            <div class="row">
              <div class="col-md-8">
                <p><b>PUESTO</b><br>
                 Ejecutivo de Soporte a clientes</p>
              </div>
              <div class="col-md-4 descripcion-btn-aling">
                <a class="btn btn-primary btn-green-small" role="button" data-toggle="collapse" href="#collapse_0" aria-expanded="false" aria-controls="collapse_0">
                Ver descripción
              </a>
              </div>
            </div>

            <div class="collapse" id="collapse_0">
              <div class="descripcion">
                <p><b>PROPÓSITO GENERAL DEL PUESTO</b>
                <br>
                Ofrecer soporte a los usuarios de Jetty y al equipo de operaciones.</p>

                <br>

                <p><b>PRINCIPALES FUNCIONES</b></p>
                <ul>
                  <li>
                    <p>Dar seguimiento a los usuarios sobre el servicio de Jetty por medio de diversos canales de comunicación (ej. Sistema de mensajería, redes sociales, correo electrónico, chat y teléfono).</p>
                  </li>
                  <li>
                    <p>Revisar, clasificar, asignar y/o solucionar cada solicitud en coordinación con el área indicada (operaciones, tecnología, marketing, socio-operador, cliente institucional, etc.).</p>
                  </li>
                  <li>
                    <p>Dar seguimiento a las unidades y conductores poder informar a los usuarios del estatus de su viaje.</p>
                  </li>
                  <li>
                    <p>Documentar las preguntas y crear respuestas predefinidas para los usuarios. </p>
                  </li>
                  <li>
                    <p>Colaborar con equipos multidisciplinarios (marketing, operaciones, tecnología) para definir, diseñar e implementar nuevas funciones para mejorar la experiencia de usuario. </p>
                  </li>
                  <li>
                    <p>Mantenerse al tanto de los procesos y herramientas de nuestras plataformas (apps de usuario y conductores).</p>
                  </li>
                  <li>
                    <p>Trabajar en colaboración con colegas y otros departamentos para agilizar procesos.</p>
                  </li>
                  <li>
                    <p>Fomentar la lealtad entre nuevos usuarios.</p>
                  </li>
                  <li>
                    <p>Ayudar en la publicación de recorridos, y modificación de horarios y trayectos de viaje cuando sea necesario. </p>
                  </li>
                  <li>
                    <p>Dar de alta vehículos o modificar la información dentro de la plataforma de administración.</p>
                  </li>
                  <li>
                    <p>Mantener actualizada la información de los conductores vigentes.</p>
                  </li>
                  <li>
                    <p>Apoyar en las capacitaciones a conductores.</p>
                  </li>
                  <li>
                    <p>El porcentaje de incidentes resueltos al primer contacto como indicador de desempeño a medir.</p>
                  </li>
                </ul>

                <br>

                <p><b>REQUISITOS</b></p>
                <ul>
                  <li>
                    <p>Puntualidad</p>
                  </li>
                  <li>
                    <p>Escolaridad: Licenciatura concluida.</p>
                  </li>
                  <li>
                    <p>Experiencia mínima de un año en servicio al cliente, telemarketing o ventas.</p>
                  </li>
                  <li>
                    <p>Conocimiento en paquetería de Microsoft Office básica.</p>
                  </li>
                  <li>
                    <p>Inglés intermedio (escrito, comprensión, lectura).</p>
                  </li>
                  <li>
                    <p>Disponibilidad de horarios.</p>
                  </li>
                </ul>

                <br>

                <p><b>HABILIDADES</b></p>
                <ul>
                  <li>
                    <p>Excepcionales habilidades de comunicación. Es muy importante que esté en constante comunicación con el equipo de negocio para poder entender las necesidades del cliente y proponer soluciones innovadoras a los problemas. </p>
                  </li>
                  <li>
                    <p>Pensamiento estructurado, organizado, motivado por la innovación y proactivo. Ánimo constante por aprender y desarrollar nuevas habilidades junto con el resto del equipo.</p>
                  </li>
                  <li>
                    <p>Comodidad trabajando en una cultura de inicio con un entorno de alto crecimiento / ágil.</p>
                  </li>
                  <li>
                    <p>Una mente aguda y un espíritu emprendedor, capaz de trabajar de forma autónoma con un mínimo de orientación y de impulsar soluciones a problemas. </p>
                  </li>
                  <li>
                    <p>Ánimo constante por aprender y desarrollar nuevas habilidades junto con el resto del equipo.</p>
                  </li>
                  <li>
                    <p>Enfoque orientado al cliente y a la resolución de problemas. </p>
                  </li>
                  <li>
                    <p>Habilidad de administración de sus tiempos y objetivos.</p>
                  </li>
                </ul>

                <br>

                <p><b>PRINCIPALES RELACIONES INTERNAS Y EXTERNAS</b></p>
                <ul>
                  <p><b>Relaciones internas:</b></p>
                  <li>
                    <p>Comunicación y colaboración con las áreas de tecnología, operaciones y marketing.</p>
                  </li>
                </ul>

                <br>

                <ul>
                  <p><b>Relaciones externas:</b></p>
                  <li>
                    <p>Interacción con usuarios y conductores. </p>
                  </li>
                </ul>

                <br>

                <p><b>INDICADORES CRÍTICOS DEL PUESTO.</b></p>
                <ul>
                  <li>
                    <p>Tiempo de respuesta a usuarios.</p>
                  </li>
                  <li>
                    <p>Porcentaje de respuesta a la primera  interacción de los usuarios (comentarios y/o dudas).</p>
                  </li>
                </ul>

                <br>

                <p><b>DATOS GENERALES DEL PUESTO:</b></p>
                <ul>
                  <p><b>1. Jornada Laboral</b></p>
                  <li>
                    <p>Lunes a Viernes.</p>
                  </li>
                  <li>
                    <p>Horario: 2pm-10pm.</p>
                  </li>
                  <li>
                    <p>40 horas a la semana.</p>
                  </li>
                  <li>
                    <p>Horario definido por la Operación.</p>
                  </li>
                  <li>
                    <p>6 días de vacaciones al cumplir el primer año de trabajo, incrementos según la ley correspondiente.</p>
                  </li>
                </ul>

                <br>

                <ul>
                  <p><b>2. Remuneración económica y Prestaciones</b></p>
                  <li>
                    <p>Pago por medio de tarjeta de nómina en cada quincena.</p>
                  </li>
                  <li>
                    <p>Prestaciones de ley: Cotización en IMSS e INFONAVIT, prima vacacional de 1 mes y aguinaldo.</p>
                  </li>
                </ul>

                <br>

                <p><b>¿TE INTERESA?</b></p>
                <ul>
                  <li>
                    <p>Escríbenos a <a href="mailto:hola@jetty.mx?subject=Atención a usuarios">hola@jetty.mx</a> utilizando <b>“Atención a usuarios”</b> como asunto del correo. Adjunta tu CV y un párrafo de por qué te interesa la vacante.</p>
                  </li>
                </ul>

              </div>
            </div>
          </div>

        </div>
      </div> -->


      <!-- VACANTE 3 IOS -->
      <div class="panel panel-default vacante">
        <div class="panel-body">

          <div class="col-md-1">
            <img src="img/vacante-developerjr.png" width="60">
          </div>

          <div class="col-md-11">
            <div class="row">
              <div class="col-md-8">
                <p><b>PUESTO</b><br>
                 Desarrollador/a móvil - iOS</p>
              </div>
              <div class="col-md-4 descripcion-btn-aling">
                <a class="btn btn-primary btn-green-small" role="button" data-toggle="collapse" href="#collapse_1" aria-expanded="false" aria-controls="collapse_1">
                Ver descripción
              </a>
              </div>
            </div>

            <div class="collapse" id="collapse_1">
              <div class="descripcion">
                <p><b>RESPONSABILIDADES</b></p>
                <ul>
                  <li>
                    <p>Como ingeniero/a de software estarás a cargo del desarrollo de la app de Jetty para iOS.</p>
                  </li>
                  <li>
                    <p>Construir componentes y funcionalidades de la aplicación de Jetty para iOS.</p>
                  </li>
                  <li>
                    <p>Ayudar a mejorar la calidad del código mediante pruebas unitarias y automatización.</p>
                  </li>
                  <li>
                    <p>Trabajar con el equipo de diseño para entender los requerimientos del usuario final.</p>
                  </li>
                  <li>
                    <p>Aportar tu conocimiento, creatividad y experiencia en reuniones de planeación y seguimiento a los procesos de desarrollo de software.</p>
                  </li>
                </ul>

                <br>

                <p><b>EXPERIENCIA Y CONOCIMIENTOS NECESARIOS</b></p>
                <ul>
                  <li>
                    <p>Fundamentos sólidos de  Swift 4/5 y Xcode.</p>
                  </li>
                  <li>
                    <p>Manejo de iOS Frameworks (CoreData, CoreAnimation, AVFoundation, etc).</p>
                  </li>
                  <li>
                    <p>Conocimiento de estándares de codificación iOS y Human Interface guidelines de Apple.</p>
                  </li>
                  <li>
                    <p>Conocimiento en el proceso de publicación en la App Store</p>
                  </li>
                  <li>
                    <p>Experiencia con Cocoapods.</p>
                  </li>
                  <li>
                    <p>Experiencia en programación orientada a objetos sobre el paradigma de Model View Controller.</p>
                  </li>
                  <li>
                    <p>Conocimiento en el consumo de Web services (REST)</p>
                  </li>
                  <li>
                    <p>Conocimiento de programación orientada a objetos o programación funcional y saber aplicarlos de manera eficiente.</p>
                  </li>
                  <li>
                    <p>Conocimiento de Git y Github.</p>
                  </li>
                  <li>
                    <p>Haber participado en procesos de desarrollo de software empleando metodologías ágiles.</p>
                  </li>
                  <li>
                    <p>Dominio del idioma Inglés (hablado, escrito, y leído).</p>
                  </li>
                </ul>

                <br>

                <p><b>HABILIDADES</b></p>
                <ul>
                  <li>
                    <p>Excepcionales habilidades de comunicación. Es muy importante que el desarrollador esté en constante comunicación con el equipo de negocio para poder entender sus necesidades y proponer soluciones innovadoras a los problemas. </p>
                  </li>
                  <li>
                    <p>Experiencia en el manejo de proyectos. </p>
                  </li>
                  <li>
                    <p>Gran capacidad de análisis para poder tomar decisiones conforme a los datos.</p>
                  </li>
                  <li>
                    <p>Ser humilde, autodidacta, estructurado, organizado, motivado por la innovación y proactivo. Ánimo constante por aprender y desarrollar nuevas habilidades junto con el resto del equipo.</p>
                  </li>
                  <li>
                    <p>Comodidad trabajando en una cultura de inicio con un entorno de alto crecimiento / ágil.</p>
                  </li>
                  <li>
                    <p>Una mente aguda, analítica y un espíritu emprendedor, capaz de trabajar de forma autónoma con un mínimo de orientación.</p>
                  </li>
                  <li>
                    <p>Ánimo constante por aprender y desarrollar nuevas habilidades junto con el resto del equipo.</p>
                  </li>
                </ul>

                <br>

                <p><b>¿TE INTERESA?</b></p>
                <p>Escríbenos a <a href="mailto:jorge@jetty.mx?subject=Desarrollador/a iOS">jorge@jetty.mx</a> utilizando <b>“Desarrollador/a móvil - iOS”</b> como asunto del correo. Envíanos cualquier link que nos ayude a conocerte (github, linkedin, cv).</p>

              </div>
            </div>
          </div>

        </div>
      </div>

      <!-- VACANTE 4 Android -->
      <div class="panel panel-default vacante">
        <div class="panel-body">

          <div class="col-md-1">
            <img src="img/vacante-developerjr.png" width="60">
          </div>

          <div class="col-md-11">
            <div class="row">
              <div class="col-md-8">
                <p><b>PUESTO</b><br>
                 Desarrollador/a móvil - Android</p>
              </div>
              <div class="col-md-4 descripcion-btn-aling">
                <a class="btn btn-primary btn-green-small" role="button" data-toggle="collapse" href="#collapse_android" aria-expanded="false" aria-controls="collapse_android">
                Ver descripción
              </a>
              </div>
            </div>

            <div class="collapse" id="collapse_android">
              <div class="descripcion">
                <p><b>RESPONSABILIDADES</b></p>
                <ul>
                  <li>
                    <p>Construir componentes y funcionalidades de la aplicación de Jetty para usuarios de Android.</p>
                  </li>
                  <li>
                    <p>Aportar tu conocimiento, creatividad y experiencia en reuniones de planeación y seguimiento a los procesos de desarrollo de software.</p>
                  </li>
                  <li>
                    <p>Ayudar a mejorar la calidad del código mediante pruebas unitarias y automatización.</p>
                  </li>
                  <li>
                    <p>Hacer el diseño conceptual y técnico de las soluciones en base a requerimientos de Jetty.</p>
                  </li>
                  <li>
                    <p>Trabajar con el equipo de diseño para entender los requerimientos del usuario final.</p>
                  </li>
                  <li>
                    <p>Participar en sesiones de planeación y tener la oportunidad de contribuir hacia el producto con ideas originales e innovadoras.</p>
                  </li>
                  <li>
                    <p>Atacar problemas complejos y entregar resultados exitosamente en tiempo y forma.</p>
                  </li>
                  <li>
                    <p>Asegurar la calidad de código y arquitectura tecnológica.</p>
                  </li>
                </ul>

                <br>

                <p><b>EXPERIENCIA Y CONOCIMIENTOS NECESARIOS</b></p>
                <ul>
                  <li>
                    <p>Trayectoria de 3 años o más desarrollando para Android, y recientemente con Kotlin.</p>
                  </li>

                  <li>
                    <p>Conocimiento profundo del SDK de Android, sus diferentes versiones, y sobre cómo desarrollar para diferentes tamaños de pantalla.</p>
                  </li>

                  <li>
                    <p>Dominio profundo de programación orientada a objetos, y experiencia en arquitectura “clean”.</p>
                  </li>

                  <li>
                    <p>Familiaridad con el ecosistema open source de Android y las librerías disponibles para tareas comunes.</p>
                  </li>

                  <li>
                    <p>Conocimiento de las Material Design Guidelines de Android.</p>
                  </li>

                  <li>
                    <p>Experiencia utilizando frameworks de Android para manipulación de sensores y gráficos.</p>
                  </li>

                  <li>
                    <p>Experiencia con almacenamiento offline, cómputo paralelo y optimización de rendimiento en Android.</p>
                  </li>

                  <li>
                    <p>Conocimiento avanzado de arquitectura y diseño de aplicaciones móviles distribuidas con el apoyo de REST APIs.</p>
                  </li>

                  <li>
                    <p>Experiencia publicando código para Android en producción mediante el Play Store de Google.</p>
                  </li>

                  <li>
                    <p>Conocimiento de Git.</p>
                  </li>

                  <li>
                    <p>Haber participado en procesos de desarrollo de software empleando metodologías ágiles.</p>
                  </li>

                  <li>
                    <p>Dominio del idioma Inglés (hablado, escrito, y leído).</p>
                  </li>
                </ul>

                <br>

                <p><b>HABILIDADES</b></p>
                <ul>
                  <li>
                    <p>Excepcionales habilidades de comunicación. Es muy importante que el desarrollador esté en constante comunicación con el equipo de negocio para poder entender sus necesidades y proponer soluciones innovadoras a los problemas. </p>
                  </li>
                  <li>
                    <p>Experiencia en el manejo de proyectos. </p>
                  </li>
                  <li>
                    <p>Gran capacidad de análisis para poder tomar decisiones conforme a los datos.</p>
                  </li>
                  <li>
                    <p>Ser humilde, autodidacta, estructurado, organizado, motivado por la innovación y proactivo. Ánimo constante por aprender y desarrollar nuevas habilidades junto con el resto del equipo.</p>
                  </li>
                  <li>
                    <p>Comodidad trabajando en una cultura de inicio con un entorno de alto crecimiento / ágil.</p>
                  </li>
                  <li>
                    <p>Una mente aguda, analítica y un espíritu emprendedor, capaz de trabajar de forma autónoma con un mínimo de orientación.</p>
                  </li>
                  <li>
                    <p>Ánimo constante por aprender y desarrollar nuevas habilidades junto con el resto del equipo.</p>
                  </li>
                </ul>

                <br>

                <p><b>¿TE INTERESA?</b></p>
                <p>Escríbenos a <a href="mailto:jorge@jetty.mx?subject=Desarrollador/a Android">jorge@jetty.mx</a> utilizando <b>“Desarrollador/a móvil - Android”</b> como asunto del correo. Envíanos cualquier link que nos ayude a conocerte (github, linkedin, cv).</p>

              </div>
            </div>
          </div>

        </div>
      </div>


      <!-- VACANTE 3 -->
      <div class="panel panel-default vacante">
        <div class="panel-body">

          <div class="col-md-1">
            <img src="img/vacante-chofer.png" width="60">
          </div>

          <div class="col-md-11">
            <div class="row">
              <div class="col-md-8">
                <p><b>PUESTO</b><br>
                Conductor ejecutivo</p>
              </div>
              <div class="col-md-4 descripcion-btn-aling">
                <a class="btn btn-primary btn-green-small" role="button" data-toggle="collapse" href="#collapse_3" aria-expanded="false" aria-controls="collapse_3">
                Ver descripción
              </a>
              </div>
            </div>

            <div class="collapse" id="collapse_3">
              <div class="descripcion">
                <p><b>PROPÓSITO GENERAL DEL PUESTO</b>
                <br>
                Manejar vehículos automotores, dentro y fuera de la ZMVM para el traslado de personas a diferentes áreas de servicio, checando las condiciones mecánicas de los vehículos asignados y realizando labores de carga de gasolina, revisión y regulación de niveles.</p>

                <p><b>LOCALIDAD</b>
                <br>
                CDMX</p>

                <p><b>EMPRESA QUE PRESTA EL SERVICIO DE CONTRATACIÓN </b>
                <br>
                Empresa de Outsourcing</p>

                <p style="margin-top: 22px"><b>PERFIL</b></p>
                <ul>
                  <p>Para hacer bien este trabajo, se requiere cumplir con las siguientes descripciones</p>
                  <li>
                    <p>Tener al menos 21 años cumplidos.</p>
                  </li>
                  <li>
                    <p>Proporcionar una licencia de conducir vigente “Tipo C”.</p>
                  </li>
                  <li>
                    <p>Habilidad para conducir.</p>
                  </li>
                  <li>
                    <p>Capaz de trabajar solo y de concentrarse durante largos períodos de tiempo.</p>
                  </li>
                  <li>
                    <p>Consciente de la seguridad y capaz de seguir métodos.</p>
                  </li>
                  <li>
                    <p>Responsable y capaz de seguir instrucciones (verbales o escritas) y cumplir con horarios.</p>
                  </li>
                  <li>
                    <p>Habilidades sociales y dotes para la atención al cliente.</p>
                  </li>
                  <li>
                    <p>Capacidad para sobrellevar un trabajo rutinario.</p>
                  </li>
                  <li>
                    <p>Capaz de seguir normativa en materia de transporte y seguridad.</p>
                  </li>
                  <li>
                    <p>Tener conocimientos básicos de mecánica automotriz (identificar el estado o la condición mecánica de su unidad).</p>
                  </li>
                </ul>

                <br>

                <p><b>FUNCIONES</b></p>
                <ul>
                  <li>
                    <p>Recoger/Entregar el vehículo en el encierro, revisar el estado de la unidad y llenar la bitácora de recolección.</p>
                  </li>
                  <li>
                    <p>Registro diario en la plataforma de conductores durante las horas de servicio.</p>
                  </li>
                  <li>
                    <p>Conducir diariamente vehículo para transporte de pasajeros.</p>
                  </li>
                  <li>
                    <p>Atender las rutas e indicaciones de ascenso y descenso de pasajeros a través de la plataforma y ofrecer servicio a los usuarios de Jetty.</p>
                  </li>
                  <li>
                    <p>Maniobrar el vehículo según disposiciones del Reglamento de Tránsito.</p>
                  </li>
                  <li>
                    <p>Ofrecer un trato atento y amable a los usuarios.</p>
                  </li>
                  <li>
                    <p>Dar mantenimiento diario al vehículo que conduce.</p>
                  </li>
                  <li>
                    <p>Limpiar la parte interna y externa del vehículo.</p>
                  </li>
                  <li>
                    <p>Verificar presión y vida de neumáticos, nivel del agua, aceite, combustible y otros líquidos lubricantes y abastece según sea necesario. Hará la nivelación en el tiempo y la forma que determine la empresa.</p>
                  </li>
                  <li>
                    <p>Realizar trabajos preventivos y en su caso correctivos de mecánica sobre fallas menores del vehículo a su cargo (cambios de llantas o baterías).</p>
                  </li>
                  <li>
                    <p>Checar las condiciones mecánicas del vehículo que vaya a conducir, cerciorándose de que la unidad esté en condiciones de circular y en su caso reportar a su supervisor las anomalías que detecte para su corrección.</p>
                  </li>
                  <li>
                    <p>Presentar informes a su superior en forma verbal ó escrita sobre daños y desperfectos del vehículo o cualquiera de los equipos que transporta (router, wi-fi, equipo de video, celular).</p>
                  </li>
                  <li>
                    <p>Abastecer el vehículo de combustible.</p>
                  </li>
                  <li>
                    <p>Entrega de facturas y comprobantes de gastos a los supervisores.</p>
                  </li>
                  <li>
                    <p>Debe realizar otras tareas relacionadas con las funciones de la unidad.</p>
                  </li>
                </ul>

                <br>

                <p><b>RESPONSABILIDADES</b></p>
                <ul>
                  <li>
                    <p>Cuidado y aseo personal.</p>
                  </li>
                  <li>
                    <p>Cuidado, mantenimiento y limpieza del vehículo.</p>
                  </li>
                  <li>
                    <p>Seguridad de personas.</p>
                  </li>
                  <li>
                    <p>Contacto con los usuarios y el equipo de Jetty.</p>
                  </li>
                  <li>
                    <p>Utilizar en todo momento el uniforme de Jetty.</p>
                  </li>
                </ul>

                <br>

                <p><b>EXPERIENCIA</b></p>
                <ul>
                  <li>
                    <p>Un (1) año de experiencia en la conducción de vehículos de transporte de pasajeros de dimensiones mayores o similares a Jetty (2 x 7 metros).</p>
                  </li>
                </ul>

                <br>

                <p><b>DOCUMENTACIÓN NECESARIA</b></p>
                <ul>
                  <li>
                    <p>Credencial de elector.</p>
                  </li>
                  <li>
                    <p>Carta de antecedentes no penales.</p>
                  </li>
                  <li>
                    <p>Estar dado de alta en el Servicio de Administración Tributaria (SAT).</p>
                  </li>
                  <li>
                    <p>Examen toxicológico o antidoping: Estos exámenes deben aprobarlos independientemente de haber aprobado el examen de conocimientos.</p>
                  </li>
                  <li>
                    <p>Exámen psicológico.</p>
                  </li>
                  <li>
                    <p>Aprobar una entrevista presencial con Jetty.</p>
                  </li>
                  <li>
                    <p>Acta de Nacimiento.</p>
                  </li>
                  <li>
                    <p>CURP</p>
                  </li>
                  <li>
                    <p>RFC</p>
                  </li>
                  <li>
                    <p>Comprobante de domicilio.</p>
                  </li>
                  <li>
                    <p>NSS (Número de seguro social)</p>
                  </li>
                </ul>

                <br>

                <p><b>¿TE INTERESA?</b></p>
                <p>Escríbenos a <a href="mailto:hola@jetty.mx?subject=Conductor ejecutivo">hola@jetty.mx</a> utilizando <b>“Conductor ejecutivo”</b> como asunto del correo. Adjunta tu CV y un párrafo de por qué te interesa la vacante.</p>

              </div>
            </div>

          </div>

        </div>
      </div>


    </div>

  </div>

</div>
