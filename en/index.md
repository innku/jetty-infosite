---
layout: default-en
title: Jetty | Soluciona tu Transporte Diario en México.
description: Aplicación de Transporte, Disfruta de un Traslado Cómodo, Rápido y Seguro de Manera Diaria a Bordo de  Camionetas Ejecutivas con Conductores Verificados.
image: https://www.jetty.mx/img/Jetty_MX.png
id: Home
---

<figure class="header">
  <img src="{{site.baseurl}}/img/back-header2.png" alt="Jetty MX" class="back">
  <figcaption>
    <div class="container">
      <div class="row">
        <div id="titleHead" class="col-md-5 col-sm-7" data-aos="fade" data-aos-duration="2500" data-aos-delay="300">
          <div class="text-fadein">
             <h2>Are you tired of wasting hours behind the wheel in traffic?</h2>
             <h2>Does it bother you to be standing on the bus?</h2>
             <h2>Are you afraid to travel by public transport?</h2>
             <h2>Stressed to see how the meter advances or the dynamic rates</h2>
          </div>
          <p class="txt-header-index">Forget your worries, travel safely and comfortably. Jetty will turn every journey into a good trip.</p>

          <p class="text-downapp-index">Download the app</p>

          <a href="https://itunes.apple.com/us/app/jetty-soluciona-tu-transporte/id1276413293?l=es&ls=1&mt=8" target="_blank" class="download-app hvr-shadow">
            <img src="/img/jetty-iOS-en.png" alt="Jetty Descargar iOS">
          </a>

          <a href="https://play.google.com/store/apps/details?id=mx.jetty.jetty" target="_blank" class="download-app hvr-shadow">
            <img src="/img/jetty-android-en.png" alt="Jetty Descargar Android">
          </a>
        </div>
        <div class="col-md-7 col-sm-5 van" data-aos="fade-right" data-aos-duration="1000" data-aos-easing="ease-out
        " >
          <img src="/img/jetty-mx-camioneta.png" alt="Jetty Van">
        </div>
      </div>
    </div>
  </figcaption>
</figure>

<div class="clearfix"></div>

<div class="container index-valor">
  <div class="row">
    <div class="col-md-4">
      <div class="text-center">
        <img src="/img/icon-shield.svg" alt="Jetty, Seguridad">
      </div>
      <h3>Safety</h3>
      <p>All our vehicles have surveillance cameras and are monitored via GPS.</p>
    </div>
    <div class="col-md-4">
      <div class="text-center">
        <img src="/img/icon-support.svg" alt="Jetty, Tranquilidads">
      </div>
      <h3>Peace of mind</h3>
      <p>Our drivers and support team are trained to give you the best service.</p>
    </div>
    <div class="col-md-4">
      <div class="text-center">
        <img src="/img/icon-gps.svg" alt="Jetty, Eficiencia">
      </div>
      <h3>Efficiency</h3>
      <p>Our trips are designed so that you can reach your destination in the most direct way.</p>
    </div>
  </div>
</div>

<div class="clearfix"></div>

<div>
  <img src="{{site.baseurl}}/img/back-gray-down.png" class="img-como-funciona">
</div>

<div class="container-fluid index-como-funciona">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <h1>How it <span>works</span></h1>
      </div>

      <div class="col-md-3" data-aos="fade-up" data-aos-easing="ease-in-sine" data-aos-duration="300">
        <div class="row">
          <div class="col-xs-2">
            <img src="img/pin-como.svg">
          </div>
          <div class="col-xs-10">
            <p>Drop the pin on your pickup and drop-off points.</p>
          </div>
        </div>
        <div class="text-center app-jetty">
          <img src="img/app-jetty-1.png" alt="Jetty, Cómo funciona">
        </div>
      </div>
      <div class="col-md-3" data-aos="fade-up" data-aos-easing="ease-in-sine" data-aos-duration="500">
        <div class="row">
          <div class="col-xs-2">
            <img src="img/reloj-como.svg">
          </div>
          <div class="col-xs-10">
            <p>Review and select the option that best suits you.</p>
          </div>
        </div>
        <div class="text-center app-jetty">
          <img src="img/app-jetty-2.png" alt="Jetty, Cómo funciona">
        </div>
      </div>
      <div class="col-md-3" data-aos="fade-up" data-aos-easing="ease-in-sine" data-aos-duration="800">
        <div class="row">
          <div class="col-xs-2">
            <img src="img/asiento-como.svg">
          </div>
          <div class="col-xs-10">
            <p>Book your trip and monitor your Jetty.</p>
            <br>
          </div>
        </div>
        <div class="text-center app-jetty">
          <img src="img/app-jetty-3.png" alt="Jetty, Cómo funciona">
        </div>
      </div>
      <div class="col-md-3" data-aos="fade-up" data-aos-easing="ease-in-sine" data-aos-duration="1000">
        <div class="row">
          <div class="col-xs-2">
            <img src="img/people-como.svg">
          </div>
          <div class="col-xs-10">
            <p>Walk to the boarding point and show your pass to the driver.</p>
          </div>
        </div>
        <div class="text-center app-jetty">
          <img src="img/app-jetty-4.png" alt="Jetty, Cómo funciona">
        </div>
      </div>

      <div class="col-md-12 text-center">
        <p class="lead text-downapp">Download the app and enjoy a good trip.</p>

        <a href="https://itunes.apple.com/us/app/jetty-soluciona-tu-transporte/id1276413293?l=es&ls=1&mt=8" target="_blank" class="download-app hvr-shadow">
          <img src="/img/jetty-iOS-en.png" alt="Jetty Descarga App iOS">
        </a>

        <a href="https://play.google.com/store/apps/details?id=mx.jetty.jetty" target="_blank" class="download-app hvr-shadow">
          <img src="/img/jetty-android-en.png" alt="Jetty Descarga App Android">
        </a>
      </div>
    </div>
  </div>
</div>

<div>
  <img src="img/back-grayblue-up.png" class="img-como-funciona">
</div>

<div class="clearfix"></div>

<div class="container-fluid content-buen-viaje">
  <div class="container buen-viaje">
    <div class="row">
      <div class="col-md-12" data-aos="fade-left">
        <h1>GOOD TRIP<br>
          <span>IS...</span>
        </h1>
      </div>
    </div>

    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
          <!-- Indicators -->
          <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
          </ol>

          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">
            <div class="item active text-center">
              <img src="img/buen-viaje-slide-1-en.png" alt="Jetty, buen viaje es..">
            </div>
            <div class="item text-center">
              <img src="img/buen-viaje-slide-2-en.png" alt="Jetty, buen viaje es..">
            </div>
          </div>

          <!-- Controls -->
          <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>
    </div>

    <div class="row buen-viaje-info">
      <div class="col-md-6 col-md-offset-3 text-center" data-aos="fade-up" >
      <a href="beneficios" class="btn btn-default btn-lg btn-green btn-beneficios">Learn more about our benefits</a>
      </div>
    </div>

  </div>
</div>

<div class="clearfix"></div>

<div class="space-greenUp">
  <img src="img/back-grayblue.png">
</div>

<div class="container testimonial">
   <div class="row">

    <div class="col-md-12 text-center">
      <h2>Testimonials</h2>
    </div>

      <div class="col-md-10 col-md-offset-1">
        <div id="carousel-testimonios" class="carousel slide" data-ride="carousel">
          <!-- Indicators -->
          <!-- <ol class="carousel-indicators">
            <li data-target="#carousel-testimonios" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-testimonios" data-slide-to="1"></li>
          </ol> -->

          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">
            <div class="item active text-center">
              <h3><img src="/img/comillas1.png" class="comillas"> A very professional driver. Excellent trip: comfortable and fast. Thank you. <img src="/img/comillas2.png" class="comillas"><br><span class="name">Andrea D.</span></h3>
            </div>
            <div class="item text-center">
              <h3><img src="/img/comillas1.png" class="comillas"> Great option, affordable and safe. Really improves my commute. I recommend it! <img src="/img/comillas2.png" class="comillas"><br><span class="name">Natalia M.</span></h3>
            </div>
            <div class="item text-center">
              <h3><img src="/img/comillas1.png" class="comillas"> Excellent as always. On time and without inconveniences. Thank you Jetty. <img src="/img/comillas2.png" class="comillas"><br><span class="name">Eunice G.</span></h3>
            </div>
          </div>

          <!-- Controls -->
          <!-- <a class="left carousel-control" href="#carousel-testimonios" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#carousel-testimonios" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a> -->
        </div>
      </div>
    </div>
</div>
