---
layout: default-en
title: Jetty | El transporte que mereces.
description: Aplicación de Transporte, Disfruta de un Traslado Cómodo, Rápido y Seguro de Manera Diaria a Bordo de  Camionetas Ejecutivas con Conductores Verificados.
id: quienes
---

<figure class="header-quienes">
  <img src="img/Back_Quienes.jpg" alt="Jetty MX" class="back">
  <figcaption>
    <div class="container header-content-quienes">
      <div class="row">
        <div class="col-md-10">
          <h1>Creamos e implementamos soluciones tecnológicas para eficientar y mejorar la calidad del transporte.</h1>
          <h2>Nuestro objetivo es mejorar la experiencia del pasajero al mismo tiempo que mejoramos la rentabilidad del empresario de transporte colectivo.</h2>
          <h3>Queremos que tengas un <br> <strong>Buen Viaje. Siempre.</strong></h3>
        </div>
      </div>
    </div>
  </figcaption>
</figure>

<div class="clearfix"></div>

<!-- <div class="space-greenUp">
  <img src="img/back-green-down.png">
</div> -->

<!-- <div class="nosotros">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <h1>¿Quiénes somos?</h1>
      </div>
    </div>
    <div class="row text-center equipo">
      <div class="col-md-3">
        <img src="img/Onesimo-Flores.jpg" alt="Onesimo Flores, Equipo Jetty">
        <h3><strong>Onésimo Flores</strong></h3>
        <p><strong>Socio fundador</strong></p>
        <p>Fue académico de MIT y es experto en transporte y desarrollo urbano.</p>
      </div>
      <div class="col-md-3">
        <img src="img/Diego-Guzman.jpg" alt="Diego Guzman, Equipo Jetty">
        <h3><strong>Diego Guzman</strong></h3>
        <p><strong>Director de Operaciones</strong></p>
        <p>Trabajó en arrendadoras y es experto en mejoras de calidad y reingeniería de procesos.</p>
      </div>
      <div class="col-md-3">
        <img src="img/Cristina-Palacios.jpg" alt="Cristina Palacios, Equipo Jetty">
        <h3><strong>Cristina Palacios</strong></h3>
        <p><strong>Socia y directora</strong></p>
        <p>Fue directora y fundadora de Aventones. Socia de www.innku.com</p>
      </div>
      <div class="col-md-3">
        <img src="img/Celestin-Soubrier.jpg" alt="Célestin Soubrier, Equipo Jetty">
        <h3><strong>Célestin Soubrier</strong></h3>
        <p><strong>Director de Marketing</strong></p>
        <p>Fue director de marketing en México de la empresa más grande de viajes compartidos.</p><br>
      </div>
    </div>

  </div>
</div> -->

<!-- <div class="space-greenUp">
  <img src="img/back-green-up.png">
</div> -->

<div class="container alianzas-content">
  <div class="row">
    <div class="col-md-10 col-md-offset-1 text-center">
      <h1>Alianzas</h1>
      <ul class="alianzas">
        <li>
          <a href="http://xibalbafestival.com" target="_blanck">
            <img src="imgs-prensa/logo-xibalba.png">
          </a>
        </li>
        <li>
          <a href="https://courses.platzi.com" target="_blanck">
            <img src="imgs-prensa/platzi.png">
          </a>
        </li>
        <li>
          <a href="#" target="_blanck">
            <img src="imgs-prensa/conecta-cuatro.png">
          </a>
        </li>
        <li>
          <a href="http://impact0.org/" target="_blanck">
            <img src="imgs-prensa/impacto.jpg">
          </a>
        </li>
        <li>
          <a href="http://mexicotravesias.com" target="_blanck">
            <img src="imgs-prensa/LOGO_TM.jpg">
          </a>
        </li>
        <li>
          <a href="https://www.latamobility.com/" target="_blanck">
            <img src="imgs-prensa/latam-mobility.png">
          </a>
        </li>
      </ul>
    </div>

    <div class="col-md-12 text-center" style="margin-top: 50px">
      <h1>Reconocimientos</h1>
      <ul class="alianzas">
        <li>
          <img src="imgs-prensa/30-promesas.png" alt="30 promesas de negocio" style="width: 140px">
        </li>
        <li>
          <img src="imgs-prensa/NEGRO.png">
        </li>
        <li>
          <a href="http://sharedusemobilitycenter.org/2018-summit/" target="_blank">
            <img src="{{site.baseurl}}/imgs-prensa/shared.png" alt="Shared Use Mobility" style="width: 250px;">
          </a>
        </li>
        <li>
          <a href="http://sharedusemobilitycenter.org/2018-summit/" target="_blank">
            <img src="{{site.baseurl}}/imgs-prensa/Startup-Spotlight.png" alt="Startup Spotlight 2018" style="width: 80px;">
          </a>
        </li>
        <li>
          <a href="https://www.worldsummitawards.org" target="_blank">
            <img src="{{site.baseurl}}/imgs-prensa/wsa_logo_2018_winner.png" alt="wsa winner 2018" style="width: 140px;">
          </a>
        </li>
        <li>
          <a href="https://pitchawards.com" target="_blank">
            <img src="{{site.baseurl}}/imgs-prensa/logo-Pitch-Awards.png" alt="Pitch Awards 2018" style="width: 160px;">
          </a>
        </li>
        <li>
          <a href="https://www.raeng.org.uk/grants-and-prizes/international-research-and-collaborations/newton-fund-programmes/leaders-in-innovation-fellowships" target="_blank">
            <img src="{{site.baseurl}}/imgs-prensa/Royal-Academy-Of-Engineering.png" alt="Royal Academy Of Engineering" style="width: 180px;">
          </a>
        </li>
        <li>
          <a href="https://www.kcurveprize.org" target="_blank">
            <img src="{{site.baseurl}}/imgs-prensa/keeling-curve.png" alt="Keeling Curve Prize" style="width: 180px;">
          </a>
        </li>
        <li>
          <a href="https://www.mitinclusiveinnovation.com" target="_blank">
            <img src="{{site.baseurl}}/imgs-prensa/MIT-IIC.png" alt="MIT Inclusive Innovation Challenge" style="width: 100px;">
          </a>
        </li>
      </ul>
      <!-- <a href="http://sharedusemobilitycenter.org/2018-summit/" target="_blank">
        <img src="imgs-prensa/shared.png" alt="Shared Use Mobility center" style="width: 200px; float:left">
        <img src="imgs-prensa/Startup-Spotlight.png" alt="Startup Spotlight 2018" style="width: 95px; float:left">
      </a> -->
    </div>

  </div>
</div>

<div class="clearfix"></div>

<div class="container reconocimientos">
  <div class="row">
    <div class="col-md-12 text-center">
      <h1>Jetty en la prensa</h1>
      <ul class="alianzas">
        <li>
          <img src="imgs-prensa/reforma.png">
        </li>
        <li>
          <img src="imgs-prensa/milenio.png">
        </li>
        <li>
          <img src="imgs-prensa/expansion.png">
        </li>
        <li>
          <img src="imgs-prensa/excelsior.png">
        </li>
        <li>
          <img src="imgs-prensa/forbes.png">
        </li>
      </ul>
      <a href="/prensa" class="btn btn-green">Prensa</a>
    </div>
  </div>
</div>


<div class="container mobility">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="row">
        <div class="col-md-3">
          <img src="img/logo-shared-mobility.jpg" alt="SHARED MOBILITY PRINCIPLES FOR LIVABLE CITIES">
        </div>
        <div class="col-md-9">
          <p class="lead">Somos parte de la coalición de empresas que promueve los <b>Principios de Movilidad Compartida para Ciudades Vivibles</b></p>
          <a href="https://static1.squarespace.com/static/59c2e59b4c326d11fcf1f516/t/5a677b38c83025d21f6c5bd5/1516731192772/10+Points+WRI+Spanish.pdf" target="_blank">Conoce cuáles son</a>
        </div>
      </div>
    </div>
  </div>
</div>

