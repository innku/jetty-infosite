---
layout: default
title: Jetty | Soluciona tu Transporte Diario en México.
description: Aplicación de Transporte, Disfruta de un Traslado Cómodo, Rápido y Seguro de Manera Diaria a Bordo de  Camionetas Ejecutivas con Conductores Verificados.
image: https://www.jetty.mx/img/Jetty_MX.png
id: Home
priority: 1.0
---

<div class="container-fluid gradient">
  <div class="container">

    <!-- HEADER -->
    <div class="row header-index">
      <div class="col-md-7" >
        <lottie-player
            src="lottie/transporte.json" background="transparent" speed="0.5" loop autoplay class="lottie-trasnporte">
        </lottie-player>
      </div>
      <div class="col-md-5 text-left header-text">
        <h1>Te llevamos cómodo y seguro a tu destino.</h1>
        <br>
        <p class="lead">Contamos con una gran cobertura hacia los principales puntos de la ciudad para llevarte a tu casa u oficina.</p>
        <p class="text-downapp-index">Descarga la app.</p>

        <a href="https://itunes.apple.com/us/app/jetty-soluciona-tu-transporte/id1276413293?l=es&ls=1&mt=8" target="_blank" class="download-app hvr-shadow">
          <img src="img/jetty-iOS.png" alt="Jetty Descargar iOS">
        </a>

        <a href="https://play.google.com/store/apps/details?id=mx.jetty.jetty" target="_blank" class="download-app hvr-shadow">
          <img src="img/jetty-android.png" alt="Jetty Descargar Android">
        </a>
      </div>
    </div>

    <!-- AMENIDADES -->
    <div class="row">
      <div class="col-md-4 amenidad">
        <img src="img/icon-shield-2.svg" alt="Jetty, Seguridad">
        <h3 style="font-weight: 800">Seguridad</h3>
        <p>Todas nuestras unidades cuentan con cámaras de vigilancia y son monitoreadas vía GPS.</p>
      </div>
      <div class="col-md-4 amenidad" >
        <img src="img/icon-support-2.svg" alt="Jetty, Tranquilidads">
        <h3 style="font-weight: 800">Tranquilidad</h3>
        <p>Nuestros conductores y el equipo de soporte, están capacitados para darte el mejor servicio.</p>
      </div>
      <div class="col-md-4 amenidad">
        <img src="img/icon-gps-2.svg" alt="Jetty, Eficiencia">
        <h3 style="font-weight: 800">Eficiencia</h3>
        <p>Nuestro viajes están pensados para que puedas llegar a tu destino de la manera más directa.</p>
      </div>
    </div>

    <!-- COMO FUNCIONA -->
    <div class="row como">
      <div class="col-md-12 text-center">
        <h1>Cómo funciona</h1>
      </div>

      <div class="col-md-12">

        <div class="row">

          <div class="col-md-4" data-aos="fade-up" data-aos-easing="ease-in-sine" data-aos-duration="500">
            <div class="txt-left">
              <img src="img/arrow-left.svg" class="arrow-hidden">
              <p>Dinos de dónde sales y a dónde viajas.</p>
            </div>
            <div class="app-jetty text-left">
              <img src="img/app-jetty-1.png" alt="Jetty, Cómo funciona">
            </div>
          </div>

          <div class="col-md-4 col-md-push-4" data-aos="fade-up" data-aos-easing="ease-in-sine" data-aos-duration="800">
            <div class="txt-right">
              <img src="img/arrow-right.svg" class="arrow-hidden">
              <p>Revisa el día, horario y elige el que más te convenga.</p>
            </div>
            <div class="app-jetty app-right">
              <img src="img/app-jetty-2.png" alt="Jetty, Cómo funciona">
            </div>
          </div>

        </div>

        <div class="row clear app-vistas-dos">

          <div class="col-md-4" data-aos="fade-up" data-aos-easing="ease-in-sine" data-aos-duration="500">
            <div class="txt-left">
              <img src="img/arrow-left.svg" class="arrow-hidden">
              <p>Reserva tu asiento pagando con tarjeta y monitorea tu Jetty.</p>
            </div>
            <div class="app-jetty text-left">
              <img src="img/app-jetty-3.png" alt="Jetty, Cómo funciona">
            </div>
          </div>

          <div class="col-md-4 col-md-push-4" data-aos="fade-up" data-aos-easing="ease-in-sine" data-aos-duration="800">
            <div class="txt-right">
              <img src="img/arrow-right.svg" class="arrow-hidden">
              <p>Camina al punto de abordaje y muéstrale tu pase al conductor.</p>
            </div>
            <div class="app-jetty app-right">
              <img src="img/app-jetty-4.png" alt="Jetty, Cómo funciona">
            </div>
          </div>

        </div>

      </div>

      <div class="col-md-12 text-center descarga">
        <p class="text-downapp-index">Descarga la app.</p>

        <a href="https://itunes.apple.com/us/app/jetty-soluciona-tu-transporte/id1276413293?l=es&ls=1&mt=8" target="_blank" class="download-app hvr-shadow">
          <img src="img/jetty-iOS.png" alt="Jetty Descargar iOS">
        </a>

        <a href="https://play.google.com/store/apps/details?id=mx.jetty.jetty" target="_blank" class="download-app hvr-shadow">
          <img src="img/jetty-android.png" alt="Jetty Descargar Android">
        </a>
      </div>

    </div>

    <!-- TESTIMONIOS -->
    <div class="row testimonios">
      <div class="col-md-12 text-center ">
        <h1>Lo que dicen nuestros usuarios</h1>
      </div>
      <div class="col-md-12">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
          <!-- Indicators -->
          <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
            <li data-target="#carousel-example-generic" data-slide-to="3"></li>
            <li data-target="#carousel-example-generic" data-slide-to="4"></li>
            <li data-target="#carousel-example-generic" data-slide-to="5"></li>
          </ol>

          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">
            <div class="item active">
              <div class="row">
                <div class="col-md-2 text-right">
                  <img src="img/stars.svg">
                </div>
                <div class="col-md-10">
                  <br>
                  <p>Excelente servicio, de hecho extraordinario de verdad, lo recomendaré mucho. <i>Juan Carlos C.</i></p>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="row">
                <div class="col-md-2 text-right">
                  <img src="img/stars.svg">
                </div>
                <div class="col-md-10">
                  <p>Genial servicio! Conductor súper amable. Cada vez van abriendo más servicios y si no te queda alguna puedes preguntar para que te digan por donde pasa y te recojan. Seguro, rápido y cómodo. No hay mejor manera para llegar a Santa Fé. <i>Nancy G.</i></p>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="row">
                <div class="col-md-2 text-right">
                  <img src="img/stars.svg">
                </div>
                <div class="col-md-10">
                  <p>Muy buen servicio, las camionetas son super puntuales, cuentan con cámaras de seguridad y el servicio al cliente siempre está atento a lo que necesites, lo utilizaré casi diario. <i>Rafael C.</i></p>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="row">
                <div class="col-md-2 text-right">
                  <img src="img/clock.svg">
                </div>
                <div class="col-md-10">
                  <p>Excelente, llevo varios meses viajando en Jetty y el servicio es perfecto, Enrique es super amable muy educado y siempre preocupado porque lleguemos a tiempo y con bien a nuestro destino, al servicio y a Enrique les doy 5 estrellas, gracias. <i>Hugo C.</i></p>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="row">
                <div class="col-md-2 text-right">
                  <img src="img/stars.svg">
                </div>
                <div class="col-md-10">
                  <br>
                  <p>Excelente servicio, ya no uso mi carro para ir a la oficina. <i>Luis Antonio F.</i></p>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="row">
                <div class="col-md-2 text-right">
                  <img src="img/corazon.svg">
                </div>
                <div class="col-md-10">
                  <p>Como ya se está volviendo una maravillosa costumbre, JETTY me acompaña casi diario para comenzar bien mi día... ¡Son únicos! ¡Los amo! <i>Klaus</i></p>
                </div>
              </div>
            </div>
          </div>

          <!-- Controls -->
          <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>
    </div>

  </div>
</div>

<!-- <script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script> -->