---
layout: default-cobertura
title: Jetty Cobertura | Explora Nuestras Rutas
description: Consulta nuestra cobertura y nuestros horarios de operaciones en la Zona Metropolitana del Valle de México.
id: cobertura
---

<div class="container cobertura">
  <div class="row">

    <div class="clearfix"></div>

    <div class="col-md-12">
      <div class=" text-center">
        <h2>Nuestra Cobertura</h2>
      </div>

      <div class="col-md-12">

        <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" class="active"><a href="#cdmx" aria-controls="cdmx" role="tab" data-toggle="tab"><b>CDMX</b></a></li>
          <li role="presentation"><a href="#puebla" aria-controls="puebla" role="tab" data-toggle="tab"><b>PUEBLA</b></a></li>
        </ul>

        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="cdmx">
            <div>
              <img src="{{site.baseurl}}/imgs-blog/mapa-rutas-jetty.png" alt="Cobertura Jetty CDMX" style="width: 100%">
            </div>
          </div>
          <div role="tabpanel" class="tab-pane" id="puebla">
            <div>
              <img src="{{site.baseurl}}/imgs-blog/Jetty-Rutas-Puebla.jpg" alt="Cobertura Jetty Puebla" style="width: 100%">
            </div>
          </div>
        </div>

      </div>

    </div>

    <!-- Rutas -->
    <div class="col-md-8 col-md-offset-2 text-center dondevas">
      <h2>Explora nuestros servicios</h2>
    </div>

    <div class="col-md-12">

      <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#rutascdmx" aria-controls="rutascdmx" role="tab" data-toggle="tab"><b>SERVICIOS CDMX</b></a></li>
        <li role="presentation"><a href="#rutaspuebla" aria-controls="rutaspuebla" role="tab" data-toggle="tab"><b>SERVICIOS PUEBLA</b></a></li>
      </ul>

      <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="rutascdmx">
          <div class="rutas">

            <ul class="nuevas-rutas">
              <li>
                <div class="col-md-4">
                  <button type="button" data-toggle="modal" data-target="#ruta6" class="btn-ruta">
                    <h4 class="panel-title">Aragón <img src="img/arrow-cobertura-1.png" class="arrow-cobertura"> Santa Fe</h4>
                  </button>
                </div>
              </li>
              <li>
                <div class="col-md-4">
                  <button type="button" data-toggle="modal" data-target="#rutaSVBus1" class="btn-ruta">
                    <h4 class="panel-title">Acoxpa <img src="img/arrow-cobertura-1.png" class="arrow-cobertura"> Santa Fe</h4>
                  </button>
                </div>
              </li>
              <li>
                <div class="col-md-4">
                  <button type="button" data-toggle="modal" data-target="#rutaSVBus2" class="btn-ruta">
                    <h4 class="panel-title">Acoxpa <img src="img/arrow-cobertura-1.png" class="arrow-cobertura"> Auditorio</h4>
                  </button>
                </div>
              </li>
              <li>
                <div class="col-md-4">
                  <button type="button" data-toggle="modal" data-target="#ruta11" class="btn-ruta">
                    <h4 class="panel-title">Azcapotzalco/Polanco <img src="img/arrow-cobertura-1.png" class="arrow-cobertura"> Santa Fe</h4>
                  </button>
                </div>
              </li>
              <li>
                <div class="col-md-4">
                  <button type="button" data-toggle="modal" data-target="#ruta12" class="btn-ruta">
                    <h4 class="panel-title">Coacalco/Satélite <img src="img/arrow-cobertura-1.png" class="arrow-cobertura"> Polanco/Reforma</h4>
                  </button>
                </div>
              </li>
              <li>
                <div class="col-md-4">
                  <button type="button" data-toggle="modal" data-target="#ruta1" class="btn-ruta">
                    <h4 class="panel-title">Cuautitlán/Perinorte <img src="img/arrow-cobertura-2.png" class="arrow-cobertura"> Polanco/Reforma <img src="img/icon-express.svg" width="60" ></h4>
                  </button>
                </div>
              </li>
              <li>
                <div class="col-md-4">
                  <button type="button" data-toggle="modal" data-target="#ruta2" class="btn-ruta">
                    <h4 class="panel-title">Cuautitlán/Perinorte <img src="img/arrow-cobertura-2.png" class="arrow-cobertura"> Santa Fe <img src="img/icon-express.svg" width="60" ></h4>
                  </button>
                </div>
              </li>
              <li>
                <div class="col-md-4">
                  <button type="button" data-toggle="modal" data-target="#taxis1" class="btn-ruta">
                    <h4 class="panel-title">Cibeles/Sevilla <img src="img/arrow-cobertura-1.png" class="arrow-cobertura"> Santa Fe</h4>
                  </button>
                </div>
              </li>
              <li>
                <div class="col-md-4">
                  <button type="button" data-toggle="modal" data-target="#rutaSVBus4" class="btn-ruta">
                    <h4 class="panel-title">Costco Coapa <img src="img/arrow-cobertura-1.png" class="arrow-cobertura"> Santa Fe</h4>
                  </button>
                </div>
              </li>
              <li>
                <div class="col-md-4">
                  <button type="button" data-toggle="modal" data-target="#rutaSVBus6" class="btn-ruta">
                    <h4 class="panel-title">Costco Coapa <img src="img/arrow-cobertura-1.png" class="arrow-cobertura"> Auditorio</h4>
                  </button>
                </div>
              </li>
              <li>
                <div class="col-md-4">
                  <button type="button" data-toggle="modal" data-target="#rutaSVBus10" class="btn-ruta">
                    <h4 class="panel-title">Huipulco <img src="img/arrow-cobertura-1.png" class="arrow-cobertura"> Santa Fe</h4>
                  </button>
                </div>
              </li>
              <li>
                <div class="col-md-4">
                  <button type="button" data-toggle="modal" data-target="#ruta16" class="btn-ruta">
                    <h4 class="panel-title">Juanacatlán <img src="img/arrow-cobertura-1.png" class="arrow-cobertura"> Santa Fe</h4>
                  </button>
                </div>
              </li>
              <li>
                <div class="col-md-4">
                  <button type="button" data-toggle="modal" data-target="#rutaSVBus8" class="btn-ruta">
                    <h4 class="panel-title">La Joya <img src="img/arrow-cobertura-1.png" class="arrow-cobertura"> Santa Fe <img src="img/icon-express.svg" width="60" ></h4>
                  </button>
                </div>
              </li>
              <li>
                <div class="col-md-4">
                  <button type="button" data-toggle="modal" data-target="#ruta15" class="btn-ruta">
                    <h4 class="panel-title">Mixcoac <img src="img/arrow-cobertura-1.png" class="arrow-cobertura"> Santa Fe</h4>
                  </button>
                </div>
              </li>
              <li>
                <div class="col-md-4">
                  <button type="button" data-toggle="modal" data-target="#ruta9" class="btn-ruta">
                    <h4 class="panel-title">M. Chabacano/Condesa <img src="img/arrow-cobertura-1.png" class="arrow-cobertura"> Santa Fe</h4>
                  </button>
                </div>
              </li>
              <li>
                <div class="col-md-4">
                  <button type="button" data-toggle="modal" data-target="#ruta7" class="btn-ruta">
                    <h4 class="panel-title">Parque de los venados/Del Valle <img src="img/arrow-cobertura-1.png" class="arrow-cobertura"> Santa Fe</h4>
                  </button>
                </div>
              </li>
              <li>
                <div class="col-md-4">
                  <button type="button" data-toggle="modal" data-target="#ruta8" class="btn-ruta">
                    <h4 class="panel-title">Polanco <img src="img/arrow-cobertura-1.png" class="arrow-cobertura"> Santa Fe</h4>
                  </button>
                </div>
              </li>
              <li>
                <div class="col-md-4">
                  <button type="button" data-toggle="modal" data-target="#rutaSVBus9" class="btn-ruta">
                    <h4 class="panel-title">Pedregal <img src="img/arrow-cobertura-1.png" class="arrow-cobertura"> Santa Fe <img src="img/icon-express.svg" width="60" ></h4>
                  </button>
                </div>
              </li>
              <li>
                <div class="col-md-4">
                  <button type="button" data-toggle="modal" data-target="#ruta4" class="btn-ruta">
                    <h4 class="panel-title">Reforma <img src="img/arrow-cobertura-1.png" class="arrow-cobertura"> Santa Fe</h4>
                  </button>
                </div>
              </li>
              <li>
                <div class="col-md-4">
                  <button type="button" data-toggle="modal" data-target="#rutaSVBus3" class="btn-ruta">
                    <h4 class="panel-title">Toreo <img src="img/arrow-cobertura-1.png" class="arrow-cobertura"> Santa Fe</h4>
                  </button>
                </div>
              </li>
              <li>
                <div class="col-md-4">
                  <button type="button" data-toggle="modal" data-target="#rutaUnidas1" class="btn-ruta">
                    <h4 class="panel-title">Viveros/Miguel Ángel de Quevedo/San Jerónimo <img src="img/arrow-cobertura-1.png" class="arrow-cobertura"> Santa Fe</h4>
                  </button>
                </div>
              </li>
              <li>
                <div class="col-md-4">
                  <button type="button" data-toggle="modal" data-target="#ruta3" class="btn-ruta">
                    <h4 class="panel-title">Zona Norte <img src="img/arrow-cobertura-1.png" class="arrow-cobertura"> Polanco</h4>
                  </button>
                </div>
              </li>
              <li>
                <div class="col-md-4">
                  <button type="button" data-toggle="modal" data-target="#ruta5" class="btn-ruta">
                    <h4 class="panel-title">Zona Norte <img src="img/arrow-cobertura-1.png" class="arrow-cobertura"> Santa Fe</h4>
                  </button>
                </div>
              </li>

            </ul>

          </div>
        </div>

        <div role="tabpanel" class="tab-pane" id="rutaspuebla">
          <div class="rutas">

            <ul class="nuevas-rutas">
              <li>
                <div class="col-md-4">
                  <button type="button" data-toggle="modal" data-target="#puebla1" class="btn-ruta">
                    <h4 class="panel-title">Angelópolis <img src="img/arrow-cobertura-1.png" class="arrow-cobertura"> FINSA</h4>
                  </button>
                </div>
              </li>
              <li>
                <div class="col-md-4">
                  <button type="button" data-toggle="modal" data-target="#puebla2" class="btn-ruta">
                    <h4 class="panel-title">Cholula <img src="img/arrow-cobertura-1.png" class="arrow-cobertura"> FINSA</h4>
                  </button>
                </div>
              </li>
              <li>
                <div class="col-md-4">
                  <button type="button" data-toggle="modal" data-target="#puebla3" class="btn-ruta">
                    <h4 class="panel-title">Cuautlancingo <img src="img/arrow-cobertura-1.png" class="arrow-cobertura"> FINSA</h4>
                  </button>
                </div>
              </li>
            </ul>

          </div>
        </div>
      </div>

    </div>


    <div class="clearfix"></div>

    <div class="col-md-12 text-center dondevas">
      <h3>¿Te interesa tener un Jetty cerca de ti?</h3>
      <p>Ingresa aquí tu origen y destino deseado y lo tomaremos en cuenta:</p>
      <a href="solicitud" class="btn btn-default btn-lg btn-green btn-header">¿A dónde te llevamos?</a>
    </div>

    <div class="col-md-8 col-md-offset-2 text-center dondevas">

      <h2>Para reservar tu viaje, descarga la app:</h2>
        <a href="https://itunes.apple.com/us/app/jetty-soluciona-tu-transporte/id1276413293?l=es&ls=1&mt=8" target="_blank" class="download-app hvr-shadow" target="_blank" class="download-app hvr-shadow">
          <img src="img/jetty-iOS.png" alt="Jetty Descargar iOS">
        </a>

        <a href="https://play.google.com/store/apps/details?id=mx.jetty.jetty" target="_blank" class="download-app hvr-shadow">
          <img src="img/jetty-android.png" alt="Jetty Descargar Android">
        </a>
    </div>

  </div>
</div>

<!-- /// MODAL 1 /// -->
<div class="modal fade" id="ruta1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Cuautitlán/Perinorte <img src="img/arrowGreen-cobertura-2.png" class="arrow-cobertura"> Polanco/Reforma <img src="img/icon-expressGreen.svg" width="60"> <!-- <span style="margin-left: 30px;">$79 pesos</span> --></h4>
      </div>
      <div class="modal-body">
        <div id="map_canvas_2" class="map_canvas"></div>

        <ul class="paradas">
          <p><b>Paradas de esta ruta</b></p>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Cuautitlán Express</b> - <i>Av. Jose María Morelos (Palomas, Glorieta, frente al Oxxo)</i>
          </li>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Perinorte Express</b> - <i>Autopista Qro-Mex (frente a Ford)</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Antara - Miyana</b> - <i>Prolongación Moliere, frente a Miyana</i></li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>KFC Ejército Nacional</b> - <i>Ejército Nacional esq Arquímides</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Mariano Escobedo</b> - <i>Spencer frente a Scotiabank</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Hamburgo esq. Toledo</b> - <i>Hamburgo 314</i>
          </li>
        </ul>

      </div>
      <div class="modal-footer">
        <p><b>Ascenso <img src="img/icon-ascenso.png" width="22" style="margin-right: 40px;"> Descenso <img src="img/icon-descenso.png" width="22"></b></p>
      </div>
    </div>
  </div>
</div>

<!-- /// MODAL 2 /// -->
<div class="modal fade" id="ruta2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Cuautitlán/Perinorte <img src="img/arrowGreen-cobertura-2.png" class="arrow-cobertura"> Santa Fe <img src="img/icon-expressGreen.svg" width="60" > <!-- <span style="margin-left: 30px;">$69 pesos</span> --></h4>
      </div>
      <div class="modal-body">
        <div id="map_canvas_7" class="map_canvas"></div>

        <ul class="paradas">
          <p><b>Paradas de esta ruta</b></p>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Cuautitlán Express</b> - <i>Av. Jose María Morelos (Palomas, Glorieta, frente al Oxxo)</i>
          </li>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Perinorte Express</b> - <i>Autopista Qro-Mex (frente a Ford)</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Glorieta Juan Salvador Agraz</b> - <i>Debajo del puente, Prol. Paseo de la Reforma esq. Juan Salvador Agraz</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Samara</b> - <i>Av. Santa Fe esquina Antonio Dovali Jaime</i>
          </li>
        </ul>

      </div>
      <div class="modal-footer">
        <p><b>Ascenso <img src="img/icon-ascenso.png" width="22" style="margin-right: 40px;"> Descenso <img src="img/icon-descenso.png" width="22"></b></p>
      </div>
    </div>
  </div>
</div>

<!-- /// MODAL 3 /// -->
<div class="modal fade" id="ruta3" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Zona Norte <img src="img/arrowGreen-cobertura-1.png" class="arrow-cobertura"> Polanco <!-- <span style="margin-left: 30px;">$69 pesos</span> --></h4>
      </div>
      <div class="modal-body">
        <div id="map_canvas" class="map_canvas"></div>

        <ul class="paradas">
          <p><b>Paradas de esta ruta</b></p>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Mundo E</b> - <i>Gasolinera entre las calles de Caléndulas y Lotos</i>
          </li>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Hospital Gral. Satélite</b> - <i>Cto. del parque, frente al parque</i>
          </li>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Torres de Satélite</b> - <i>Periférico y Circuito Poetas frente al Centro Castellano</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Antara - Miyana</b> - <i>Prolongación Moliere, frente a Miyana</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>KFC Ejército Nacional</b> - <i>Ejército Nacional esq Arquímides</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Mariano Escobedo</b> - <i>Spencer frente a Scotiabank</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Chapultepec</b> - <i>Torre BBVA Bancomer, Paseo de la Reforma, Juárez, Ciudad de México, CDMX, México</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Reforma 222</b> - <i>Paseo de la Reforma 222, Juárez, Ciudad de México, CDMX, México</i>
          </li>
        </ul>

      </div>
      <div class="modal-footer">
        <p><b>Ascenso <img src="img/icon-ascenso.png" width="22" style="margin-right: 40px;"> Descenso <img src="img/icon-descenso.png" width="22"></b></p>
      </div>
    </div>
  </div>
</div>

<!-- /// MODAL 4 /// -->
<div class="modal fade" id="ruta4" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Reforma <img src="img/arrowGreen-cobertura-1.png" class="arrow-cobertura"> Santa Fe <!-- <span style="margin-left: 30px;">$59 pesos</span> --></h4>
      </div>
      <div class="modal-body">
        <div id="map_canvas_3" class="map_canvas"></div>

        <ul class="paradas">
          <p><b>Paradas de esta ruta</b></p>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Corp. Citibanamex</b> - <i>Roberto Medellín esq. Av. Vasco de Quiroga</i>
          </li>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Lieja esq. Hamburgo</b> - <i>Lieja esq. Hamburgo enfrente Restaurante Pialadero</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Arcos Bosques 2</b> - <i>Paseo de Los Tamarindos 150, Bosques de las Lomas, Ciudad de México, CDMX, México</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Ibero</b> - <i>Av. Vasco de Quiroga frente a la Ibero</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Punta Santa Fe</b> - <i>Entrada peatonal a Punta Santa Fe</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Av. Santa Fe 695</b> - <i>Av. Sta Fe 695, Lomas de Santa Fe, Contadero, Ciudad de México, CDMX, México</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Bahia Office Depot</b> - <i>Office Depot, Juan Salvador Agraz, Contadero, Ciudad de México, CDMX, México</i>
          </li>
        </ul>

      </div>
      <div class="modal-footer">
        <p><b>Ascenso <img src="img/icon-ascenso.png" width="22" style="margin-right: 40px;"> Descenso <img src="img/icon-descenso.png" width="22"></b></p>
      </div>
    </div>
  </div>
</div>

<!-- /// MODAL 5 /// -->
<div class="modal fade" id="ruta5" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Zona Norte <img src="img/arrowGreen-cobertura-1.png" class="arrow-cobertura"> Santa Fe <!-- <span style="margin-left: 30px;">$79 pesos</span> --></h4>
      </div>
      <div class="modal-body">
        <div id="map_canvas_4" class="map_canvas"></div>

        <ul class="paradas">
          <p><b>Paradas de esta ruta</b></p>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Cuautitlán</b> - <i>Av. Jose María Morelos (Palomas, Glorieta, frente al Oxxo)</i>
          </li>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Perinorte</b> - <i>Autopista Qro-Mex (frente a Ford)</i>
          </li>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>City Shops Valle Dorado</b> - <i>Periférico Boulevard Manuel Ávila Camacho 3130, Valle Dorado, bahía City Shops</i>
          </li>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Santa Mónica</b> - <i>City Club / Puente peatonal frente a Toks</i>
          </li>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Mundo E</b> - <i>Gasolinera entre las calles de Caléndulas y Lotos</i>
          </li>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Hospital Gral. Satélite</b> - <i>Cto. del parque, frente al parque</i>
          </li>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Torres de Satélite</b> - <i>Periférico y Circuito Poetas frente al Centro Castellano</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Arcos Bosques 2</b> - <i>Paseo de Los Tamarindos 150, Bosques de las Lomas, Ciudad de México, CDMX, México</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Corp. Citibanamex</b> - <i>Roberto Medellín esq. Av. Vasco de Quiroga</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Ibero</b> - <i>Av. Vasco de Quiroga frente a la Ibero</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Av. Santa Fe 695</b> - <i>Av Sta Fe 695, Lomas de Santa Fe, Contadero, Ciudad de México, CDMX, México</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Bahia Office Depot</b> - <i>Office Depot, Juan Salvador Agraz, Contadero, Ciudad de México, CDMX, México</i>
          </li>
        </ul>

      </div>
      <div class="modal-footer">
        <p><b>Ascenso <img src="img/icon-ascenso.png" width="22" style="margin-right: 40px;"> Descenso <img src="img/icon-descenso.png" width="22"></b></p>
      </div>
    </div>
  </div>
</div>

<!-- /// MODAL 6 /// -->
<div class="modal fade" id="ruta6" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Aragón <img src="img/arrowGreen-cobertura-1.png" class="arrow-cobertura"> Santa Fe <!-- <span style="margin-left: 30px;">$79 pesos</span> --></h4>
      </div>
      <div class="modal-body">
        <div id="map_canvas_5" class="map_canvas"></div>

        <ul class="paradas">
          <p><b>Paradas de esta ruta</b></p>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Bosques de Egipto</b> - <i>En el entronque con Bosques de África</i>
          </li>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Bosque de Africa</b> - <i>Blvrd Bosque de Africa esq Bosque de Rhodesia</i>
          </li>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Bosque de Aragón</b> - <i>Puerta 8 del Bosque de Aragón (junto al Metro)</i>
          </li>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Metro Deportivo Oceanía (AM)</b> - <i>Metro Deportivo Oceanía, bajo el Puente de la Estación</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>F. C. de Cuernavaca</b> - <i>Av. F. C. de Cuernavaca 2337, Lomas - Virreyes, Lomas de Chapultepec III Secc, 11000 Ciudad de México, CDMX</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Arcos Bosques 2</b> - <i>Paseo de Los Tamarindos 150, Bosques de las Lomas, Ciudad de México, CDMX, México</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Bahia Office Depot</b> - <i>Office Depot, Juan Salvador Agraz, Contadero, Ciudad de México, CDMX, México</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Corp. Citibanamex</b> - <i>Roberto Medellín esq. Av. Vasco de Quiroga</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Ibero</b> - <i>Av. Vasco de Quiroga frente a la Ibero</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Punta Santa Fe</b> - <i>Entrada peatonal a Punta Santa Fe</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Av. Santa Fe 695</b> - <i>Av Sta Fe 695, Lomas de Santa Fe, Contadero, Ciudad de México, CDMX, México</i>
          </li>
        </ul>

      </div>
      <div class="modal-footer">
        <p><b>Ascenso <img src="img/icon-ascenso.png" width="22" style="margin-right: 40px;"> Descenso <img src="img/icon-descenso.png" width="22"></b></p>
      </div>
    </div>
  </div>
</div>

<!-- /// MODAL 7 /// -->
<div class="modal fade" id="ruta7" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Parque de los venados/Del Valle <img src="img/arrowGreen-cobertura-1.png" class="arrow-cobertura"> Santa Fe <!-- <span style="margin-left: 30px;">$69 pesos</span> --></h4>
      </div>
      <div class="modal-body">
        <div id="map_canvas_6" class="map_canvas"></div>

        <ul class="paradas">
          <p><b>Paradas de esta ruta</b></p>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Parque de los Venados</b> - <i>División del Norte, entre Miguel Laurent y Eje 7 Municipio Libre</i>
          </li>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Nápoles</b> - <i>San Antonio esq. Pensilvania, sobre San Antonio</i>
          </li>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Del Valle-Matías Romero</b> - <i>Matías Romero y Gabriel Mancera</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Westin</b> - <i>Av. Javier Barros Sierra 515</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Pasaje Santa Fe</b> - <i>Ernesto Martínez Domínguez, Frente a Banorte de Pasaje Santa Fe</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Punta Santa Fe</b> - <i>Entrada peatonal a Punta Santa Fe</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Bahia Office Depot</b> - <i>Office Depot, Juan Salvador Agraz, Contadero, Ciudad de México, CDMX, México</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Av Santa Fe 546</b> - <i>Av Santa Fe 546, Lomas de Santa Fe</i>
          </li>
        </ul>

      </div>
      <div class="modal-footer">
        <p><b>Ascenso <img src="img/icon-ascenso.png" width="22" style="margin-right: 40px;"> Descenso <img src="img/icon-descenso.png" width="22"></b></p>
      </div>
    </div>
  </div>
</div>

<!-- /// MODAL 8 /// -->
<div class="modal fade" id="ruta8" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Polanco <img src="img/arrowGreen-cobertura-1.png" class="arrow-cobertura"> Santa Fe <!-- <span style="margin-left: 30px;">$59 pesos</span> --></h4>
      </div>
      <div class="modal-body">
        <div id="map_canvas_8" class="map_canvas"></div>

        <ul class="paradas">
          <p><b>Paradas de esta ruta</b></p>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Lamartine</b> - <i>Lamartine 220, Polanco V Sección, Mexico City, CDMX</i>
          </li>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Parque América</b> - <i>Horacio esq. Anatole France</i>
          </li>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Palmas</b> - <i>Palmas esq. Sierra Mojada</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Bosques de la Reforma 495</b> - <i>Bosques de la Reforma 495, frente a La Iglesia Señor de la Resurrección</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Corp. Citibanamex</b> - <i>Roberto Medellín esq. Av. Vasco de Quiroga</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Ibero</b> - <i>Av. Vasco de Quiroga frente a la Ibero</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Punta Santa Fe</b> - <i>Entrada peatonal a Punta Santa Fe</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Av. Santa Fe 695</b> - <i>Av Sta Fe 695, Lomas de Santa Fe, Contadero, Ciudad de México, CDMX, México</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Bahia Office Depot</b> - <i>Office Depot, Juan Salvador Agraz, Contadero, Ciudad de México, CDMX, México</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Arcos Bosques 2</b> - <i>Paseo de Los Tamarindos 150, Bosques de las Lomas, Ciudad de México, CDMX, México</i>
          </li>
        </ul>

      </div>
      <div class="modal-footer">
        <p><b>Ascenso <img src="img/icon-ascenso.png" width="22" style="margin-right: 40px;"> Descenso <img src="img/icon-descenso.png" width="22"></b></p>
      </div>
    </div>
  </div>
</div>

<!-- /// MODAL 9 /// -->
<div class="modal fade" id="ruta9" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">M. Chabacano/Condesa <img src="img/arrowGreen-cobertura-1.png" class="arrow-cobertura"> Santa Fe <!-- <span style="margin-left: 30px;">$69 pesos</span> --></h4>
      </div>
      <div class="modal-body">
        <div id="map_canvas_9" class="map_canvas"></div>

        <ul class="paradas">
          <p><b>Paradas de esta ruta</b></p>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Metro Chabacano</b> - <i>Metro Chabacano, Esquina Jose A. Torres con Eje 3 Sur, frente Comercial Mexicana</i>
          </li>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Centro Médico</b> - <i>Av. Baja California 1, Roma Sur, Fuera del salón Belmar</i>
          </li>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Metro Chilpancingo</b> - <i>Chilpancingo 70, Fuera de restaurante Jacs</i>
          </li>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Patriotismo</b> - <i>Juchitán 9, Frente al Oxxo</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Bahía Patio Santa Fe (VQ)</b> - <i>Vasco de Quiroga 200-400, Santa Fe, Zedec Sta Fé, 01219 Ciudad de México, CDMX</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Ibero</b> - <i>Av. Vasco de Quiroga frente a la Ibero</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Punta Santa Fe</b> - <i>Entrada peatonal a Punta Santa Fe</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Bahia Office Depot</b> - <i>Office Depot, Juan Salvador Agraz, Contadero, Ciudad de México, CDMX, México</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Av. Santa Fe 695</b> - <i>Av Sta Fe 695, Lomas de Santa Fe, Contadero, Ciudad de México, CDMX, México</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Cetelem</b> - <i>Paseo de la Reforma 2693, Lomas de Bezares, Ciudad de México, CDMX, México</i>
          </li>
        </ul>

      </div>
      <div class="modal-footer">
        <p><b>Ascenso <img src="img/icon-ascenso.png" width="22" style="margin-right: 40px;"> Descenso <img src="img/icon-descenso.png" width="22"></b></p>
      </div>
    </div>
  </div>
</div>

<!-- /// MODAL 10 /// CANCELADA -->
<div class="modal fade" id="ruta10" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Las Alamedas/Atizapán <img src="img/arrowGreen-cobertura-1.png" class="arrow-cobertura"> Santa Fe</h4>
      </div>
      <div class="modal-body">
        <div id="map_canvas_10" class="map_canvas"></div>
      </div>
      <div class="modal-footer">
        <p><b>Ascenso <img src="img/icon-ascenso.png" width="22" style="margin-right: 40px;"> Descenso <img src="img/icon-descenso.png" width="22"></b></p>
      </div>
    </div>
  </div>
</div>

<!-- /// MODAL 11 /// -->
<div class="modal fade" id="ruta11" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Azcapotzalco/Polanco <img src="img/arrowGreen-cobertura-1.png" class="arrow-cobertura"> Santa Fe <!-- <span style="margin-left: 30px;">$69 pesos</span> --></h4>
      </div>
      <div class="modal-body">
        <div id="map_canvas_11" class="map_canvas"></div>

        <ul class="paradas">
          <p><b>Paradas de esta ruta</b></p>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Azcapotzalco</b> - <i>Frente a Angar Azcapotzalco Av. de las Granjas</i>
          </li>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Vips Camarones</b> - <i> Av. Cuitláhuac</i>
          </li>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Metro Cuitláhuac </b> - <i>Mariano Escobedo #3  Frente al Oxxo</i>
          </li>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Laguna de Términos</b> - <i>Esq. Mariano Escobedo (Gasolinera)</i>
          </li>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Parque América (AM)</b> - <i>Horacio esq. Anatole France</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Punta del Parque</b> - <i>Avenida Santa Fe y Av de los Poetas</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Arcos Bosques 2</b> - <i>Paseo de Los Tamarindos 150, Bosques de las Lomas, Ciudad de México, CDMX, México</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Corp. Citibanamex</b> - <i>Roberto Medellín esq. Av. Vasco de Quiroga</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Ibero</b> - <i>Av. Vasco de Quiroga frente a la Ibero</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Punta Santa Fe</b> - <i>Entrada peatonal a Punta Santa Fe</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Av. Santa Fe 695</b> - <i>Av Sta Fe 695, Lomas de Santa Fe, Contadero, Ciudad de México, CDMX, México</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Bahia Office Depot</b> - <i>Office Depot, Juan Salvador Agraz, Contadero, Ciudad de México, CDMX, México</i>
          </li>
        </ul>

      </div>
      <div class="modal-footer">
        <p><b>Ascenso <img src="img/icon-ascenso.png" width="22" style="margin-right: 40px;"> Descenso <img src="img/icon-descenso.png" width="22"></b></p>
      </div>
    </div>
  </div>
</div>

<!-- /// MODAL 12 /// -->
<div class="modal fade" id="ruta12" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Coacalco/Satélite <img src="img/arrowGreen-cobertura-1.png" class="arrow-cobertura"> Polanco/Reforma <!-- <span style="margin-left: 30px;">$79 pesos</span> --></h4>
      </div>
      <div class="modal-body">
        <div id="map_canvas_12" class="map_canvas"></div>

        <ul class="paradas">
          <p><b>Paradas de esta ruta</b></p>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Maxiplaza Tultitlán (Express)</b> - <i>De las Torres, Fuentes del Valle, Méx., México, enfrente del Soriana</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Ejército Nacional (frente Antara)</b> - <i>Antara Fashion Hall, Avenida Ejército Nacional, Granada, Ciudad de México, CDMX, México</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>KFC Ejército Nacional</b> - <i>Ejército Nacional esq Arquímides</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Thiers</b> - <i>Avenida Thiers, Anzures, Ciudad de México, CDMX, México</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Fuente de La Diana Cazadora</b> - <i>Fuente de La Diana Cazadora, Avenida Paseo de la Reforma, Juárez, Ciudad de México, CDMX, México</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Ángel de la Independencia</b> - <i>Paseo de la Reforma 342, Juárez, Ciudad de México, CDMX, México</i>
          </li>
        </ul>

      </div>
      <div class="modal-footer">
        <p><b>Ascenso <img src="img/icon-ascenso.png" width="22" style="margin-right: 40px;"> Descenso <img src="img/icon-descenso.png" width="22"></b></p>
      </div>
    </div>
  </div>
</div>

<!-- /// MODAL 14 /// -->
<div class="modal fade" id="ruta14" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Tacubaya <img src="img/arrowGreen-cobertura-1.png" class="arrow-cobertura"> Santa Fe <!-- <span style="margin-left: 30px;">$30 pesos</span> --></h4>
      </div>
      <div class="modal-body">
        <div id="map_canvas_14" class="map_canvas"></div>

        <ul class="paradas">
          <p><b>Paradas de esta ruta</b></p>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Tacubaya</b> - <i>Avenida Observatorio 97, frente a la fuente pasando el puente peatonal</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Corp. Citibanamex 2</b> - <i>Roberto Medellín esq. Av. Vasco de Quiroga</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Samara</b> - <i>Av. Santa Fe esquina Antonio Dovali Jaime</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Av Santa Fe</b> - <i>Av Sta Fe esq Juan O'Gorman, segunda glorieta de Av Santa Fe</i>
          </li>
        </ul>

      </div>
      <div class="modal-footer">
        <p><b>Ascenso <img src="img/icon-ascenso.png" width="22" style="margin-right: 40px;"> Descenso <img src="img/icon-descenso.png" width="22"></b></p>
      </div>
    </div>
  </div>
</div>

<!-- /// MODAL 15 /// -->
<div class="modal fade" id="ruta15" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Mixcoac <img src="img/arrowGreen-cobertura-1.png" class="arrow-cobertura"> Santa Fe <!-- <span style="margin-left: 30px;">$59 pesos</span> --></h4>
      </div>
      <div class="modal-body">
        <div id="map_canvas_15" class="map_canvas"></div>

        <ul class="paradas">
          <p><b>Paradas de esta ruta</b></p>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Mixcoac</b> - <i>Frente a Office Depot, Avenida Patriotismo, Insurgentes Mixcoac</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Westin</b> - <i>Av. Javier Barros Sierra 515</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Pasaje Santa Fe</b> - <i>Ernesto Martínez Domínguez, Frente a Banorte de Pasaje Santa Fe</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Punta Santa Fe</b> - <i>Entrada peatonal a Punta Santa Fe</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Bahia Office Depot</b> - <i>Office Depot, Juan Salvador Agraz, Contadero, Ciudad de México, CDMX, México</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Av Santa Fe 546</b> - <i>Av Santa Fe 546, Lomas de Santa Fe</i>
          </li>
        </ul>

      </div>
      <div class="modal-footer">
        <p><b>Ascenso <img src="img/icon-ascenso.png" width="22" style="margin-right: 40px;"> Descenso <img src="img/icon-descenso.png" width="22"></b></p>
      </div>
    </div>
  </div>
</div>

<!-- /// MODAL 16 /// -->
<div class="modal fade" id="ruta16" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Juanacatlán <img src="img/arrowGreen-cobertura-1.png" class="arrow-cobertura"> Santa Fe <!-- <span style="margin-left: 30px;">$30 pesos</span> --></h4>
      </div>
      <div class="modal-body">
        <div id="map_canvas_16" class="map_canvas"></div>

        <ul class="paradas">
          <p><b>Paradas de esta ruta</b></p>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Juanacatlán</b> - <i>Gral. Pedro Antonio de Los Santos 70, San Miguel Chapultepec II Secc</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Corp. Citibanamex 2</b> - <i>Roberto Medellín esq. Av. Vasco de Quiroga</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Samara</b> - <i>Av. Santa Fe esquina Antonio Dovali Jaime</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Av Santa Fe</b> - <i>Av Sta Fe esq Juan O'Gorman, segunda glorieta de Av Santa Fe</i>
          </li>
        </ul>

      </div>
      <div class="modal-footer">
        <p><b>Ascenso <img src="img/icon-ascenso.png" width="22" style="margin-right: 40px;"> Descenso <img src="img/icon-descenso.png" width="22"></b></p>
      </div>
    </div>
  </div>
</div>

<!-- /// MODAL 1 SVBus /// -->
<div class="modal fade" id="rutaSVBus1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Acoxpa <img src="img/arrowGreen-cobertura-1.png" class="arrow-cobertura"> Santa Fe <!-- <span style="margin-left: 30px;">$79 pesos</span> --></h4>
      </div>
      <div class="modal-body">
        <div id="map_canvas_SVBus1" class="map_canvas"></div>

        <ul class="paradas">
          <p><b>Paradas de esta ruta</b></p>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Acoxpa</b> - <i>Av. Acoxpa altura de Boutique Palacio</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Glorieta Juan Salvador Agraz</b> - <i>Debajo del puente, Prol. Paseo de la Reforma esq. Juan Salvador Agraz</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Samara</b> - <i>Av. Santa Fe esquina Antonio Dovali Jaime</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Tec de Monterrey Santa Fe</b> - <i>Av. de los Poetas, pasando el Tecnológico de Monterrey Campus Santa Fe</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Av Santa Fe</b> - <i>Av Sta Fe esq Juan O'Gorman, segunda glorieta de Av Santa Fe</i>
          </li>
        </ul>

      </div>
      <div class="modal-footer">
        <p><b>Ascenso <img src="img/icon-ascenso.png" width="22" style="margin-right: 40px;"> Descenso <img src="img/icon-descenso.png" width="22"></b></p>
      </div>
    </div>
  </div>
</div>

<!-- /// MODAL 2 SVBus /// -->
<div class="modal fade" id="rutaSVBus2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Acoxpa <img src="img/arrowGreen-cobertura-1.png" class="arrow-cobertura"> Auditorio <!-- <span style="margin-left: 30px;">$30 pesos</span> --></h4>
      </div>
      <div class="modal-body">
        <div id="map_canvas_SVBus2" class="map_canvas"></div>

        <ul class="paradas">
          <p><b>Paradas de esta ruta</b></p>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Acoxpa</b> - <i>Av. Acoxpa altura de Boutique Palacio</i>
          </li>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Auditorio Nacional</b> - <i>Estacionamiento Ecológico del Bosque de Chapultepec</i>
          </li>
        </ul>

      </div>
      <div class="modal-footer">
        <p><b>Ascenso <img src="img/icon-ascenso.png" width="22" style="margin-right: 40px;"> Descenso <img src="img/icon-descenso.png" width="22"></b></p>
      </div>
    </div>
  </div>
</div>

<!-- /// MODAL 3 SVBus /// -->
<div class="modal fade" id="rutaSVBus3" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Toreo <img src="img/arrowGreen-cobertura-1.png" class="arrow-cobertura"> Santa Fe <!-- <span style="margin-left: 30px;">$25 pesos</span> --></h4>
      </div>
      <div class="modal-body">
        <div id="map_canvas_SVBus3" class="map_canvas"></div>

        <ul class="paradas">
          <p><b>Paradas de esta ruta</b></p>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Panteón Sanctorum (Toreo)</b> - <i>Panteón Sanctorum, Mexico City, CDMX, Mexico</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Glorieta Vasco de Quiroga</b> - <i>Avenida Vasco de Quiroga, Margarita Maza de Juárez, Mexico City, CDMX, Mexico</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Samara</b> - <i>Av. Santa Fe esquina Antonio Dovali Jaime</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Glorieta Juan Salvador Agraz</b> - <i>Debajo del puente, Prol. Paseo de la Reforma esq. Juan Salvador Agraz</i>
          </li>
        </ul>

      </div>
      <div class="modal-footer">
        <p><b>Ascenso <img src="img/icon-ascenso.png" width="22" style="margin-right: 40px;"> Descenso <img src="img/icon-descenso.png" width="22"></b></p>
      </div>
    </div>
  </div>
</div>

<!-- /// MODAL 4 SVBus /// -->
<div class="modal fade" id="rutaSVBus4" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Costco Coapa <img src="img/arrowGreen-cobertura-1.png" class="arrow-cobertura"> Santa Fe <!-- <span style="margin-left: 30px;">$40 pesos</span> --></h4>
      </div>
      <div class="modal-body">
        <div id="map_canvas_SVBus4" class="map_canvas"></div>

        <!-- <ul class="paradas">
          <p><b>Paradas de esta ruta</b></p>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b></b> - <i></i>
          </li>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b></b> - <i></i>
          </li>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b></b> - <i></i>
          </li>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b></b> - <i></i>
          </li>
        </ul> -->

      </div>
      <div class="modal-footer">
        <p><b>Ascenso <img src="img/icon-ascenso.png" width="22" style="margin-right: 40px;"> Descenso <img src="img/icon-descenso.png" width="22"></b></p>
      </div>
    </div>
  </div>
</div>

<!-- /// MODAL 5 SVBus /// -->
<div class="modal fade" id="rutaSVBus5" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Costco Coapa <img src="img/arrowGreen-cobertura-1.png" class="arrow-cobertura"> Centro Comercial Santa Fe <!-- <span style="margin-left: 30px;">$40 pesos</span> --></h4>
      </div>
      <div class="modal-body">
        <div id="map_canvas_SVBus5" class="map_canvas"></div>

        <!-- <ul class="paradas">
          <p><b>Paradas de esta ruta</b></p>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b></b> - <i></i>
          </li>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b></b> - <i></i>
          </li>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b></b> - <i></i>
          </li>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b></b> - <i></i>
          </li>
        </ul> -->

      </div>
      <div class="modal-footer">
        <p><b>Ascenso <img src="img/icon-ascenso.png" width="22" style="margin-right: 40px;"> Descenso <img src="img/icon-descenso.png" width="22"></b></p>
      </div>
    </div>
  </div>
</div>

<!-- /// MODAL 6 SVBus /// -->
<div class="modal fade" id="rutaSVBus6" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Costco Coapa <img src="img/arrowGreen-cobertura-1.png" class="arrow-cobertura"> Auditorio <!-- <span style="margin-left: 30px;">$30 pesos</span> --></h4>
      </div>
      <div class="modal-body">
        <div id="map_canvas_SVBus6" class="map_canvas"></div>

        <ul class="paradas">
          <p><b>Paradas de esta ruta</b></p>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Costco Coapa</b> - <i>Calle Puente 186, frente al Tec de Monterrey</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Auditorio Nacional</b> - <i>Estacionamiento Ecológico del Bosque de Chapultepec</i>
          </li>
        </ul>

      </div>
      <div class="modal-footer">
        <p><b>Ascenso <img src="img/icon-ascenso.png" width="22" style="margin-right: 40px;"> Descenso <img src="img/icon-descenso.png" width="22"></b></p>
      </div>
    </div>
  </div>
</div>

<!-- /// MODAL 7 SVBus /// CANCELADA -->
<div class="modal fade" id="rutaSVBus7" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Auditorio <img src="img/arrowPurple-cobertura-1.png" class="arrow-cobertura"> Santa Fe</h4>
      </div>
      <div class="modal-body">
        <div id="map_canvas_SVBus7" class="map_canvas"></div>
      </div>
      <div class="modal-footer">
        <p><b>Ascenso <img src="img/icon-ascenso.png" width="22" style="margin-right: 40px;"> Descenso <img src="img/icon-descenso.png" width="22"></b></p>
      </div>
    </div>
  </div>
</div>

<!-- /// MODAL 8 SVBus /// -->
<div class="modal fade" id="rutaSVBus8" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">La Joya <img src="img/arrowGreen-cobertura-1.png" class="arrow-cobertura"> Santa Fe <img src="img/icon-expressGreen.svg" width="60" > <!-- <span style="margin-left: 30px;">$55 pesos</span> --></h4>
      </div>
      <div class="modal-body">
        <div id="map_canvas_SVBus8" class="map_canvas"></div>

        <ul class="paradas">
          <p><b>Paradas de esta ruta</b></p>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>La Joya</b> - <i>Av de los Insurgentes Sur, La Joya, frente a Farmacias del Ahorro</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Samara</b> - <i>Av. Santa Fe esquina Antonio Dovali Jaime</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Av Santa Fe</b> - <i>Av Sta Fe esq Juan O'Gorman, segunda glorieta de Av Santa Fe</i>
          </li>
        </ul>

      </div>
      <div class="modal-footer">
        <p><b>Ascenso <img src="img/icon-ascenso.png" width="22" style="margin-right: 40px;"> Descenso <img src="img/icon-descenso.png" width="22"></b></p>
      </div>
    </div>
  </div>
</div>

<!-- /// MODAL 9 SVBus /// -->
<div class="modal fade" id="rutaSVBus9" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Pedregal <img src="img/arrowGreen-cobertura-1.png" class="arrow-cobertura"> Santa Fe <img src="img/icon-expressGreen.svg" width="60" ></h4>
      </div>
      <div class="modal-body">
        <div id="map_canvas_SVBus9" class="map_canvas"></div>

        <ul class="paradas">
          <p><b>Paradas de esta ruta</b></p>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Picacho</b> - <i>Boulevard Cataratas esq Periferico Sur (junto a salida Office Max)</i>
          </li>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Perisur</b> - <i>Avenida Zacatepetl, Insurgentes Cuicuilco frente a la entrada de Perisur</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Samara</b> - <i>Av. Santa Fe esquina Antonio Dovali Jaime</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Av Santa Fe</b> - <i>Av Sta Fe esq Juan O'Gorman, segunda glorieta de Av Santa Fe</i>
          </li>
        </ul>

      </div>
      <div class="modal-footer">
        <p><b>Ascenso <img src="img/icon-ascenso.png" width="22" style="margin-right: 40px;"> Descenso <img src="img/icon-descenso.png" width="22"></b></p>
      </div>
    </div>
  </div>
</div>

<!-- /// MODAL 10 SVBus /// -->
<div class="modal fade" id="rutaSVBus10" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Huipulco <img src="img/arrowGreen-cobertura-1.png" class="arrow-cobertura"> Santa Fe <!-- <span style="margin-left: 30px;">$40 pesos</span> --></h4>
      </div>
      <div class="modal-body">
        <div id="map_canvas_SVBus10" class="map_canvas"></div>

        <ul class="paradas">
          <p><b>Paradas de esta ruta</b></p>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Huipulco</b> - <i>Huipulco, San Lorenzo Huipulco, Huipulco, Tlalpan, CDMX, México</i>
          </li>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Costco Coapa</b> - <i>Calle Puente 186, frente al Tec de Monterrey</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Glorieta Juan Salvador Agraz</b> - <i>Debajo del puente, Prol. Paseo de la Reforma esq. Juan Salvador Agraz</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Tec de Monterrey Santa Fe</b> - <i>Av. de los Poetas, pasando el Tecnológico de Monterrey Campus Santa Fe</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Samara</b> - <i>Av. Santa Fe esquina Antonio Dovali Jaime</i>
          </li>
        </ul>

      </div>
      <div class="modal-footer">
        <p><b>Ascenso <img src="img/icon-ascenso.png" width="22" style="margin-right: 40px;"> Descenso <img src="img/icon-descenso.png" width="22"></b></p>
      </div>
    </div>
  </div>
</div>

<!-- /// MODAL 1 TaxisElectricos /// -->
<div class="modal fade" id="taxis1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Cibeles/Sevilla <img src="img/arrowGreen-cobertura-1.png" class="arrow-cobertura"> Santa Fe <!-- <span style="margin-left: 30px;">$55 pesos</span> --></h4>
      </div>
      <div class="modal-body">
        <div id="map_canvas_TaxiElectrico1" class="map_canvas"></div>

        <ul class="paradas">
          <p><b>Paradas de esta ruta</b></p>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Cibeles</b> - <i>Fuente de la Cibeles, Plaza Villa de Madrid, Roma Norte, Ciudad de México, CDMX, México</i>
          </li>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Sevilla</b> - <i>Toledo, Juárez, Ciudad de México, CDMX, México</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Centro Comercial Santa Fe</b> - <i>Sobre Vasco de Quiroga</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Samara</b> - <i>Av. Santa Fe esquina Antonio Dovali Jaime</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Bahía Patio Santa Fe (VQ)</b> - <i>Vasco de Quiroga 200-400, Santa Fe, Zedec Sta Fé, 01219 Ciudad de México, CDMX</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Corp. Citibanamex 2</b> - <i>Roberto Medellín esq. Av. Vasco de Quiroga</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Pasaje Santa Fe</b> - <i>Ernesto Martínez Domínguez, Frente a Banorte de Pasaje Santa Fe</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Bahia Office Depot</b> - <i>Office Depot, Juan Salvador Agraz, Contadero, Ciudad de México, CDMX, México</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Av Santa Fe 546</b> - <i>Av Santa Fe 546, Lomas de Santa Fe</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Westin</b> - <i>Av. Javier Barros Sierra 515</i>
          </li>
        </ul>

      </div>
      <div class="modal-footer">
        <p><b>Ascenso <img src="img/icon-ascenso.png" width="22" style="margin-right: 40px;"> Descenso <img src="img/icon-descenso.png" width="22"></b></p>
      </div>
    </div>
  </div>
</div>

<!-- /// MODAL 1 RutasUnidas /// -->
<div class="modal fade" id="rutaUnidas1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Viveros/Miguel Ángel de Quevedo/San Jerónimo <img src="img/arrowGreen-cobertura-1.png" class="arrow-cobertura"> Santa Fe <!-- <span style="margin-left: 30px;">$54 pesos</span> --></h4>
      </div>
      <div class="modal-body">
        <div id="map_canvas_Rutas1" class="map_canvas"></div>

        <ul class="paradas">
          <p><b>Paradas de esta ruta</b></p>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Viveros (OD)</b> - <i>Frente a Office Depot de Av. Universidad</i>
          </li>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Miguel Ángel de Quevedo (PM)</b> - <i>Scotiabank Universidad, 1821, Av. Universidad, Oxtopulco, CDMX, México</i>
          </li>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>San Jerónimo (AM)</b> - <i>Av. Periférico Sur esq. Tarasquillo</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Toyota Santa Fe</b> - <i>Juan Salvador Agraz 20, Lomas de Santa Fe, Contadero, Ciudad de México, CDMX, México</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Av. Santa Fe 443</b> - <i>Avenida Santa Fe 443, Lomas de Santa Fe, Contadero, Ciudad de México, CDMX, México</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>GEPP</b> - <i>Avenida Santa Fe 485, Lomas de Santa Fe, Contadero, Ciudad de México, CDMX, México</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Samara</b> - <i>Av. Santa Fe esquina Antonio Dovali Jaime</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>Glorieta Juan Salvador Agraz</b> - <i>Debajo del puente, Prol. Paseo de la Reforma esq. Juan Salvador Agraz</i>
          </li>
        </ul>

      </div>
      <div class="modal-footer">
        <p><b>Ascenso <img src="img/icon-ascenso.png" width="22" style="margin-right: 40px;"> Descenso <img src="img/icon-descenso.png" width="22"></b></p>
      </div>
    </div>
  </div>
</div>


<!-- /// MODAL 1 Puebla /// -->
<div class="modal fade" id="puebla1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Angelópolis <img src="img/arrowGreen-cobertura-1.png" class="arrow-cobertura"> FINSA</h4>
      </div>
      <div class="modal-body">
        <div id="map_canvas_Puebla1" class="map_canvas"></div>

        <ul class="paradas">
          <p><b>Paradas de esta ruta</b></p>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Parque Mariposas Zona Azul</b> - <i>Lomas de Angelópolis, Tlaxcalancingo.d</i>
          </li>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Glorieta Dominó</b> - <i>Gran Boulevard, Lomas de Angelópolis, Tlaxcalancingo.</i>
          </li>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Oxxo Angelopolis</b> - <i>Boulevard de los Reyes, Av del Castillo 1, Lomas de Angelópolis</i>
          </li>

          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>FINSA: Tule esquina con San Lorenzo</b> - <i>Av. San Lorenzo Almecatla, Sanctorum, CP 72730.</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>FINSA: Faurecia 2</b> - <i>Tule, Zona VW, Sanctorum.</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>FINSA: CEDIS Oxxo</b> - <i>Ebano 11, Zona VW, Sanctorum.</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>FINSA: Bodega Coppel</b> - <i>Tule, Zona VW, 72730 Sanctorum</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>FINSA: SA Automotive</b> - <i>Ebano, Zona VW, Sanctorum.</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>FINSA: INTEVA</b> - <i>Acaicas, Zona VW, Sanctorum.</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>FINSA: Faurecia 1</b> - <i>Acacias, Zona VW, Sanctorum.</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>FINSA: Puerta 7 de VW</b> - <i>Av. San Lorenzo Almecatla, Sanctorum, 72730 Sanctorum.</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>FINSA: Puerta 3 de VW</b> - <i>Av 28 de noviembre, Sanctorum, 72730.</i>
          </li>
        </ul>

      </div>
      <div class="modal-footer">
        <p><b>Ascenso <img src="img/icon-ascenso.png" width="22" style="margin-right: 40px;"> Descenso <img src="img/icon-descenso.png" width="22"></b></p>
      </div>
    </div>
  </div>
</div>

<!-- /// MODAL 2 Puebla /// -->
<div class="modal fade" id="puebla2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Cholula <img src="img/arrowGreen-cobertura-1.png" class="arrow-cobertura"> FINSA</h4>
      </div>
      <div class="modal-body">
        <div id="map_canvas_Puebla2" class="map_canvas"></div>

        <ul class="paradas">
          <p><b>Paradas de esta ruta</b></p>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Plaza Sur</b> - <i>Carr Izucar de Matamoros 5003, Jardines de San Carlos, Emiliano Zapata.</i>
          </li>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Puente UDLAP</b> - <i>Esq. Periferico con Calz. del Ciprés s/n, Residencial Real de Cholula. </i>
          </li>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Puente Carcaña, Oxxo Periferico Oriente</b> - <i>Anillo Perif. Ecológico 3220, Santiago Momoxpan, 72775 San Andrés Cholula.</i>
          </li>

          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>FINSA: Tule esquina con San Lorenzo</b> - <i>Av. San Lorenzo Almecatla, Sanctorum, CP 72730.</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>FINSA: Faurecia 2</b> - <i>Tule, Zona VW, Sanctorum.</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>FINSA: CEDIS Oxxo</b> - <i>Ebano 11, Zona VW, Sanctorum.</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>FINSA: Bodega Coppel</b> - <i>Tule, Zona VW, 72730 Sanctorum</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>FINSA: SA Automotive</b> - <i>Ebano, Zona VW, Sanctorum.</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>FINSA: INTEVA</b> - <i>Acaicas, Zona VW, Sanctorum.</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>FINSA: Faurecia 1</b> - <i>Acacias, Zona VW, Sanctorum.</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>FINSA: Puerta 7 de VW</b> - <i>Av. San Lorenzo Almecatla, Sanctorum, 72730 Sanctorum.</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>FINSA: Puerta 3 de VW</b> - <i>Av 28 de noviembre, Sanctorum, 72730.</i>
          </li>
        </ul>

      </div>
      <div class="modal-footer">
        <p><b>Ascenso <img src="img/icon-ascenso.png" width="22" style="margin-right: 40px;"> Descenso <img src="img/icon-descenso.png" width="22"></b></p>
      </div>
    </div>
  </div>
</div>

<!-- /// MODAL 3 Puebla /// -->
<div class="modal fade" id="puebla3" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Cuautlancingo <img src="img/arrowGreen-cobertura-1.png" class="arrow-cobertura"> FINSA</h4>
      </div>
      <div class="modal-body">
        <div id="map_canvas_Puebla3" class="map_canvas"></div>

        <ul class="paradas">
          <p><b>Paradas de esta ruta</b></p>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Deportivo San Jose, Cuautlancingo</b> - <i>Barrio del Perdón, San Juan Cuautlancingo.</i>
          </li>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Mercado Municipal, Cuautlancingo</b> - <i>Av México - Puebla, Barrio del Calvario, San Juan Cuautlancingo.</i>
          </li>
          <li>
            <img src="img/icon-ascenso.png" width="10">
            <b>Bodega Aurrera, Cuautlancingo</b> - <i>Av México - Puebla, Barrio del Calvario,San Juan Cuautlancingo.</i>
          </li>

          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>FINSA: Tule esquina con San Lorenzo</b> - <i>Av. San Lorenzo Almecatla, Sanctorum, CP 72730.</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>FINSA: Faurecia 2</b> - <i>Tule, Zona VW, Sanctorum.</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>FINSA: CEDIS Oxxo</b> - <i>Ebano 11, Zona VW, Sanctorum.</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>FINSA: Bodega Coppel</b> - <i>Tule, Zona VW, 72730 Sanctorum</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>FINSA: SA Automotive</b> - <i>Ebano, Zona VW, Sanctorum.</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>FINSA: INTEVA</b> - <i>Acaicas, Zona VW, Sanctorum.</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>FINSA: Faurecia 1</b> - <i>Acacias, Zona VW, Sanctorum.</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>FINSA: Puerta 7 de VW</b> - <i>Av. San Lorenzo Almecatla, Sanctorum, 72730 Sanctorum.</i>
          </li>
          <li>
            <img src="img/icon-descenso.png" width="10">
            <b>FINSA: Puerta 3 de VW</b> - <i>Av 28 de noviembre, Sanctorum, 72730.</i>
          </li>
        </ul>

      </div>
      <div class="modal-footer">
        <p><b>Ascenso <img src="img/icon-ascenso.png" width="22" style="margin-right: 40px;"> Descenso <img src="img/icon-descenso.png" width="22"></b></p>
      </div>
    </div>
  </div>
</div>