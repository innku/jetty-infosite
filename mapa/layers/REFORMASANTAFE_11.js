var json_REFORMASANTAFE_11 = {
"type": "FeatureCollection",
"name": "REFORMASANTAFE_11",
"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
"features": [
{ "type": "Feature", "properties": { "Name": "Reforma -> Santa Fe", "NOMBRE": "LIEJA ESQ HAMBURGO", "HORARIO": "8:10, 9:00", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.1745988, 19.421918 ] } },
{ "type": "Feature", "properties": { "Name": "Reforma -> Santa Fe", "NOMBRE": "ARCOS BOSQUES", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2530707, 19.387168 ] } },
{ "type": "Feature", "properties": { "Name": "Reforma -> Santa Fe", "NOMBRE": "CITIBANAMEX", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2596056, 19.3732942 ] } },
{ "type": "Feature", "properties": { "Name": "Reforma -> Santa Fe", "NOMBRE": "IBERO", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.263766, 19.3678039 ] } },
{ "type": "Feature", "properties": { "Name": "Reforma -> Santa Fe", "NOMBRE": "PUNTA SANTA FE", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2673519, 19.3653487 ] } },
{ "type": "Feature", "properties": { "Name": "Reforma -> Santa Fe", "NOMBRE": "AV SANTA FE 695", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2747847, 19.3572083 ] } },
{ "type": "Feature", "properties": { "Name": "Reforma -> Santa Fe", "NOMBRE": "BAHIA OFFICE DEPOT", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.277617, 19.3575518 ] } }
]
}
