var json_VALLEDORADOPOLANCOREFORMA_18 = {
"type": "FeatureCollection",
"name": "VALLEDORADOPOLANCOREFORMA_18",
"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
"features": [
{ "type": "Feature", "properties": { "Name": "Valle Dorado -> Polanco -> Reforma", "NOMBRE": "GLORIETA PALMA", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.1645779, 19.4288821 ] } },
{ "type": "Feature", "properties": { "Name": "Valle Dorado -> Polanco -> Reforma", "NOMBRE": "HAMBURGO ESQ TOLEDO", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.1721041, 19.4227462 ] } },
{ "type": "Feature", "properties": { "Name": "Valle Dorado -> Polanco -> Reforma", "NOMBRE": "CITY SHOPS", "HORARIO": "7:05", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2078161, 19.5512954 ] } },
{ "type": "Feature", "properties": { "Name": "Valle Dorado -> Polanco -> Reforma", "NOMBRE": "CITY CLUB", "HORARIO": "7:15", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2168737, 19.537394 ] } },
{ "type": "Feature", "properties": { "Name": "Valle Dorado -> Polanco -> Reforma", "NOMBRE": "MUNDO E", "HORARIO": "7:18", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2252722, 19.5262251 ] } },
{ "type": "Feature", "properties": { "Name": "Valle Dorado -> Polanco -> Reforma", "NOMBRE": "HOSPITAL GRAL SATELITE", "HORARIO": "7:25", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2368224, 19.5135761 ] } },
{ "type": "Feature", "properties": { "Name": "Valle Dorado -> Polanco -> Reforma", "NOMBRE": "TORRES DE SATELITE", "HORARIO": "7:35", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2370452, 19.5026111 ] } },
{ "type": "Feature", "properties": { "Name": "Valle Dorado -> Polanco -> Reforma", "NOMBRE": "ANTARA-MIYANA", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.200893, 19.4398313 ] } },
{ "type": "Feature", "properties": { "Name": "Valle Dorado -> Polanco -> Reforma", "NOMBRE": "KFC EJERCITO NACIONAL", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.1903505, 19.4377786 ] } },
{ "type": "Feature", "properties": { "Name": "Valle Dorado -> Polanco -> Reforma", "NOMBRE": "MARIANO ESCOBEDO", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.1820863, 19.4323609 ] } }
]
}
