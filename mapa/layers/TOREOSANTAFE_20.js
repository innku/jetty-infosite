var json_TOREOSANTAFE_20 = {
"type": "FeatureCollection",
"name": "TOREOSANTAFE_20",
"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
"features": [
{ "type": "Feature", "properties": { "Name": "Toreo -> Santa Fe Norte", "NOMBRE": "GLORIETA JUAN SALVADOR AGRAZ", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2763279, 19.3578556 ] } },
{ "type": "Feature", "properties": { "Name": "Toreo -> Santa Fe Norte", "NOMBRE": "PANTEON SANCTORUM (TOREO)", "HORARIO": "6:30, 7:00, 7:30, 8:25, 9:00", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2145297, 19.4564184 ] } },
{ "type": "Feature", "properties": { "Name": "Toreo -> Santa Fe Norte", "NOMBRE": "GLORIETA VASCO DE QUIROGA", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2488952, 19.3797706 ] } }
]
}
