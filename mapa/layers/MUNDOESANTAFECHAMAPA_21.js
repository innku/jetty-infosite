var json_MUNDOESANTAFECHAMAPA_21 = {
"type": "FeatureCollection",
"name": "MUNDOESANTAFECHAMAPA_21",
"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
"features": [
{ "type": "Feature", "properties": { "Name": "Mundo E -> Santa Fe (AM) Chamapa", "NOMBRE": "PASAJE SANTA FE", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2637094, 19.3665001 ] } },
{ "type": "Feature", "properties": { "Name": "Mundo E -> Santa Fe (AM) Chamapa", "NOMBRE": "IBERO", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2594319, 19.3733682 ] } },
{ "type": "Feature", "properties": { "Name": "Mundo E -> Santa Fe (AM) Chamapa", "NOMBRE": "ARCOS BOSQUES", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2530707, 19.387168 ] } },
{ "type": "Feature", "properties": { "Name": "Mundo E -> Santa Fe (AM) Chamapa", "NOMBRE": "MUNDO E", "HORARIO": "8:35", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2252722, 19.5262251 ] } },
{ "type": "Feature", "properties": { "Name": "Mundo E -> Santa Fe (AM) Chamapa", "NOMBRE": "CHILIS SATELITE", "HORARIO": "8:40", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2332533, 19.5156892 ] } },
{ "type": "Feature", "properties": { "Name": "Mundo E -> Santa Fe (AM) Chamapa", "NOMBRE": "FUENTES DE SATELITE", "HORARIO": "8:42", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2468793, 19.5188522 ] } },
{ "type": "Feature", "properties": { "Name": "Mundo E -> Santa Fe (AM) Chamapa", "NOMBRE": "APENINOS", "HORARIO": "8:45", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.259107, 19.5175934 ] } },
{ "type": "Feature", "properties": { "Name": "Mundo E -> Santa Fe (AM) Chamapa", "NOMBRE": "BAHIA OFFICE DEPOT", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2776355, 19.357463 ] } },
{ "type": "Feature", "properties": { "Name": "Mundo E -> Santa Fe (AM) Chamapa", "NOMBRE": "AV SANTA FE 546", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2738962, 19.3574617 ] } }
]
}
