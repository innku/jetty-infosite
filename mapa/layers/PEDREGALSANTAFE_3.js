var json_PEDREGALSANTAFE_3 = {
"type": "FeatureCollection",
"name": "PEDREGALSANTAFE_3",
"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
"features": [
{ "type": "Feature", "properties": { "NOMBRE": "PICACHO", "RUTA": "PEDREGAL -> SANTA FE", "HORARIO": "6:10, 6:40, 7:55, 8:25", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.204991977641527, 19.306295876871459 ] } },
{ "type": "Feature", "properties": { "NOMBRE": "ZACATEPETL", "RUTA": "PEDREGAL -> SANTA FE", "HORARIO": "6:15, 6:45, 8:00, 8:30", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.193313844003754, 19.303199686422833 ] } },
{ "type": "Feature", "properties": { "NOMBRE": "SAMARA", "RUTA": "PEDREGAL -> SANTA FE", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.260175443510633, 19.368091882113724 ] } },
{ "type": "Feature", "properties": { "NOMBRE": "AV SANTA FE", "RUTA": "PEDREGAL -> SANTA FE", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.274815954863627, 19.357212861970659 ] } }
]
}
