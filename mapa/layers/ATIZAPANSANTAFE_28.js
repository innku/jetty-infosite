var json_ATIZAPANSANTAFE_28 = {
"type": "FeatureCollection",
"name": "ATIZAPANSANTAFE_28",
"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
"features": [
{ "type": "Feature", "properties": { "Name": "Atizapan -> Santa Fe", "TIPO": "ASCENSO", "NOMBRE": "WALMART ALAMEDAS", "HORARIO": "6:40" }, "geometry": { "type": "Point", "coordinates": [ -99.2440164, 19.5552048 ] } },
{ "type": "Feature", "properties": { "Name": "Atizapan -> Santa Fe", "TIPO": "ASCENSO", "NOMBRE": "GALERIAS ATIZAPAN", "HORARIO": "6:52" }, "geometry": { "type": "Point", "coordinates": [ -99.2710824, 19.5520578 ] } },
{ "type": "Feature", "properties": { "Name": "Atizapan -> Santa Fe", "TIPO": "DESCENSO", "NOMBRE": "ARCOS BOSQUES", "HORARIO": null }, "geometry": { "type": "Point", "coordinates": [ -99.2530707, 19.387168 ] } },
{ "type": "Feature", "properties": { "Name": "Atizapan -> Santa Fe", "TIPO": "DESCENSO", "NOMBRE": "IBERO", "HORARIO": null }, "geometry": { "type": "Point", "coordinates": [ -99.263766, 19.3678039 ] } },
{ "type": "Feature", "properties": { "Name": "Atizapan -> Santa Fe", "TIPO": "DESCENSO", "NOMBRE": "PUNTA SANTA FE", "HORARIO": null }, "geometry": { "type": "Point", "coordinates": [ -99.2673519, 19.3653487 ] } },
{ "type": "Feature", "properties": { "Name": "Atizapan -> Santa Fe", "TIPO": "DESCENSO", "NOMBRE": "BAHIA OFFICE DEPOT", "HORARIO": null }, "geometry": { "type": "Point", "coordinates": [ -99.2776355, 19.357463 ] } },
{ "type": "Feature", "properties": { "Name": "Atizapan -> Santa Fe", "TIPO": "DESCENSO", "NOMBRE": "AV STA FE 546", "HORARIO": null }, "geometry": { "type": "Point", "coordinates": [ -99.2738928, 19.3574637 ] } }
]
}
