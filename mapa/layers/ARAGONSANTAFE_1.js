var json_ARAGONSANTAFE_1 = {
"type": "FeatureCollection",
"name": "ARAGONSANTAFE_1",
"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
"features": [
{ "type": "Feature", "properties": { "Name": "Aragon -> Santa Fe", "TIPO": "ASCENSO", "NOMBRE": "BOSQUES DE EGIPTO", "HORARIO": "5:30" }, "geometry": { "type": "Point", "coordinates": [ -99.0496936, 19.4716031 ] } },
{ "type": "Feature", "properties": { "Name": "Aragon -> Santa Fe", "TIPO": "ASCENSO", "NOMBRE": "BOSQUE DE AFRICA", "HORARIO": "5:35" }, "geometry": { "type": "Point", "coordinates": [ -99.0495135, 19.4685886 ] } },
{ "type": "Feature", "properties": { "Name": "Aragon -> Santa Fe", "TIPO": "ASCENSO", "NOMBRE": "BOSQUE DE ARAGON", "HORARIO": "5:45" }, "geometry": { "type": "Point", "coordinates": [ -99.0693485, 19.4582055 ] } },
{ "type": "Feature", "properties": { "Name": "Aragon -> Santa Fe", "TIPO": "ASCENSO", "NOMBRE": "METRO DEPORTIVO OCEANIA", "HORARIO": "5:50" }, "geometry": { "type": "Point", "coordinates": [ -99.0789307, 19.4514097 ] } },
{ "type": "Feature", "properties": { "Name": "Aragon -> Santa Fe", "TIPO": "DESCENSO", "NOMBRE": "ARCOS BOSQUES", "HORARIO": null }, "geometry": { "type": "Point", "coordinates": [ -99.2530707, 19.387168 ] } },
{ "type": "Feature", "properties": { "Name": "Aragon -> Santa Fe", "TIPO": "DESCENSO", "NOMBRE": "CITY BANAMEX", "HORARIO": null }, "geometry": { "type": "Point", "coordinates": [ -99.2596056, 19.3732942 ] } },
{ "type": "Feature", "properties": { "Name": "Aragon -> Santa Fe", "TIPO": "DESCENSO", "NOMBRE": "IBERO", "HORARIO": null }, "geometry": { "type": "Point", "coordinates": [ -99.263766, 19.3678039 ] } },
{ "type": "Feature", "properties": { "Name": "Aragon -> Santa Fe", "TIPO": "DESCENSO", "NOMBRE": "PUNTA SANTA FE", "HORARIO": null }, "geometry": { "type": "Point", "coordinates": [ -99.2673519, 19.3653487 ] } },
{ "type": "Feature", "properties": { "Name": "Aragon -> Santa Fe", "TIPO": "DESCENSO", "NOMBRE": "AV SANTA FE 695", "HORARIO": null }, "geometry": { "type": "Point", "coordinates": [ -99.2747847, 19.3572083 ] } },
{ "type": "Feature", "properties": { "Name": "Aragon -> Santa Fe", "TIPO": "DESCENSO", "NOMBRE": "JUAN SALVADOR AGRAZ", "HORARIO": null }, "geometry": { "type": "Point", "coordinates": [ -99.277617, 19.3575518 ] } }
]
}
