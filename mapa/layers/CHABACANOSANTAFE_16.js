var json_CHABACANOSANTAFE_16 = {
"type": "FeatureCollection",
"name": "CHABACANOSANTAFE_16",
"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
"features": [
{ "type": "Feature", "properties": { "Name": "Chabacano -> Santa Fe", "NOMBRE": "METRO CHILPANCINGO", "HORARIO": "6:57, 8:22", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.1694017, 19.4058837 ] } },
{ "type": "Feature", "properties": { "Name": "Chabacano -> Santa Fe", "NOMBRE": "CENTRO MEDICO", "HORARIO": "7:02, 8:27", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.177942, 19.4059793 ] } },
{ "type": "Feature", "properties": { "Name": "Chabacano -> Santa Fe", "NOMBRE": "PATIO SANTA FE", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2541491, 19.3765308 ] } },
{ "type": "Feature", "properties": { "Name": "Chabacano -> Santa Fe", "NOMBRE": "IBERO", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.263766, 19.3678039 ] } },
{ "type": "Feature", "properties": { "Name": "Chabacano -> Santa Fe", "NOMBRE": "PUNTA SANTA FE", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2673519, 19.3653487 ] } },
{ "type": "Feature", "properties": { "Name": "Chabacano -> Santa Fe", "NOMBRE": "BAHIA OFFICE DEPOT", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2776355, 19.357463 ] } },
{ "type": "Feature", "properties": { "Name": "Chabacano -> Santa Fe", "NOMBRE": "AV SANTA FE 695", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2747847, 19.3572083 ] } },
{ "type": "Feature", "properties": { "Name": "Chabacano -> Santa Fe", "NOMBRE": "METRO CHABACANO", "HORARIO": "6:40, 8:00", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.1335148, 19.4086726 ] } },
{ "type": "Feature", "properties": { "Name": "Chabacano -> Santa Fe", "NOMBRE": "CENTRO MEDICO", "HORARIO": "6:50, 8:19", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.1555758, 19.4065761 ] } }
]
}
