var json_DELVALLESANTAFE_14 = {
"type": "FeatureCollection",
"name": "DELVALLESANTAFE_14",
"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
"features": [
{ "type": "Feature", "properties": { "RUTA": "Del Valle  -> Santa Fe", "NOMBRE": "PARQUE DE LOS VENADOS", "HORARIOS": "6:00, 6:20, 6:30, 7:00, 7:20, 8:10, 9:20", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.1571827, 19.3722651 ] } },
{ "type": "Feature", "properties": { "RUTA": "Del Valle  -> Santa Fe", "NOMBRE": "MATIAS ROMERO", "HORARIOS": "6:10, 6:40, 7:10, 7:30, 8:10, 8:22, 8:40, 9:30", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.1659591, 19.3803199 ] } },
{ "type": "Feature", "properties": { "RUTA": "Del Valle  -> Santa Fe", "NOMBRE": "NAPOLES", "HORARIOS": "6:20, 6:50, 7:22, 7:42, 8:20, 8:35, 8:50, 9:45", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.1768583, 19.3858006 ] } },
{ "type": "Feature", "properties": { "RUTA": "Del Valle  -> Santa Fe", "NOMBRE": "WESTIN", "HORARIOS": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2592569, 19.3656751 ] } },
{ "type": "Feature", "properties": { "RUTA": "Del Valle  -> Santa Fe", "NOMBRE": "PASAJE SANTA FE", "HORARIOS": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2635798, 19.3663845 ] } },
{ "type": "Feature", "properties": { "RUTA": "Del Valle  -> Santa Fe", "NOMBRE": "AV SANTA FE 546", "HORARIOS": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2738479, 19.3575062 ] } }
]
}
