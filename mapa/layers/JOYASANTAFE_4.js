var json_JOYASANTAFE_4 = {
"type": "FeatureCollection",
"name": "JOYASANTAFE_4",
"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
"features": [
{ "type": "Feature", "properties": { "NOMBRE": "LA JOYA", "RUTA": "JOYA -> SANTA FE", "HORARIO": "6:10, 6:40, 8:10, 8:40", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.169645425357359, 19.28000527734007 ] } },
{ "type": "Feature", "properties": { "NOMBRE": "SAMARA", "RUTA": "JOYA -> SANTA FE", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.260175443510576, 19.368045688233913 ] } },
{ "type": "Feature", "properties": { "NOMBRE": "AV SANTA FE", "RUTA": "JOYA -> SANTA FE", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.274815954863598, 19.357235960446893 ] } }
]
}
