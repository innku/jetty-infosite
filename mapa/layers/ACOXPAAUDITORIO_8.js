var json_ACOXPAAUDITORIO_8 = {
"type": "FeatureCollection",
"name": "ACOXPAAUDITORIO_8",
"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
"features": [
{ "type": "Feature", "properties": { "NOMBRE": "ACOXPA", "RUTA": "ACOXPA -> AUDITORIO", "HORARIO": "6:10, 6:25, 6:50, 7:20, 7:50, 8:05, 9:20, 9:55", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.137925337527875, 19.300574232747991 ] } },
{ "type": "Feature", "properties": { "NOMBRE": "AUDITORIO", "RUTA": "ACOXPA -> AUDITORIO", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.194332926419776, 19.426225153393759 ] } }
]
}
