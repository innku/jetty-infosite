var json_VALLEDORADOLOMASVERDESSANTAFE_19 = {
"type": "FeatureCollection",
"name": "VALLEDORADOLOMASVERDESSANTAFE_19",
"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
"features": [
{ "type": "Feature", "properties": { "Name": "Valle Dorado -> Lomas Verdes - Santa Fe", "NOMBRE": "CITY SHOPS", "HORARIO": "7:00", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.207818, 19.5513056 ] } },
{ "type": "Feature", "properties": { "Name": "Valle Dorado -> Lomas Verdes - Santa Fe", "NOMBRE": "SANTA MONICA", "HORARIO": "7:08", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2168737, 19.537394 ] } },
{ "type": "Feature", "properties": { "Name": "Valle Dorado -> Lomas Verdes - Santa Fe", "NOMBRE": "MUNDO E", "HORARIO": "7:13", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2252722, 19.5262251 ] } },
{ "type": "Feature", "properties": { "Name": "Valle Dorado -> Lomas Verdes - Santa Fe", "NOMBRE": "HOSPITAL SAN JOSE", "HORARIO": "7:23", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2424412, 19.5182422 ] } },
{ "type": "Feature", "properties": { "Name": "Valle Dorado -> Lomas Verdes - Santa Fe", "NOMBRE": "FUENTES DE SATELITE", "HORARIO": "7:26", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2468793, 19.5188522 ] } },
{ "type": "Feature", "properties": { "Name": "Valle Dorado -> Lomas Verdes - Santa Fe", "NOMBRE": "APENINOS", "HORARIO": "7:31", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.259107, 19.5175934 ] } },
{ "type": "Feature", "properties": { "Name": "Valle Dorado -> Lomas Verdes - Santa Fe", "NOMBRE": "PABELLON BOSQUES", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2674166, 19.3828205 ] } },
{ "type": "Feature", "properties": { "Name": "Valle Dorado -> Lomas Verdes - Santa Fe", "NOMBRE": "ARCOS BOSQUES", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2530707, 19.387168 ] } },
{ "type": "Feature", "properties": { "Name": "Valle Dorado -> Lomas Verdes - Santa Fe", "NOMBRE": "CITIBANAMEX", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2596056, 19.3732942 ] } },
{ "type": "Feature", "properties": { "Name": "Valle Dorado -> Lomas Verdes - Santa Fe", "NOMBRE": "IBERO", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.263766, 19.3678039 ] } }
]
}
