var json_CUAUTITLANSANTAFE_22 = {
"type": "FeatureCollection",
"name": "CUAUTITLANSANTAFE_22",
"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
"features": [
{ "type": "Feature", "properties": { "Name": "Cuautitlan -> Zona Santa Fe (AM)", "NOMBRE": "CUAUTITLAN", "HORARIO": "5:40", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.1997233, 19.644181 ] } },
{ "type": "Feature", "properties": { "Name": "Cuautitlan -> Zona Santa Fe (AM)", "NOMBRE": "PERINORTE", "HORARIO": "5:50", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.1895852, 19.6058081 ] } },
{ "type": "Feature", "properties": { "Name": "Cuautitlan -> Zona Santa Fe (AM)", "NOMBRE": "CITY SHOPS VALLE DORADO", "HORARIO": "6:00", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2078161, 19.5512954 ] } },
{ "type": "Feature", "properties": { "Name": "Cuautitlan -> Zona Santa Fe (AM)", "NOMBRE": "SANTA MONICA", "HORARIO": "6:05", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2168737, 19.537394 ] } },
{ "type": "Feature", "properties": { "Name": "Cuautitlan -> Zona Santa Fe (AM)", "NOMBRE": "MUNDO E", "HORARIO": "6:10", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2252722, 19.5262251 ] } },
{ "type": "Feature", "properties": { "Name": "Cuautitlan -> Zona Santa Fe (AM)", "NOMBRE": "HOSPITAL GRAL. SATELITE", "HORARIO": "6:14", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2368224, 19.5135761 ] } },
{ "type": "Feature", "properties": { "Name": "Cuautitlan -> Zona Santa Fe (AM)", "NOMBRE": "TORRES DE SATELITE", "HORARIO": "6:20", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2370452, 19.5026111 ] } },
{ "type": "Feature", "properties": { "Name": "Cuautitlan -> Zona Santa Fe (AM)", "NOMBRE": "ARCOS BOSQUES", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2530707, 19.387168 ] } },
{ "type": "Feature", "properties": { "Name": "Cuautitlan -> Zona Santa Fe (AM)", "NOMBRE": "CITIBANAMEX", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2596056, 19.3732942 ] } },
{ "type": "Feature", "properties": { "Name": "Cuautitlan -> Zona Santa Fe (AM)", "NOMBRE": "IBERO", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2637671, 19.3674491 ] } },
{ "type": "Feature", "properties": { "Name": "Cuautitlan -> Zona Santa Fe (AM)", "NOMBRE": "AV SANTA FE 695", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.274855738861632, 19.357337016241882 ] } },
{ "type": "Feature", "properties": { "Name": "Cuautitlan -> Zona Santa Fe (AM)", "NOMBRE": "BAHIA OFFICE DEPOT", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.277793634116421, 19.357614197529667 ] } }
]
}
