var json_POLANCOSANTAFE_12 = {
"type": "FeatureCollection",
"name": "POLANCOSANTAFE_12",
"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
"features": [
{ "type": "Feature", "properties": { "Name": "Polanco -> Santa Fe", "NOMBRE": "LAMARTINE", "HORARIO": "9:00", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.1896736, 19.4349037 ] } },
{ "type": "Feature", "properties": { "Name": "Polanco -> Santa Fe", "NOMBRE": "PARQUE AMERICA", "HORARIO": "9:05", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.1974457, 19.4342855 ] } },
{ "type": "Feature", "properties": { "Name": "Polanco -> Santa Fe", "NOMBRE": "PALMAS", "HORARIO": "9:15", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2113475, 19.4299299 ] } },
{ "type": "Feature", "properties": { "Name": "Polanco -> Santa Fe", "NOMBRE": "BOSQUES DE REFORMA 495", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2407711, 19.4075072 ] } },
{ "type": "Feature", "properties": { "Name": "Polanco -> Santa Fe", "NOMBRE": "ARCOS BOSQUES", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2530707, 19.387168 ] } },
{ "type": "Feature", "properties": { "Name": "Polanco -> Santa Fe", "NOMBRE": "CITIBANAMEX", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2596056, 19.3732942 ] } },
{ "type": "Feature", "properties": { "Name": "Polanco -> Santa Fe", "NOMBRE": "IBERO", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.263766, 19.3678039 ] } },
{ "type": "Feature", "properties": { "Name": "Polanco -> Santa Fe", "NOMBRE": "PUNTA SANTA FE", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2673519, 19.3653487 ] } },
{ "type": "Feature", "properties": { "Name": "Polanco -> Santa Fe", "NOMBRE": "AV SANTA FE 695", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2747847, 19.3572083 ] } },
{ "type": "Feature", "properties": { "Name": "Polanco -> Santa Fe", "NOMBRE": "BAHIA OFFICE DEPOT", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.277617, 19.3575518 ] } }
]
}
