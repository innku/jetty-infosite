var json_CIBELESSEVILLASANTAFE_17 = {
"type": "FeatureCollection",
"name": "CIBELESSEVILLASANTAFE_17",
"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
"features": [
{ "type": "Feature", "properties": { "Name": "Cibeles -> Sevilla -> Santa Fe", "NOMBRE": "CIBELES", "HORARIO": "6:10, 6:20, 6:40, 7:00, 7:20, 8:10, 8:20, 8:40, 9:00, 9:20, 10:10, 10:20, 10:40, 11:00", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.1662049, 19.4195954 ] } },
{ "type": "Feature", "properties": { "Name": "Cibeles -> Sevilla -> Santa Fe", "NOMBRE": "SEVILLA", "HORARIO": "6:20, 6:30, 6:50, 7:10, 7:30, 8:20, 8:30, 8:50, 9:10, 9:30, 10:20, 10:30, 10:50, 11:10", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.1717935, 19.4222573 ] } },
{ "type": "Feature", "properties": { "Name": "Cibeles -> Sevilla -> Santa Fe", "NOMBRE": "PATIO SANTA FE", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2541491, 19.3765308 ] } },
{ "type": "Feature", "properties": { "Name": "Cibeles -> Sevilla -> Santa Fe", "NOMBRE": "CITIBANAMEX", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2596056, 19.3732942 ] } },
{ "type": "Feature", "properties": { "Name": "Cibeles -> Sevilla -> Santa Fe", "NOMBRE": "PASAJE SANTA FE", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2635798, 19.3663845 ] } },
{ "type": "Feature", "properties": { "Name": "Cibeles -> Sevilla -> Santa Fe", "NOMBRE": "CENTRO COMERCIAL SANTA FE", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2708211, 19.3642219 ] } },
{ "type": "Feature", "properties": { "Name": "Cibeles -> Sevilla -> Santa Fe", "NOMBRE": "BAHIA OFFICE DEPOT", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2776355, 19.357463 ] } },
{ "type": "Feature", "properties": { "Name": "Cibeles -> Sevilla -> Santa Fe", "NOMBRE": "AV SANTA FE 546", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2738635, 19.3574854 ] } },
{ "type": "Feature", "properties": { "Name": "Cibeles -> Sevilla -> Santa Fe", "NOMBRE": "WESTIN", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2592569, 19.3656751 ] } }
]
}
