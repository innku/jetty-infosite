var json_AZCAPOTZALCOPOLANCOSANTAFE_27 = {
"type": "FeatureCollection",
"name": "AZCAPOTZALCOPOLANCOSANTAFE_27",
"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
"features": [
{ "type": "Feature", "properties": { "Name": "Azcapotzalco -> Polanco -> Santa Fe", "NOMBRE": "PARQUE AMERICA", "HORARIO": "7:22, 7:44", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.1974457, 19.4342855 ] } },
{ "type": "Feature", "properties": { "Name": "Azcapotzalco -> Polanco -> Santa Fe", "NOMBRE": null, "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2530707, 19.387168 ] } },
{ "type": "Feature", "properties": { "Name": "Azcapotzalco -> Polanco -> Santa Fe", "NOMBRE": null, "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2596056, 19.3732942 ] } },
{ "type": "Feature", "properties": { "Name": "Azcapotzalco -> Polanco -> Santa Fe", "NOMBRE": null, "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.263766, 19.3678039 ] } },
{ "type": "Feature", "properties": { "Name": "Azcapotzalco -> Polanco -> Santa Fe", "NOMBRE": null, "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2673519, 19.3653487 ] } },
{ "type": "Feature", "properties": { "Name": "Azcapotzalco -> Polanco -> Santa Fe", "NOMBRE": null, "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.265635, 19.3628727 ] } },
{ "type": "Feature", "properties": { "Name": "Azcapotzalco -> Polanco -> Santa Fe", "NOMBRE": "AZCAPOTZALCO", "HORARIO": "6:40, 7:00", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.1744423, 19.4825771 ] } },
{ "type": "Feature", "properties": { "Name": "Azcapotzalco -> Polanco -> Santa Fe", "NOMBRE": "VIPS CAMARONES", "HORARIO": "6:52, 7:10", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.1765237, 19.4680457 ] } },
{ "type": "Feature", "properties": { "Name": "Azcapotzalco -> Polanco -> Santa Fe", "NOMBRE": "METRO CUITLAHUAC", "HORARIO": "7:05, 7:25", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.1806918, 19.4562621 ] } },
{ "type": "Feature", "properties": { "Name": "Azcapotzalco -> Polanco -> Santa Fe", "NOMBRE": "LAGUNA DE TERMINOS", "HORARIO": "7:12, 7:32", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.1831161, 19.4435248 ] } }
]
}
