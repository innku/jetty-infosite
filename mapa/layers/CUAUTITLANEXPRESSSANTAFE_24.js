var json_CUAUTITLANEXPRESSSANTAFE_24 = {
"type": "FeatureCollection",
"name": "CUAUTITLANEXPRESSSANTAFE_24",
"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
"features": [
{ "type": "Feature", "properties": { "Name": "Cuautitlan (Express) -> Santa Fe", "NOMBRE": "CUAUTITLAN", "HORARIO": "6:30, 6:40", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.1997233, 19.644181 ] } },
{ "type": "Feature", "properties": { "Name": "Cuautitlan (Express) -> Santa Fe", "NOMBRE": "PERINORTE", "HORARIO": "7:00, 7:10", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.1895852, 19.6058081 ] } },
{ "type": "Feature", "properties": { "Name": "Cuautitlan (Express) -> Santa Fe", "NOMBRE": "GLORIETA JUAN SALVADRO AGRAZ", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2776355, 19.357463 ] } },
{ "type": "Feature", "properties": { "Name": "Cuautitlan (Express) -> Santa Fe", "NOMBRE": "SAMARA", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2595927, 19.3672734 ] } }
]
}
