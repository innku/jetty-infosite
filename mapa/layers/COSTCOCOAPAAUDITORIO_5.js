var json_COSTCOCOAPAAUDITORIO_5 = {
"type": "FeatureCollection",
"name": "COSTCOCOAPAAUDITORIO_5",
"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
"features": [
{ "type": "Feature", "properties": { "Name": "Costco Coapa -> Auditorio", "NOMBRE": "COSTCO", "HORARIOS": "6:20, 6:30, 6:40, 8:00, 8:15, 8:35", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.1375439, 19.2844196 ] } },
{ "type": "Feature", "properties": { "Name": "Costco Coapa -> Auditorio", "NOMBRE": "AUDITORIO", "HORARIOS": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.1937986, 19.4253389 ] } }
]
}
