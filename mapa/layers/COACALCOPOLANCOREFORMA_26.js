var json_COACALCOPOLANCOREFORMA_26 = {
"type": "FeatureCollection",
"name": "COACALCOPOLANCOREFORMA_26",
"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
"features": [
{ "type": "Feature", "properties": { "Name": "Coacalco -> Polanco -> Reforma", "NOMBRE": "MAXIPLAZA TULTITLAN", "HORARIO": "6:00", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.13608485026343, 19.640752421794733 ] } },
{ "type": "Feature", "properties": { "Name": "Coacalco -> Polanco -> Reforma", "NOMBRE": "EJERCITO NACIONAL (FRENTE ANTARA)", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.201235754446401, 19.438181048021285 ] } },
{ "type": "Feature", "properties": { "Name": "Coacalco -> Polanco -> Reforma", "NOMBRE": "KFC EJERCITO NACIONAL", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.1903215, 19.4377772 ] } },
{ "type": "Feature", "properties": { "Name": "Coacalco -> Polanco -> Reforma", "NOMBRE": "THIERS", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.178622031845379, 19.433422936040966 ] } },
{ "type": "Feature", "properties": { "Name": "Coacalco -> Polanco -> Reforma", "NOMBRE": "FUENTE DE LA DIANA CAZADORA", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.166805803901568, 19.426804139906871 ] } },
{ "type": "Feature", "properties": { "Name": "Coacalco -> Polanco -> Reforma", "NOMBRE": "ANGEL DE LA INDEPENDENCIA", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.171149284042173, 19.424870390989437 ] } }
]
}
