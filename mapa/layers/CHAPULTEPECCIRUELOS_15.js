var json_CHAPULTEPECCIRUELOS_15 = {
"type": "FeatureCollection",
"name": "CHAPULTEPECCIRUELOS_15",
"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
"features": [
{ "type": "Feature", "properties": { "Name": "Chapultepec -> Ciruelos", "NOMBRE": "TORRE BANCOMER", "HORARIO": "8:00", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.1745988, 19.421918 ] } },
{ "type": "Feature", "properties": { "Name": "Chapultepec -> Ciruelos", "NOMBRE": "CIRUELOS", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2397328, 19.404596 ] } }
]
}
