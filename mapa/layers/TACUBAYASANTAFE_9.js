var json_TACUBAYASANTAFE_9 = {
"type": "FeatureCollection",
"name": "TACUBAYASANTAFE_9",
"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
"features": [
{ "type": "Feature", "properties": { "NOMBRE": "TACUBAYA", "RUTA": "TACUBAYA -> SANTA FE", "HORARIO": "6:10, 6:30, 6:50, 7:35, 8:00, 8:25", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.189534364170726, 19.404145096881951 ] } },
{ "type": "Feature", "properties": { "NOMBRE": "SAMARA", "RUTA": "TACUBAYA -> SANTA FE", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.260239709969227, 19.368048575351764 ] } },
{ "type": "Feature", "properties": { "NOMBRE": "CITIBANAMEX", "RUTA": "TACUBAYA -> SANTA FE", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.259603165997348, 19.373314593225473 ] } },
{ "type": "Feature", "properties": { "NOMBRE": "AV SANTA FE", "RUTA": "TACUBAYA -> SANTA FE", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.274782291480406, 19.357238847756175 ] } }
]
}
