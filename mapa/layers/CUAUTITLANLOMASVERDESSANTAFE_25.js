var json_CUAUTITLANLOMASVERDESSANTAFE_25 = {
"type": "FeatureCollection",
"name": "CUAUTITLANLOMASVERDESSANTAFE_25",
"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
"features": [
{ "type": "Feature", "properties": { "Name": "Cuautitlan -> Lomas Verdes -> Santa Fe", "NOMBRE": "CUAUTITLAN", "HORARIO": "6:00, 6:20, 6:50", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.1997233, 19.644181 ] } },
{ "type": "Feature", "properties": { "Name": "Cuautitlan -> Lomas Verdes -> Santa Fe", "NOMBRE": "PERINORTE", "HORARIO": "6:10, 6:33, 7:07", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.1895852, 19.6058081 ] } },
{ "type": "Feature", "properties": { "Name": "Cuautitlan -> Lomas Verdes -> Santa Fe", "NOMBRE": "CITY SHOPS VALLE DORADO", "HORARIO": "6:25, 6:48, 7:17", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.207818, 19.5513056 ] } },
{ "type": "Feature", "properties": { "Name": "Cuautitlan -> Lomas Verdes -> Santa Fe", "NOMBRE": "SANTA MONICA", "HORARIO": "6:35, 6:55, 7:25", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2168737, 19.537394 ] } },
{ "type": "Feature", "properties": { "Name": "Cuautitlan -> Lomas Verdes -> Santa Fe", "NOMBRE": "MUNDO E", "HORARIO": "6:40, 6:58, 7:30", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2252722, 19.5262251 ] } },
{ "type": "Feature", "properties": { "Name": "Cuautitlan -> Lomas Verdes -> Santa Fe", "NOMBRE": "HOSPITAL SAN JOSE", "HORARIO": "6:45, 7:04, 7:40", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2424412, 19.5182422 ] } },
{ "type": "Feature", "properties": { "Name": "Cuautitlan -> Lomas Verdes -> Santa Fe", "NOMBRE": "FUENTES DE SATELITE", "HORARIO": "6:46, 7:05, 7:43", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2468793, 19.5188522 ] } },
{ "type": "Feature", "properties": { "Name": "Cuautitlan -> Lomas Verdes -> Santa Fe", "NOMBRE": "APENINOS", "HORARIO": "6:48, 7:08, 7:48", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.259107, 19.5175934 ] } },
{ "type": "Feature", "properties": { "Name": "Cuautitlan -> Lomas Verdes -> Santa Fe", "NOMBRE": "HOSPITAL LOS ANGELES INTERLOMAS", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2815561, 19.3956256 ] } },
{ "type": "Feature", "properties": { "Name": "Cuautitlan -> Lomas Verdes -> Santa Fe", "NOMBRE": "PABELLON BOSQUES", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2674431, 19.3828359 ] } },
{ "type": "Feature", "properties": { "Name": "Cuautitlan -> Lomas Verdes -> Santa Fe", "NOMBRE": "ARCOS BOSQUES", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.253066349055374, 19.387177495252779 ] } },
{ "type": "Feature", "properties": { "Name": "Cuautitlan -> Lomas Verdes -> Santa Fe", "NOMBRE": "NULLTELEVISA SANTA FE", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.252087050637101, 19.377200481937187 ] } },
{ "type": "Feature", "properties": { "Name": "Cuautitlan -> Lomas Verdes -> Santa Fe", "NOMBRE": "CITIBANAMEX", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.259627648457737, 19.373320367274463 ] } },
{ "type": "Feature", "properties": { "Name": "Cuautitlan -> Lomas Verdes -> Santa Fe", "NOMBRE": "IBERO", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.263838631656256, 19.367869573946958 ] } },
{ "type": "Feature", "properties": { "Name": "Cuautitlan -> Lomas Verdes -> Santa Fe", "NOMBRE": "PUNTA SANTA FE", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.267364105961988, 19.365375082300591 ] } },
{ "type": "Feature", "properties": { "Name": "Cuautitlan -> Lomas Verdes -> Santa Fe", "NOMBRE": "BAHIA OFFICE DEPOT", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.277646739353756, 19.35752180381942 ] } },
{ "type": "Feature", "properties": { "Name": "Cuautitlan -> Lomas Verdes -> Santa Fe", "NOMBRE": "CITY MARKET", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.271966808527836, 19.358445738566413 ] } }
]
}
