var json_VIVEROS_MAQ_SNJERONIMOSANTAFE_2 = {
"type": "FeatureCollection",
"name": "VIVEROS_MAQ_SNJERONIMOSANTAFE_2",
"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
"features": [
{ "type": "Feature", "properties": { "Name": "San Jeronimo -> SF", "NOMBRE": "SAN JERONIMO", "HORARIO": "6:25, 8:15", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2118852, 19.3288117 ] } },
{ "type": "Feature", "properties": { "Name": "San Jeronimo -> SF", "NOMBRE": "SAMARA", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2602145, 19.3680641 ] } },
{ "type": "Feature", "properties": { "Name": "San Jeronimo -> SF", "NOMBRE": "AV SANTA FE 443", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2678749, 19.3613397 ] } },
{ "type": "Feature", "properties": { "Name": "San Jeronimo -> SF", "NOMBRE": "GEPP", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2721126, 19.3586615 ] } },
{ "type": "Feature", "properties": { "Name": "San Jeronimo -> SF", "NOMBRE": "GLORIETA OFFICE DEPOT", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2763225, 19.3578606 ] } },
{ "type": "Feature", "properties": { "Name": "San Jeronimo -> SF", "NOMBRE": "TOYOTA SANTA FE", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2792882, 19.3600008 ] } },
{ "type": "Feature", "properties": { "Name": "San Jeronimo -> SF", "NOMBRE": "VIVEROS", "HORARIO": "6:10, 7:45", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.176509695207429, 19.353248537529488 ] } },
{ "type": "Feature", "properties": { "Name": "San Jeronimo -> SF", "NOMBRE": "MIGUEL A DE QUEVEDO", "HORARIO": "6:14, 7:55", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.18140618729872, 19.344193462855088 ] } }
]
}
