var json_ARAGONPOLANCO_0 = {
"type": "FeatureCollection",
"name": "ARAGONPOLANCO_0",
"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
"features": [
{ "type": "Feature", "properties": { "Name": "Aragon -> Polanco", "NOMBRE": "BOSQUES DE EGIPTO", "HORARIO": "6:15", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.0496936, 19.4716031 ] } },
{ "type": "Feature", "properties": { "Name": "Aragon -> Polanco", "NOMBRE": "BOSQUE DE AFRICA", "HORARIO": "6:20", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.0495135, 19.4685886 ] } },
{ "type": "Feature", "properties": { "Name": "Aragon -> Polanco", "NOMBRE": "BOSQUES DE ARAGON", "HORARIO": "6:25", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.0693485, 19.4582055 ] } },
{ "type": "Feature", "properties": { "Name": "Aragon -> Polanco", "NOMBRE": "BBVA ARQUIMIDES", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.1902699, 19.4380028 ] } },
{ "type": "Feature", "properties": { "Name": "Aragon -> Polanco", "NOMBRE": "ANTARA", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2016873, 19.43855 ] } }
]
}
