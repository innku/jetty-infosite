var json_JUANACATLANSANTAFE_13 = {
"type": "FeatureCollection",
"name": "JUANACATLANSANTAFE_13",
"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
"features": [
{ "type": "Feature", "properties": { "NOMBRE": "JUANACATLAN", "RUTA": "JUANACATLAN > SANTA FE", "HORARIO": "6:10, 6:40, 7:40, 8:10", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.182312038336136, 19.41242619109045 ] } },
{ "type": "Feature", "properties": { "NOMBRE": "SAMARA", "RUTA": "JUANACATLAN > SANTA FE", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.260264192429787, 19.368048575351764 ] } },
{ "type": "Feature", "properties": { "NOMBRE": "AV SANTA FE", "RUTA": "JUANACATLAN > SANTA FE", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.274855738861916, 19.357238847756165 ] } },
{ "type": "Feature", "properties": { "NOMBRE": "CITIBANAMEX", "RUTA": "JUANACATLAN > SANTA FE", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.259603165997447, 19.373314593225473 ] } }
]
}
