var json_ACOXPACOAPASANTAFE_7 = {
"type": "FeatureCollection",
"name": "ACOXPACOAPASANTAFE_7",
"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
"features": [
{ "type": "Feature", "properties": { "Name": "Acoxpa -> Coapa -> Santa Fe", "TIPO": "ASCENSO", "NOMBRE": "ACOXPA", "HORARIO": "6:20 , 8:15" }, "geometry": { "type": "Point", "coordinates": [ -99.1381019, 19.299196 ] } },
{ "type": "Feature", "properties": { "Name": "Acoxpa -> Coapa -> Santa Fe", "TIPO": "ASCENSO", "NOMBRE": "COSTCO", "HORARIO": "6:30 , 8:25" }, "geometry": { "type": "Point", "coordinates": [ -99.1375439, 19.2844196 ] } },
{ "type": "Feature", "properties": { "Name": "Acoxpa -> Coapa -> Santa Fe", "TIPO": "DESCENSO", "NOMBRE": "TEC DE MONTERREY", "HORARIO": null }, "geometry": { "type": "Point", "coordinates": [ -99.2613616, 19.3613002 ] } },
{ "type": "Feature", "properties": { "Name": "Acoxpa -> Coapa -> Santa Fe", "TIPO": "DESCENSO", "NOMBRE": "SAMARA", "HORARIO": null }, "geometry": { "type": "Point", "coordinates": [ -99.2601613, 19.3681135 ] } },
{ "type": "Feature", "properties": { "Name": "Acoxpa -> Coapa -> Santa Fe", "TIPO": "DESCENSO", "NOMBRE": "GLORIETA JUAN SAL AG", "HORARIO": null }, "geometry": { "type": "Point", "coordinates": [ -99.2769047, 19.3578974 ] } }
]
}
