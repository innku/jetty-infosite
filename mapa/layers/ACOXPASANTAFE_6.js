var json_ACOXPASANTAFE_6 = {
"type": "FeatureCollection",
"name": "ACOXPASANTAFE_6",
"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
"features": [
{ "type": "Feature", "properties": { "Name": "Acoxpa -> Santa Fe", "TIPO": "ASCENSO", "NOMBRE": "ACOXPA", "HORARIO": "6:40 , 7:20 , 7:50 , 8:30 , 9:10 , 9:45" }, "geometry": { "type": "Point", "coordinates": [ -99.1381019, 19.299196 ] } },
{ "type": "Feature", "properties": { "Name": "Acoxpa -> Santa Fe", "TIPO": "DESCENSO", "NOMBRE": "TEC DE MONTERREY", "HORARIO": null }, "geometry": { "type": "Point", "coordinates": [ -99.2613616, 19.3613002 ] } },
{ "type": "Feature", "properties": { "Name": "Acoxpa -> Santa Fe", "TIPO": "DESCENSO", "NOMBRE": "SAMARA", "HORARIO": null }, "geometry": { "type": "Point", "coordinates": [ -99.2601613, 19.3681135 ] } },
{ "type": "Feature", "properties": { "Name": "Acoxpa -> Santa Fe", "TIPO": "DESCENSO", "NOMBRE": "GLORIETA JUAN SAL AG", "HORARIO": null }, "geometry": { "type": "Point", "coordinates": [ -99.2769047, 19.3578974 ] } }
]
}
