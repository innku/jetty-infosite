var json_REFORMABOSQUES_10 = {
"type": "FeatureCollection",
"name": "REFORMABOSQUES_10",
"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
"features": [
{ "type": "Feature", "properties": { "Name": "Reforma -> Bosques", "NOMBRE": "REFORMA ESQ HAMBURGO", "HORARIO": "8:40", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.1745988, 19.421918 ] } },
{ "type": "Feature", "properties": { "Name": "Reforma -> Bosques", "NOMBRE": "BOSQUE DED DURAZNOS", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2425115, 19.4037075 ] } },
{ "type": "Feature", "properties": { "Name": "Reforma -> Bosques", "NOMBRE": "ARCOS BOSQUES", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.267348, 19.3826699 ] } },
{ "type": "Feature", "properties": { "Name": "Reforma -> Bosques", "NOMBRE": "PABELLON BOSQUES", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.2530707, 19.387168 ] } }
]
}
