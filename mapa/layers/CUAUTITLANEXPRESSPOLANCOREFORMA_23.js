var json_CUAUTITLANEXPRESSPOLANCOREFORMA_23 = {
"type": "FeatureCollection",
"name": "CUAUTITLANEXPRESSPOLANCOREFORMA_23",
"crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:OGC:1.3:CRS84" } },
"features": [
{ "type": "Feature", "properties": { "Name": "Cuautitlan (Express) -> Polanco -> Reforma", "NOMBRE": "CUAUTITLAN", "HORARIO": "6:20", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.1997233, 19.644181 ] } },
{ "type": "Feature", "properties": { "Name": "Cuautitlan (Express) -> Polanco -> Reforma", "NOMBRE": "PERINORTE", "HORARIO": "6:33", "TIPO": "ASCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.1895852, 19.6058081 ] } },
{ "type": "Feature", "properties": { "Name": "Cuautitlan (Express) -> Polanco -> Reforma", "NOMBRE": "ANTARA-MIYANA", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.200893, 19.4398313 ] } },
{ "type": "Feature", "properties": { "Name": "Cuautitlan (Express) -> Polanco -> Reforma", "NOMBRE": "KFC EJERCITO NACIONAL", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.1903215, 19.4377772 ] } },
{ "type": "Feature", "properties": { "Name": "Cuautitlan (Express) -> Polanco -> Reforma", "NOMBRE": "MARIANO ESCOBEDO", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.1820863, 19.4323609 ] } },
{ "type": "Feature", "properties": { "Name": "Cuautitlan (Express) -> Polanco -> Reforma", "NOMBRE": "HAMBURGO ESQ TOLEDO", "HORARIO": null, "TIPO": "DESCENSO" }, "geometry": { "type": "Point", "coordinates": [ -99.1721041, 19.4227462 ] } }
]
}
