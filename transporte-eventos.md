---
layout: tours
title: Jetty | La Mejor Solución de Transporte para tu Evento
description: Organizas o vas a un evento y estás buscando transporte. ¿Consideras rentar una camioneta con chofer? Nos adaptamos a tu necesidad al mejor precio.
id: transporte-eventos
---

<div class="header-eventos">
  <div class="container header-content-organizaciones">
    <div class="row">
      <div class="col-md-12 text-center">
        <h1>¿Estás buscando transporte para un evento?</h1>
        <h1>¿Consideras rentar una camioneta con chofer?</h1>
      </div>
      <div class="col-md-8 col-md-offset-2 text-center">
        <h3>Nos adaptamos a tu necesidad al mejor precio para resolver tu transporte.</h3>
        <button type="button" class="btn btn-default btn-gray" data-toggle="modal" data-target="#ModalEventos">
          Cotiza
        </button>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid eventos-content backgrayblue-down">
  <div class="container eventos">
    <div class="row">
      <div class="col-md-8 col-md-offset-2 text-center">
        <h2>Ya sea que organices o vayas a un evento masivo, tenemos la solución para ti.</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 eventos">
        <ul>
          <li>
            <figure>
              <img src="img/evento-deportivo.jpg" alt="Jetty, Evento deportivo">
              <figcaption>
                <h3>Evento Deportivo</h3>
              </figcaption>
            </figure>
          </li>
          <li>
            <figure>
              <img src="img/evento-boda.jpg" alt="jetty, Evento boda">
              <figcaption>
                <h3>Boda</h3>
              </figcaption>
            </figure>
          </li>
          <li>
            <figure>
              <img src="img/evento-concierto.jpg" alt="jetty, Evento Concierto">
              <figcaption>
                <h3>Concierto</h3>
              </figcaption>
            </figure>
          </li>
          <li>
            <figure>
              <img src="img/evento-empresarial.jpg" alt="jetty, Evento Empresarial">
              <figcaption>
                <h3>Evento Empresarial</h3>
              </figcaption>
            </figure>
          </li>
          <li>
            <figure>
              <img src="img/evento-ecoturismo.jpg" alt="jetty, Evento ecoturismo">
              <figcaption>
                <h3>Turismo</h3>
              </figcaption>
            </figure>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>


<div class="container-fluid content-buen-viaje" id="destino">
  <div class="container buen-viaje">

    <div class="row tour">
      <div class="col-md-12 text-center" data-aos="fade">
        <h2>Nuestros Tours</h2>
      </div>
    </div>

    <div class="row tour" data-aos="fade">

      <div class="col-md-4">
        <img src="img/muralismo-tour.jpg" alt="Tours Time Out Jetty">
        <h3>Nuevo Muralismo Mexicano</h3>
        <p>Te llevaremos a conocer de cerca el trabajo de artistas urbanos como Smithe, Curiot, Jesús Benítez “Dhear” y Colectivo Germen.</p>
        <p><b>Precio:</b><br> $980 MXN</p>
        <p><b>Duración: 5hrs</b></p>
        <p><b>Incluye:</b> Guía, lunch y transporte</p>
        <p><b>Próxima fecha:</b> 11 de mayo</p>
        <br>
        <button type="button" class="btn btn-green-small" data-toggle="modal" data-target="#ModalTourMuralismo">
          Agendar
        </button>
        <a href="tours/nuevo-muralismo-mexicano" class="btn btn-dark-gray-small">Más información</a>
      </div>

      <div class="col-md-4">
        <img src="img/mercado-san-juan-tour.jpg" alt="Tours Time Out Jetty">
        <h3>Mercado de San Juan</h3>
        <p>Además de comer en locales como Triana Café Gourmet y Las Tapas de San Juan, tendrás una clase de cocina con el chef Bernardo Bukantz.</p>
        <p><b>Precio:</b><br> $2,890 MXN</p>
        <p><b>Duración: 6hrs</b></p>
        <p><b>Incluye:</b> Guía, comida y transporte</p>
        <p><b>Próxima fecha:</b> 18 de mayo</p>
        <button type="button" class="btn btn-green-small" data-toggle="modal" data-target="#ModalTourMercado">
          Agendar
        </button>
        <a href="tours/mercado-san-juan" class="btn btn-dark-gray-small">Más información</a>
      </div>

      <div class="col-md-4">
        <img src="img/santa-maria-la-ribera-tour.jpg" alt="Tours Time Out Jetty">
        <h3>Santa María la Ribera</h3>
        <p>Visitarás lugares como el Kiosco Morisco, el Museo de Geología, Casa Equis, María Ciento 38 y Casa Nool.</p>
        <p><b>Precio:</b><br> $990 MXN</p>
        <p><b>Duración: 6hrs</b></p>
        <p><b>Incluye:</b> Guía, desayuno, comida y transporte</p>
        <p><b>Próxima fecha:</b> 25 de mayo</p>
        <br>
        <button type="button" class="btn btn-green-small" data-toggle="modal" data-target="#ModalTourSantamaria">
          Agendar
        </button>
        <a href="tours/santa-maria" class="btn btn-dark-gray-small">Más información</a>
      </div>

    </div>

  </div>
</div>

<div class="clearfix"></div>

<div class="space-greenUp">
  <img src="img/back-grayblue.png">
</div>

<div class="container eventos-servicios">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <h2 class="text-center">Nuestro servicio incluye:</h2>
    </div>

    <div class="col-md-4 text-center">
      <img src="img/chofer.png" class="img-servicios">
      <h3>Conductores profesionales</h3>
      <p>El conductor pasó un estricto proceso de selección que incluye entrevistas, exámenes toxicológicos y evaluaciones psicométricas, entre otras.</p>
    </div>
    <div class="col-md-4 text-center">
      <img src="img/interiores.png" class="img-servicios">
      <h3>Equipamiento</h3>
      <p>Aire acondicionado en cabina y para pasajeros, 4 cargadores USB por fila, GPS, Cámaras de seguridad hacia el interior y el exterior.</p>
    </div>
    <div class="col-md-4 text-center">
      <img src="img/camionetas.png" class="img-servicios">
      <h3>Vehículos nuevos</h3>
      <p>Crafter VW - 19pax <br> Transit Ford - 14 y 17pax</p>
    </div>

    <div class="col-md-8 col-md-offset-2 text-center">
      <button type="button" class="btn btn-default btn-green" data-toggle="modal" data-target="#ModalEventos">
        Cotiza
      </button>
    </div>
  </div>
</div>

<!-- Modal Eventos -->
<div class="modal fade" id="ModalEventos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <!-- <h4 class="modal-title" id="myModalLabel">Déjanos tus datos y nos pondremos en contacto</h4> -->
      </div>

      <div class="_form_3"></div><script src="https://jetty.activehosted.com/f/embed.php?id=3" type="text/javascript" charset="utf-8"></script>

      <!-- <form class="js--form-events">
        <div class="modal-body">

            <div class="col-md-6 form-group">
              <label  for="name">Nombre</label>
              <input  type="text"
                      class="form-control"
                      required
                      maxlength="30"
                      name="name"
                      placeholder="Nombre" />
            </div>

            <div class="col-md-6 form-group">
              <label  for="lastname">Apellido</label>
              <input  type="text"
                      class="form-control"
                      required
                      maxlength="60"
                      name="lastname"
                      placeholder="Apellido" />
            </div>

            <div class="col-md-6 form-group">
              <label for="mail">Correo electrónico</label>
              <input  type="email"
                      class="form-control"
                      required
                      maxlength="60"
                      name="email"
                      placeholder="Correo electrónico" />
            </div>

            <div class="col-md-6 form-group">
              <label for="cellphone">Número telefónico</label>
              <input  type="text"
                      class="form-control"
                      required
                      maxlength="20"
                      name="phone"
                      placeholder="Número telefónico">
            </div>

            <div class="col-md-12 form-group">
              <label for="eventname">¿Cómo se llama el evento?</label>
              <input  type="text"
                      class="form-control"
                      required
                      maxlength="150"
                      name="eventname"
                      placeholder="">
            </div>

            <div class="col-md-6 form-group">
              <label for="company">¿Eres uno de los organizadores?</label>
              <div class="radio">
                <label>
                  <input  type="radio"
                          name="organizer"
                          value="true"
                          checked>
                  Sí
                </label>
              </div>
              <div class="radio">
                <label>
                  <input  type="radio"
                          name="organizer"
                          value="false">
                  No
                </label>
              </div>
            </div>

            <div class="col-md-6 form-group">
              <label for="company">¿Cuál es tu necesidad?</label>
              <div class="radio">
                <label>
                  <input  type="radio"
                          name="trip"
                          value="Viaje sencillo" checked>
                  Viaje sencillo
                </label>
              </div>
              <div class="radio">
                <label>
                  <input  type="radio"
                          name="trip"
                          value="Viaje redondo">
                  Viaje redondo
                </label>
              </div>
              <div class="radio">
                <label>
                  <input  type="radio"
                          name="trip"
                          value="Vueltas constantes">
                  Vueltas constantes
                </label>
              </div>
            </div>

            <div class="col-md-12 form-group">
              <label for="company">¿Número de asistentes?</label>
            </div>
            <div class="col-md-6 form-group">
              <div class="radio">
                <label>
                  <input  type="radio"
                          name="people"
                          value="1-14" checked>
                  1 a 14
                </label>
              </div>
              <div class="radio">
                <label>
                  <input  type="radio"
                          name="people"
                          value="15-19">
                  15 a 19
                </label>
              </div>
              <div class="radio">
                <label>
                  <input  type="radio"
                          name="people"
                          value="20-28">
                  20 a 28
                </label>
              </div>
            </div>

            <div class="col-md-6 form-group">
              <div class="radio">
                <label>
                  <input  type="radio"
                          name="people"
                          value="29-38">
                  29 a 38
                </label>
              </div>
              <div class="radio">
                <label>
                  <input  type="radio"
                          name="people"
                          value="39-58">
                  39 a 58
                </label>
              </div>
              <div class="radio">
                <label>
                  <input  type="radio"
                          name="people"
                          value="+59">
                  Más de 59
                </label>
              </div>
            </div>

            <div class="col-md-12 form-group">
              <label for="adress">Dirección de salida</label>
              <input  type="text"
                      class="form-control"
                      name="origin_address"
                      maxlength="200"
                      placeholder="Calle, Nº, Colonia, Delegación, Ciudad, Estado" />
            </div>

            <div class="col-md-6 form-group">
              <label for="datetimepicker-event-1">Fecha de salida</label>
              <div class='input-group date datepicker'>
                <input  type='text'
                        class="form-control event-date-picker"
                        id='datetimepicker-event-1'
                        maxlength="20"
                        name="origin_date"
                        placeholder="Día / Mes / Año"/>
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
              </div>
            </div>

            <div class="col-md-6 form-group">
              <label for="timepicker-event-1">Hora de salida</label>
              <input  type="text"
                      id="timepicker-event-1"
                      maxlength="20"
                      class="form-control event-time-picker"
                      name="origin_time"
                      placeholder="AM / PM" />
            </div>

            <div class="col-md-12 form-group">
              <label for="adress">Dirección de Llegada</label>
              <input  type="text"
                      class="form-control"
                      maxlength="200"
                      name="destination_address"
                      placeholder="Calle, Nº, Colonia, Delegación, Ciudad, Estado" />
            </div>

            <div class="col-md-6 form-group">
              <label for="datetimepicker-event-2">Fecha de regreso  </label>
              <div class='input-group' data-date-format="dd/mm/yyyy">
                <input  type='text'
                        class="form-control event-date-picker"
                        id='datetimepicker-event-2'
                        maxlength="20"
                        name="destination_date"
                        placeholder="Día / Mes / Año" />
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
              </div>
            </div>

            <div class="col-md-6 form-group">
              <label for="timepicker-event-2">Hora de regreso </label>
              <input  type="text"
                      class="form-control event-time-picker"
                      id="timepicker-event-2"
                      maxlength="20"
                      name="destination_time"
                      placeholder="AM / PM">
            </div>

        </div>
        <div class="clearfix"></div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
          <button type="submit" class="btn btn-green-small">Enviar</button>
        </div>
      </form> -->

    </div>
  </div>
</div>

 <!-- Modal Conductor -->
 <div id="ModalSuccess" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Recibimos tu solicitud</h4>
      </div>

      <div class="modal-body text-center">
        <div class="row">
          <div class="col-md-12  .center">
              <h5>Nos pondremos en contacto contigo dentro de poco.</h5>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>


<!-- MODAL TOURS MURALISMO-->
<div class="modal fade" id="ModalTourMuralismo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header header-modal-muralismo">
        <h2 class="modal-title title-tours-modal" id="myModalLabel">Nuevo Muralismo Mexicano</h2>
      </div>

      <form class="js--form-events">
        <div class="modal-body">
          <div class="col-md-12">
            <div class="row">
              <div class="form-group col-md-6">
                <label for="nuevo-muralismo-name">Nombre</label>
                <input name="name" id="nuevo-muralismo-name" type="text" class="form-control" maxlength="30" placeholder="Nombre">
                <input name="eventname" type="hidden" value="Nuevo Muralismo Mexicano">
              </div>
              <div class="form-group col-md-6">
                <label for="nuevo-muralismo-lastname">Apellido</label>
                <input name="lastname" id="nuevo-muralismo-lastname" type="text" class="form-control" maxlength="60" placeholder="Apellido">
              </div>
              <div class="form-group col-md-6">
                <label for="nuevo-muralismo-email">Correo electrónico</label>
                <input name="email" id="nuevo-muralismo-email" type="email" class="form-control" placeholder="Correo electrónico">
              </div>
              <div class="form-group col-md-6">
                <label for="nuevo-muralismo-phone">Celular</label>
                <input name="phone" id="nuevo-muralismo-phone" type="text" class="form-control" placeholder="Número telefónico">
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="row">
              <div class="form-group col-md-6">
                <label for="nuevo-muralismo-people">Número de personas</label>
                <input name="people" id="nuevo-muralismo-people" type="text" class="form-control" placeholder="Número">
              </div>
              <div class="form-group col-md-6">
                <label for="nuevo-muralismo-date">Fecha de salida</label>
                <select class="form-control" id="nuevo-muralismo-date" name="origin_date">
                  <option value="11/Mayo/2019">11 de Mayo del 2019</option>
                </select>
              </div>
            </div>
          </div>
        </div>

        <div class="modal-footer modal-footer-tours">
          <button type="button" class="btn btn-gray-small" data-dismiss="modal">Cerrar</button>
          <button type="submit" class="btn btn-green-small">Enviar</button>
        </div>
      </form>

    </div>
  </div>
</div>

<!-- MODAL TOURS MERCADO-->
<div class="modal fade" id="ModalTourMercado" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header header-modal-mercado">
        <h2 class="modal-title title-tours-modal" id="myModalLabel">Mercado de San Juan</h2>
      </div>

      <form class="js--form-events">
        <div class="modal-body">
          <div class="col-md-12">
            <div class="row">
              <div class="form-group col-md-6">
                <label for="nuevo-mercado-name">Nombre</label>
                <input name="name" id="nuevo-mercado-name" type="text" class="form-control" maxlength="30" placeholder="Nombre">
                <input name="eventname" type="hidden" value="Mercado de San Juan">
              </div>
              <div class="form-group col-md-6">
                <label for="nuevo-mercado-lastname">Apellido</label>
                <input name="lastname" id="nuevo-mercado-lastname" type="text" class="form-control" maxlength="60" placeholder="Apellido">
              </div>
              <div class="form-group col-md-6">
                <label for="nuevo-mercado-email">Correo electrónico</label>
                <input name="email" id="nuevo-mercado-email" type="email" class="form-control" placeholder="Correo electrónico">
              </div>
              <div class="form-group col-md-6">
                <label for="nuevo-mercado-phone">Celular</label>
                <input name="phone" id="nuevo-mercado-phone" type="text" class="form-control" placeholder="Número telefónico">
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="row">
              <div class="form-group col-md-6">
                <label for="nuevo-mercado-people">Número de personas</label>
                <input name="people" id="nuevo-mercado-people" type="text" class="form-control" placeholder="Número">
              </div>
              <div class="form-group col-md-6">
                <label for="nuevo-mercado-date">Fecha de salida</label>
                <select class="form-control" id="nuevo-mercado-date" name="origin_date">
                  <option value="18/Mayo/2019">18 de Mayo del 2019</option>
                </select>
              </div>
            </div>
          </div>
        </div>

        <div class="modal-footer modal-footer-tours">
          <button type="button" class="btn btn-gray-small" data-dismiss="modal">Cerrar</button>
          <button type="submit" class="btn btn-green-small">Enviar</button>
        </div>
      </form>

    </div>
  </div>
</div>

<!-- MODAL TOURS SANTA MARIA-->
<div class="modal fade" id="ModalTourSantamaria" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header header-modal-santamaria">
        <h2 class="modal-title title-tours-modal" id="myModalLabel">Santa María la Ribera</h2>
      </div>

      <form class="js--form-events">
        <div class="modal-body">
          <div class="col-md-12">
            <div class="row">
              <div class="form-group col-md-6">
                <label for="nuevo-santamaria-name">Nombre</label>
                <input name="name" id="nuevo-santamaria-name" type="text" class="form-control" maxlength="30" placeholder="Nombre">
                <input name="eventname" type="hidden" value="Santa María la Ribera">
              </div>
              <div class="form-group col-md-6">
                <label for="nuevo-santamaria-lastname">Apellido</label>
                <input name="lastname" id="nuevo-santamaria-lastname" type="text" class="form-control" maxlength="60" placeholder="Apellido">
              </div>
              <div class="form-group col-md-6">
                <label for="nuevo-santamaria-email">Correo electrónico</label>
                <input name="email" id="nuevo-santamaria-email" type="email" class="form-control" placeholder="Correo electrónico">
              </div>
              <div class="form-group col-md-6">
                <label for="nuevo-santamaria-phone">Celular</label>
                <input name="phone" id="nuevo-santamaria-phone" type="text" class="form-control" placeholder="Número telefónico">
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="row">
              <div class="form-group col-md-6">
                <label for="nuevo-santamaria-people">Número de personas</label>
                <input name="people" id="nuevo-santamaria-people" type="text" class="form-control" placeholder="Número">
              </div>
              <div class="form-group col-md-6">
                <label for="nuevo-santamaria-date">Fecha de salida</label>
                <select class="form-control" id="nuevo-santamaria-date" name="origin_date">
                  <option value="25/Mayo/2019">25 de Mayo del 2019</option>
                </select>
              </div>
            </div>
          </div>
        </div>

        <div class="modal-footer modal-footer-tours">
          <button type="button" class="btn btn-gray-small" data-dismiss="modal">Cerrar</button>
          <button type="submit" class="btn btn-green-small">Enviar</button>
        </div>
      </form>

    </div>
  </div>
</div>

